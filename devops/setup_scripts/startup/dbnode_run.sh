env_file=./$1_env.sh
ports=$2
image=$3

CMD="docker run --env-file=$env_file \
	--name $1  -h $1 \
	--mount type=bind,src=/home/tcafe/db_data/dbnode1,dst=/var/lib/cassandra \
	--mount type=bind,src=/home/tcafe/db_config/dbnode1,dst=/etc/cassandra \
	--cap-add=NET_ADMIN \
	-p $ports \
	--restart unless-stopped --privileged=true \
	-e CASSANDRA_CLUSTER_NAME='Test Cluster' -e CASSANDRA_ENDPOINT_SNITCH=GossipingPropertyFileSnitch -e CASSANDRA_DC=datacenter1 -d $3"

echo "$CMD"
bash -c "$CMD" > /var/log/tcafe_ops/db_run.log 2>&1 
