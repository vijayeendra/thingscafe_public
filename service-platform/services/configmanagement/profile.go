//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package configmanagement

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.com/service-platform/common"
	objsvc "gitlab.com/service-platform/services/objectmanagement"
)

/*
 * Config profile contain profile for a certain kind of profile
 * described in the profile below
 */
type Config_profile struct {
	Id                gocql.UUID                 `json:"id"`                /* profile id */
	Name              string                     `json:"name"`              /* profile belong to entity */
	Entity_id         gocql.UUID                 `json:"entity_id"`         /* profile belong to entity */
	Profile_type      string                     `json:"profile_type"`      /* Asset or Entity  */
	Profile_type_id   gocql.UUID                 `json:"profile_type_id"`   /* to which entity/asset_type this applies */
	Profile_type_name string                     `json:"profile_type_name"` /* to which asset_type this applies */
	Cschema_id        gocql.UUID                 `json:"cschema_id"`        /* which schema was used */
	Cschema_name      string                     `json:"cschema_name"`      /* which schema was used */
	Aschema_id        gocql.UUID                 `json:"aschema_id"`        /* which apply schema to be used */
	Aschema_name      string                     `json:"aschema_name"`      /* which apply schema to be used */
	Config            string                     `json:"config"`            /* Configuration as json key value pairs This would be driven by the schema */
	Published_to      []gocql.UUID               `json:"published_to"`
	Apply_to          map[gocql.UUID]interface{} `json:"apply_to"`
	Apply_time        uint64                     `json:"apply_time"`
	Sync_time         uint64                     `json:"sync_time"`
	Config_order      int                        `json:"config_order"`
	Kv                map[string]interface{}     `json:"kv"`
}

func (e *Config_profile) SetEntityId(id gocql.UUID) {
	e.Entity_id = id
}

func (e *Config_profile) GetEntityId() gocql.UUID {
	return (e.Entity_id)
}

func (e *Config_profile) SetId(id gocql.UUID) {
	e.Id = id
}

func (e *Config_profile) GetId() gocql.UUID {
	return (e.Id)
}

var Publish_profile = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var object Config_profile
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_config_profile_query_options
	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	obj.Init("config_profiles_by_entity", &qoptions)

	if err := svc.Copy_object_svc(w, r, &obj, &object); err != nil {
		fmt.Println(err)
	}
	return
})

var Modify_config_profile = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	var object Config_profile
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	obj.Init("config_profiles", &qoptions)

	if err := svc.Modify_object_svc(w, r, &obj, &object); err != nil {
		fmt.Println(err)
	}
	return
})

var Update_profile_config = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	var object Config_profile
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options
	var uoptions common.Update_options

	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	obj.Init("config_profiles", &qoptions)
	uoptions.Where = make(map[string]interface{}, 5)
	obj.Init_for_update("config_profiles", &uoptions)

	if err := svc.Update_object_svc(w, r, &obj, &object); err != nil {
		fmt.Println(err)
	}
	return
})

var Create_config_profile = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	var object Config_profile
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	obj.Init("config_profiles", &qoptions)

	if err := svc.Create_object_svc(w, r, &obj, &object); err != nil {
		fmt.Println(err)
	}
	return
})

var List_config_profiles = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_config_profile_query_options
	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	if strings.Contains(r.URL.Path, "by-entity") {
		obj.Init("config_profiles_by_entity", &qoptions)
	} else {
		obj.Init("config_profiles", &qoptions)
	}
	if r.Method == "POST" {
		delete(qoptions.Where, "entity_id")
	} else {
		obj.Init("config_profiles_by_entity", &qoptions)
	}

	if err := svc.List_objects_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
})

var Get_config_profile = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj config_profile_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_config_profile_query_options
	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	obj.Init("config_profiles", &qoptions)

	if err := svc.Get_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
	return
})

var Delete_config_profile = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	obj.Init("config_profiles", &qoptions)

	if err := svc.Delete_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
		http.NotFound(w, r)
		return
	}
	return
})

func SetRoutes(router *mux.Router) *mux.Router {
	s := router.PathPrefix("/config").Subrouter()
	s.HandleFunc("/list-profiles", List_config_profiles).Methods("GET")
	s.HandleFunc("/list-profiles-by-entity", List_config_profiles).Methods("POST")
	s.HandleFunc("/list-profiles", List_config_profiles).Methods("POST")
	s.HandleFunc("/get-profile", Get_config_profile).Methods("POST")
	s.HandleFunc("/delete-profile", Delete_config_profile).Methods("POST")
	s.HandleFunc("/create-profile", Create_config_profile).Methods("POST")
	s.HandleFunc("/modify-profile", Create_config_profile).Methods("POST")
	s.HandleFunc("/update-profile-config", Update_profile_config).Methods("POST")
	s.HandleFunc("/publish-profile", Publish_profile).Methods("POST")
	SetschemaRoutes(s)
	return router
}

var default_config_profile_query_options common.Query_options
