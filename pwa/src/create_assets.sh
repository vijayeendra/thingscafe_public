#!/bin/bash
src=$1
dest=$2

cp $src/android/icon/drawable-hdpi-icon.png $dest/icon-72x72.png
cp $src/android/icon/drawable-xhdpi-icon.png $dest/icon-96x96.png
cp $src/android/icon/drawable-xxxhdpi-icon.png $dest/icon-192x192.png
cp $src/ios/icon/icon-76@2x.png $dest/icon-152x152.png
cp $src/ios/icon/icon-72@2x.png $dest/icon-144x144.png
cp $src/ios/icon/icon-72@2x.png $dest/icon-144x144.png
