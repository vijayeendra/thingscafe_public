//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package main

import (
	"errors"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/service-platform/common/mykafka"
	smsprovider "gitlab.com/service-platform/tc-mw/smsprovider"
	"gitlab.com/service-platform/tc-mw/utils"
)

var tcurl string

/*
	Structs
*/

type requestPayloadStruct struct {
	ProxyCondition string `json:"proxy_condition"`
}

/*
	Getters
*/

// Get the port to listen on
func getListenAddress() string {
	port := utils.Getenv("APPPORT", "8081")
	return ":" + port
}

// Serve a reverse proxy for a given url
func serveReverseProxy(target string, res http.ResponseWriter, req *http.Request) {
	// parse the url
	url, _ := url.Parse(target)

	// create the reverse proxy
	proxy := httputil.NewSingleHostReverseProxy(url)

	// Update the headers to allow for SSL redirection
	req.URL.Host = url.Host
	req.URL.Scheme = url.Scheme
	req.Header.Set("X-Forwarded-Host", req.Header.Get("Host"))
	req.Host = url.Host

	// Note that ServeHttp is non blocking and uses a go routine under the hood
	//log.Printf("Original url %s Redirecting to url: %s\n", req.URL, url)
	proxy.ServeHTTP(res, req)
	/*
		headers := res.Header()
		log.Printf("Deleting key from headers :", headers)
		headers.Del("Access-Control-Allow-Origin")
		log.Printf("After deleting headers :", headers)
		headers.Set("Access-Control-Allow-Origin", "*")
		log.Printf("After setting headers :", headers)
	*/
}

// Given a request send it to the appropriate url
var handleRequestAndRedirect = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//	requestPayload := parseRequestBody(req)
	//	url := getProxyUrl(requestPayload.ProxyCondition)

	//url := utils.Getenv("THINGSCAFE_SERVICE", "http://localhost:8083/")
	//url := os.Getenv("THINGSCAFE_SERVICE")
	serveReverseProxy(tcurl, w, r)
})

/*
	Entry
*/
const (
	Homefolder = "/tmp/"
)

func main() {

	tcurl = utils.Getenv("THINGSCAFE_SERVICE", "http://localhost:8083/")
	wait, _ := strconv.Atoi(utils.Getenv("WAIT_TIME", "300"))
	port := os.Getenv("APPPORT")
	addr := ":" + port

	port1 := os.Getenv("PROXYPORT")
	addr1 := ":" + port1

	r := mux.NewRouter()
	x := http.StripPrefix("/static/", http.FileServer(http.Dir("./static/")))
	r.PathPrefix("/static/").Handler(x)
	http.Handle("/", r)
	//	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	//r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(Homefolder+"static/"))))
	r.PathPrefix("/tc/").HandlerFunc(handleRequestAndRedirect)
	logRouter := handlers.LoggingHandler(os.Stdout, r)

	r1 := mux.NewRouter()
	//s := r1.PathPrefix("/aj/").Subrouter()
	s := r1.PathPrefix("/tcprov/").Subrouter()
	// Separate router for services
	s = smsprovider.SetRoutes(s)
	if s == nil {
		err := errors.New("Error connecting to SMS provider")
		panic(err)
	}
	s = SetRoutes(s)
	if s == nil {
		err := errors.New("Error connecting to thingscafe")
		panic(err)
	}
	//smsprovider.Register_cb(Parse_response_lite)
	logRouter1 := handlers.LoggingHandler(os.Stdout, r1)

	//cors optionsGoes Below
	corsOpts := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodGet, //http methods for your app
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
			http.MethodHead,
		},

		Debug:            true,
		AllowCredentials: true,
		AllowedHeaders: []string{
			"*", //or you can your header key values which you are using in your application

		},
	})
	// Start the App
	//log.Fatal(http.ListenAndServe(addr, logRouter))
	go func() {
		create_provdata_producer()
		serv := &http.Server{
			Addr:           addr,
			Handler:        corsOpts.Handler(logRouter1),
			ReadTimeout:    10 * time.Second,
			WriteTimeout:   time.Duration(wait * int(time.Second)),
			MaxHeaderBytes: 1 << 20,
		}
		log.Printf("starting server", addr)

		log.Fatal(serv.ListenAndServeTLS("server.pem", "server.key"))
		destroy_provdata_producer()
	}()

	_ = corsOpts
	log.Printf("starting proxy", addr1)
	serv := &http.Server{
		Addr:           addr1,
		Handler:        logRouter,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   30 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(serv.ListenAndServeTLS("server.pem", "server.key"))
	//log.Fatal(http.ListenAndServeTLS(addr1, "server.pem", "server.key", logRouter))

}

var prov_kp interface{}

func create_provdata_producer() {
	kc := utils.Getenv("TC_PROV_CLIENT", "tc_prov_process")

	mykafka.InitProducer()
	h := mykafka.GetProducer(kc)
	if h == nil {
		log.Fatal("Cannot create Producer")
	}
	prov_kp = h
}

func destroy_provdata_producer() {
	mykafka.Close(prov_kp)
}

func prov_publish(topic string, key string, data interface{}) {

	if !mykafka.Topic_exists(prov_kp, topic) {
		tc := mykafka.Get_topic_config(topic)
		err := mykafka.Create_topic(prov_kp, topic, tc)
		if err != nil {
			log.Println("Cannot create topic :", topic, err)
			return
		}
	}

	mykafka.Push(prov_kp, topic, key, data.([]byte))
}
