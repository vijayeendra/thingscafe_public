# !/bin/bash

if [ "$#" -ne 1 ]
then
	echo "Usage : $0 <package>"
	exit -1
fi
echo "Installing $1"
set -x

cd /var/www/html
rm -rf tcafe.old
mv tcafe tcafe.old
tar -xvzf $1
cp tcafe/manifest.hbs ~tcafe/scripts/whitelist/views/manifest2.hbs
cp tcafe/index2.hbs ~tcafe/scripts/whitelist/views/index2.hbs
forever restartall
