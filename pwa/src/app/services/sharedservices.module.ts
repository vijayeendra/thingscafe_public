//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { NgModule, ModuleWithProviders} from '@angular/core';
import { LoginService } from './login.service';
import { UserService } from './user.service';
import { EntityService } from './entity.service';
import { AssetService } from './asset.service';
import { AssetTypeService } from './asset_type.service';
import { ProfileService } from './profile.service';
import { SchemaService } from './schema.service';
import { TokenService } from './token.service';
import { ProvisionService } from './provision.service';
import { GlobalService } from './global.service';
import { MenuService } from './menu.service';
import { DataqueryService } from './dataquery.service';
import { StatsqueryService } from './statsquery.service';
import { PhoneValidator } from '../common/phone-validator';

@NgModule({
})
export class SharedServicesModule {
	 static forRoot():ModuleWithProviders {
        return {
            ngModule: SharedServicesModule,
            providers: [MenuService, LoginService, UserService, EntityService, AssetService, TokenService, AssetTypeService, ProfileService, SchemaService, ProvisionService, DataqueryService, StatsqueryService, PhoneValidator, GlobalService]
        };
	 }
}
