# !/bin/bash

if [ "$#" -ne 1 ]
then
	echo "Usage : $0 <package>"
	exit -1
fi

echo "Installing from $1"
set -x

nodes="tcnode11 tcnode12"

for node in $nodes 
do
	echo "Stopping services on $node"
	docker exec -i $node supervisorctl <<-EOF
	stop all
	EOF
done
sleep 5

cp -r ~/bin ~/bin.backup
echo "Backed up current running software to ~/bin.backup"
echo "Installing new release $1"
tar -C ~/bin -xvzf $1
#cp -r  $1/* ~/bin/
sleep 5

for node in $nodes 
do
	echo "Starting services on $node"
	if  [ $node == "tcnode11" ]
	then
		docker exec -i $node supervisorctl <<-'EOF'
		start all
		stop tc-collect-process
		EOF
	fi
	if  [ $node == "tcnode12" ]
	then
		docker exec -i $node supervisorctl <<-'!!'
		start all
		stop tc-provision
		!!
	fi
	sleep 10
done
