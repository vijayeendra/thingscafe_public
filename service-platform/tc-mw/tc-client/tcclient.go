//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package tcclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"sort"
	"strings"
)

type Login_info struct {
	Username         string `json:"username"`
	Password         string `json:"password"`
	Saas_entity_name string `json:"saas_entity_name"`
}

type Login_response struct {
	Id               string `json:"id"`
	Access_token     string `json:"access_token"`
	Refresh_token    string `json:"refresh_token"`
	Saas_entity_name string `json:"saas_entity_name"`
}

type User_info struct {
	Id               string `json:"id"`
	Entity_id        string `json:"entity_id"`
	First_name       string `json:"first_name"`
	Last_name        string `json:"last_name"`
	Saas_entity_name string `json:"saas_entity_name"`
}

var my_login_response Login_response
var tc_url string
var apiversion string

func Login(url string, saas string, user string, password string, version string) (error, interface{}) {
	login_input := new(Login_info)
	login_input.Username = user
	login_input.Password = password
	login_input.Saas_entity_name = saas

	login_json := new(bytes.Buffer)
	json.NewEncoder(login_json).Encode(login_input)

	//fmt.Println(login_json)
	tc_url = url
	apiversion = version
	login_url := url + "login"
	req, err := http.NewRequest("POST", login_url, login_json)
	if err != nil {
		log.Print(err)
		return err, nil
	}
	req.Header.Set("Content-Type", "application/json")

	fmt.Println("Performing ", req.URL.String())
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Print(err)
		return err, nil
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(body, &my_login_response)
	if err != nil {
		log.Print(err)
		return err, nil
	}
	log.Print(my_login_response)
	return nil, &my_login_response
}

const (
	BASIC_SCHEMA  string = "Basic "
	BEARER_SCHEMA string = "Bearer "
)

func Get_user_from_request(r *http.Request, w http.ResponseWriter) (interface{}, *User_info, error) {

	var user_info [1]User_info

	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		err := errors.New("Authorization header missing")
		return nil, nil, err
	}

	// Get the token from the request header
	// The first six characters are skipped - e.g. "Basic ".
	if strings.HasPrefix(authHeader, BASIC_SCHEMA) {
		err := errors.New("Basic Auth not supported")
		return nil, nil, err
	}

	// Confirm the request is sending Basic Authentication credentials.
	if !strings.HasPrefix(authHeader, BEARER_SCHEMA) {
		err := errors.New("Authorization requires Bearer schema")
		return nil, nil, err
	}

	//splitToken := strings.Split(reqToken, "Bearer")
	//reqToken = splitToken[1]
	access_token := authHeader[len(BEARER_SCHEMA):]

	bearer := "Bearer " + access_token
	self_url := tc_url + "v1/users/get-user"
	req, err := http.NewRequest("GET", self_url, nil)
	if err != nil {
		log.Print("Get_user:Request:", err)
		return nil, nil, err
	}
	req.Header.Add("Authorization", bearer)
	req.Header.Set("Content-Type", "application/json")

	fmt.Println("Performing ", req.URL.String())
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Print("Get_user:Perform:", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return nil, nil, nil
	}

	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)
		err = json.Unmarshal(body, &user_info)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			log.Print("Get_user:Unmarshall:", resp, string(body), err)
			return nil, nil, nil
		}
		log.Print(user_info[0])
		handle := new(Login_response)
		handle.Access_token = access_token
		handle.Id = user_info[0].Id
		return handle, &user_info[0], nil
	} else {
		log.Print("Get_user:", resp.Status)
		http.Error(w, resp.Status, resp.StatusCode)
		return nil, nil, nil
	}
}

func Get_user(access_token string) (interface{}, *User_info, error) {

	var user_info [1]User_info
	bearer := "Bearer " + access_token
	self_url := tc_url + "v1/users/get-user"
	req, err := http.NewRequest("GET", self_url, nil)
	if err != nil {
		log.Print("Get_user:Request:", err)
		return nil, nil, err
	}
	req.Header.Add("Authorization", bearer)
	req.Header.Set("Content-Type", "application/json")

	fmt.Println("Performing ", req.URL.String())
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Print("Get_user:Perform:", err)
		return nil, nil, err
	}

	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)
		err = json.Unmarshal(body, &user_info)
		if err != nil {
			log.Print("Get_user:Unmarshall:", resp, string(body), err)
			return nil, nil, err
		}
		log.Print(user_info[0])
		handle := new(Login_response)
		handle.Access_token = access_token
		handle.Id = user_info[0].Id
		return handle, &user_info[0], nil
	} else {
		err = errors.New(resp.Status)
		log.Print("Get_user:", resp.Status)
		return nil, nil, err
	}
}

type generic_query struct {
	Qoptions Query_options `json:"qoptions"`
}

type generic_input struct {
	Id       string        `json:"id"`
	Qoptions Query_options `json:"qoptions"`
}

type Query_options struct {
	Where           map[string]interface{} `json:"where"`
	Groupby         string                 `json:"groupby"`
	Orderby         map[string]string      `json:"orderby"`
	Limit           int                    `json:"limit"`
	Allow_filtering bool                   `json:"allow_filtering"`
	Aggregate       map[string]interface{} `json:"aggregate"`
}

type Entity_info struct {
	Id                    string                 `json:"id"`
	Name                  string                 `json:"name"`
	Entity_type           int                    `json:"entity_type"`
	Entity_id             string                 `json:"entity_id"`
	Serviced_by_entity_id string                 `json:"serviced_by_entity_id`
	Roles                 string                 `json:"roles"`
	Country               string                 `json:"country"`
	State                 string                 `json:"state"`
	City                  string                 `json:"city"`
	Address               string                 `json:"address"`
	Address2              string                 `json:"address2"`
	Zip                   string                 `json:"zip"`
	Phones                []string               `json:"phones"`
	Phone                 string                 `json:"phone"`
	Email                 string                 `json:"email"`
	Tags                  string                 `json:"tags"`
	Kv                    map[string]interface{} `json:"kv"`
}

type Asset_info struct {
	Entity_id       string
	Id              string
	Asset_type      string
	Parent_asset_id string
	Subtype         string
	Name            string
	Description     string
	User_defined    bool
	Kv              map[string]interface{}
	Collmap         string
	Location        string
	Ipaddr          net.IP
	Mobile          string
	Serial_no       string
	Configured      bool
	Root_asset_flag bool
}

type Asset_type_info struct {
	Id                          string
	Entity_id                   string
	Controlled_by_asset_type_id string
	Addressable                 bool
	Collection_type_list        map[string]interface{}
	Config                      string
	Kind                        string
	Kv                          map[string]string
	Name                        string
	Root_asset_flag             bool
}

var Config_profiles []Config_profile
var Entities []Entity_info
var Assets []Asset_info
var Asset_types []Asset_type_info

func get_suburl(object_type string, operation string) (string, interface{}) {
	var object interface{}
	var suburl string = ""

	switch object_type {
	case "profile":
		{
			parturl := operation + "-" + object_type
			if operation == "list" {
				parturl = operation + "-" + object_type + "s"
			}
			suburl = apiversion + "/config/" + parturl
			object = Config_profiles
		}
		break
	case "child-entity":
		{
			if operation == "list" {
				suburl = apiversion + "/" + "entities/" + operation + "-" + "child-entities"
			} else {
				suburl = apiversion + "/" + "entities/" + operation + "-" + "child-entities"
			}
			object = Entities
		}
		break
	case "controlled-entity":
		{
			if operation == "list" {
				suburl = apiversion + "/" + "entities/" + operation + "-" + "controlled-entities"
			} else {
				suburl = apiversion + "/" + "entities/" + operation + "-" + "controlled-entities"
			}
			object = Entities
		}
		break
	case "entity":
		{
			if operation == "list" {
				suburl = apiversion + "/" + "entities/" + operation + "-" + "entities"
			} else {
				suburl = apiversion + "/" + "entities/" + operation + "-" + "entity"
			}
			object = Entities
		}
		break
	case "asset":
		{
			parturl := object_type + "s/" + operation + "-" + object_type
			if operation == "list" {
				parturl = object_type + "s/" + operation + "-" + object_type + "s"
			}
			suburl = apiversion + "/" + parturl
			object = Assets
		}
		break
	case "asset-type":
		{
			parturl := object_type + "s/" + operation + "-" + object_type
			if operation == "list" {
				parturl = "assets/" + operation + "-" + object_type + "s"
			}
			suburl = apiversion + "/" + parturl
			object = Assets
		}
		break
	}
	return suburl, object
}

type Config_profile struct {
	Id                string /* profile id */
	Name              string /* profile id */
	Entity_id         string /* profile belong to entity */
	Profile_type      string /* Owner_config, Asset or Entity  */
	Profile_type_id   string /* to which entity/asset_type this applies */
	Profile_type_name string /* to which asset_type this applies */
	Cschema_id        string /* which schema was used */
	Cschema_name      string /* which schema was used */
	Aschema_id        string /* which apply schema to be used */
	Aschema_name      string /* which apply schema to be used */
	Config            string /* Configuration as json key value pairs This would be driven by the schema */
	Apply_to          map[string]interface{}
	Apply_time        int64
	Sync_time         int64
	Config_order      int
}

func Get_controller(handle interface{}, site_id string, ctrl_name string) *Asset_info {
	assets := Get_list_assets(handle, site_id)

	if len(*assets) == 0 {
		return nil
	}

	for _, asset := range *assets {
		if ctrl_name != "" {
			if asset.Name == ctrl_name {
				return &asset
			}
		} else if asset.Root_asset_flag == true {
			return &asset
		}
	}
	return nil
}

func Get_site(handle interface{}, entity_id string, site_name string) *Entity_info {

	sites := Get_sites(handle, entity_id)
	if len(*sites) == 0 {
		return nil
	}

	for _, site := range *sites {
		if site.Name == site_name {
			return &site
		}
	}
	return nil
}

func Get_sites(handle interface{}, entity_id string) *[]Entity_info {
	var entities []Entity_info = []Entity_info{}
	var sites []Entity_info = []Entity_info{}
	body := Get_object(handle, "list", "controlled-entity", entity_id, "serviced_by_entity_id")
	if len(body) > 0 {
		json.Unmarshal(body, &entities)
		for _, entity := range entities {
			if entity.Entity_type == 4 {
				sites = append(sites, entity)
			}
		}
		log.Print("Sites:", sites)
		return &sites
	} else {
		body := Get_object(handle, "list", "child-entity", entity_id, "entity_id")
		if len(body) > 0 {
			json.Unmarshal(body, &entities)
			for _, entity := range entities {
				if entity.Entity_type == 4 {
					sites = append(sites, entity)
				}
			}
			log.Print("Sites:", sites)
			return &sites
		}
	}
	return nil
}

func Get_controlled_entities(handle interface{}, entity_id string) *[]Entity_info {
	entities := []Entity_info{}
	body := Get_object(handle, "list", "controlled-entity", entity_id, "serviced_by_entity_id")
	if len(body) > 0 {
		json.Unmarshal(body, &entities)
		return (&entities)
	}
	return nil
}

func Get_asset(handle interface{}, asset_id string) *Asset_info {
	var assets []Asset_info = []Asset_info{}
	body := Get_object(handle, "get", "asset", asset_id, "id")
	if len(body) > 0 {
		json.Unmarshal(body, &assets)
		return &assets[0]
		//log.Print(assets)
	}
	return nil
}

func Get_entity(handle interface{}, entity_id string) *Entity_info {
	//var entities []Entity_info = []Entity_info{}
	var entity *Entity_info = new(Entity_info)
	body := Get_object(handle, "get", "entity", entity_id, "id")
	if len(body) > 0 {
		json.Unmarshal(body, &entity)
		//log.Print(entity)
		return entity
	}
	return nil
}

type ProfileSorter []Config_profile

func (a ProfileSorter) Len() int           { return len(a) }
func (a ProfileSorter) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ProfileSorter) Less(i, j int) bool { return a[i].Config_order < a[j].Config_order }

// Get list of objects matching criteria given as input
// example = suburl = v1/config/list-entities
func Get_list_profiles(handle interface{}, entity_id string) *[]Config_profile {
	body := Get_object(handle, "list", "profile", entity_id, "entity_id")
	config_profiles := []Config_profile{}
	if len(body) > 0 {
		json.Unmarshal(body, &config_profiles)
		sort.Sort(ProfileSorter(config_profiles))
		//log.Print(Config_profiles)
		return (&config_profiles)
	}
	return nil
}

// Get list of objects matching criteria given as input
// example = suburl = v1/config/list-entities
func Get_list_assets(handle interface{}, entity_id string) *[]Asset_info {
	assets := []Asset_info{}
	body := Get_object(handle, "list", "asset", entity_id, "entity_id")
	if len(body) > 0 {
		json.Unmarshal(body, &assets)
		//log.Print(Assets)
		return (&assets)
	}
	return nil
}

// Get list of objects matching criteria given as input
// example = suburl = v1/config/list-entities
func Get_list_asset_types(handle interface{}, entity_id string) *[]Asset_type_info {
	asset_types := []Asset_type_info{}
	body := Get_object(handle, "list", "asset-type", entity_id, "entity_id")
	if len(body) > 0 {
		json.Unmarshal(body, &asset_types)
		Asset_types = asset_types
		return &asset_types
	}
	return nil
}

// Get list of objects matching criteria given as input
// example = suburl = v1/config/list-entities
func Get_object(handle interface{}, collection string, object_type string, match_id string, match string) []byte {
	var login_response *Login_response = handle.(*Login_response)

	var bearer = "Bearer " + login_response.Access_token
	var input_data generic_query

	if match != "" {
		input_data.Qoptions.Where = make(map[string]interface{}, 10)
		input_data.Qoptions.Where[match] = match_id
		input_data.Qoptions.Allow_filtering = true
	} else {
		/*
			input_data = new(generic_input)
			input_data.Id = match_id
		*/
	}

	suburl, _ := get_suburl(object_type, collection)

	input_json := new(bytes.Buffer)
	json.NewEncoder(input_json).Encode(input_data)

	fmt.Println(input_json)
	final_url := tc_url + suburl
	req, err := http.NewRequest("POST", final_url, input_json)
	if err != nil {
		log.Print(err)
		return nil
	}
	req.Header.Add("Authorization", bearer)
	req.Header.Set("Content-Type", "application/json")

	fmt.Println("Performing ", req.URL.String())
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Print(err)
		return nil
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	return (body)
}
