//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package main

import (
	"context"
	"encoding/json"
	"log"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/google/uuid"
	cooked "gitlab.com/service-platform/common/cooked"
	mykafka "gitlab.com/service-platform/common/mykafka"
	"gitlab.com/service-platform/tc-collect/process/aggregates"
	"gitlab.com/service-platform/tc-mw/utils"
	sarama "gopkg.in/Shopify/sarama.v2"
	//"github.com/Shopify/sarama"
)

// Sarma configuration options
var (
	brokers    = ""
	version    = ""
	group      = ""
	topics     = ""
	assignor   = ""
	oldest     = true
	verbose    = false
	plugin_dir = ""
)
var my_node_id string

var aggregate_topics map[string]bool

type topic_cg_config struct {
	topics []string
	group  string
	offset int64
	create bool
}

// Consumer represents a Sarama consumer group consumer
type Consumer struct {
	ready   chan bool
	plugin  string
	handle  interface{}
	process interface{}
	data    interface{}
}

type process_plugin interface {
	Init() interface{}
	Parse(*sarama.ConsumerMessage) (interface{}, error)
}

var events_kp interface{}
var stats_kp interface{}

func create_event_producer() {
	kc := utils.Getenv("EVENT_PROCESSOR_CLIENT", "tc_event_process")

	mykafka.InitProducer()
	h := mykafka.GetProducer(kc)
	if h == nil {
		log.Fatal("Cannot create Producer")
	}
	events_kp = h
}

func create_stats_producer() {
	kc := utils.Getenv("STATS_PROCESSOR_CLIENT", "tc_stats_process")

	h := mykafka.GetProducer(kc)
	if h == nil {
		log.Fatal("Cannot create Producer")
	}
	stats_kp = h
}

func main() {
	//var topics []string
	var err error = nil
	var count = 0

	p := utils.Getenv("COLLECTOR_PLUGINS", "")

	if p == "" {
		log.Panicf("No plugins defined")
		return
	}
	my_node_id = uuid.New().String()
	create_event_producer()
	create_stats_producer()
	aggregate_topics = make(map[string]bool)
	aggregates.Setup(my_node_id, stats_kp)

	plugin_dir = utils.Getenv("COLLECTOR_PLUGIN_DIR", "./plugins")
	/*
		//Traverse plugin directory and load plugins
			files, err := ioutil.ReadDir(plugin_dir)
		    if err != nil {
		        log.Fatal(err)
				return
		    }

		    for _, file := range files {
				if strings.Contains(file.Name(), ".so") {
		        	fmt.Println("Adding plugin file.Name()")
					plugins = append(plugins, file.Name())
		    }
	*/

	plugins := strings.Split(p, ",")

	//client, err := mykafka.InitConsumerg("Process_data", "range", "process")

	//if err != nil {
	//	log.Panicf("Error creating consumer group client: %v", err)
	//}

	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}

	for _, plug := range plugins {
		var proc interface{}
		// Load plugin
		log.Println("Loading process plugin ", plug)
		// Load plugin
		mod := plugin_dir + "/" + plug + "_process.so"
		proc, err := utils.Load_plugin(mod, "Processor")
		if err != nil {
			continue
		}

		_ = aggregates.Setup_app_aggregates(plug)

		/*
		 * Setup  new Sarama consumer groups for all topics
		 */
		topic_cfg := get_topics(plug)

		for _, topc := range topic_cfg {

			if topc.create {
				for _, topic := range topc.topics {
					tc := mykafka.Get_topic_config(topic)
					err := mykafka.Create_topic(stats_kp, topic, tc)
					if err != nil {
						log.Panicf("Error creating topic :%s :%v", topic, err)
					}
				}
			}

			topics := topc.topics
			group := topc.group
			offset := topc.offset
			client_id := plug + "_process"
			handle := mykafka.InitConsumerg(group, "range", client_id, offset)

			if err != nil {
				log.Println("Error creating consumer group client: %v", err)
				continue
			}

			p := proc.(process_plugin)
			plugin_data := p.Init()
			consumer := Consumer{
				ready:   make(chan bool),
				plugin:  plug,
				handle:  handle,
				process: proc,
				data:    plugin_data,
			}

			wg.Add(1)
			go func() {
				defer wg.Done()
				for {
					log.Printf("Starting Client Consume : %v", topics)
					if err := handle.Consume(ctx, topics, &consumer); err != nil {
						log.Printf("Error from consumer: %v", err)
					} else {
						consumer.ready = make(chan bool)
					}
					// check if context was cancelled, signaling that the consumer should stop
					if ctx.Err() != nil {
						log.Printf("Closing Client due to Context Error : %v", ctx.Err)
						handle.Close()
						return
					}
					time.Sleep(10 * time.Second)
				}
			}()
			<-consumer.ready // Await till the consumer has been set up
		}
		count++
		log.Println("Consumers up for plugin ", plug)
	}

	if count > 0 || err == nil {
		log.Println("Consumers up and running!...")
		sigterm := make(chan os.Signal, 1)
		signal.Notify(sigterm, syscall.SIGINT, syscall.SIGTERM)
		select {
		case <-ctx.Done():
			log.Println("terminating: context cancelled")
		case <-sigterm:
			log.Println("terminating: via signal")
			aggregates.Shutdown()
			cancel()
		}
		wg.Wait()
	}
	mykafka.Close(events_kp)
	mykafka.Close(stats_kp)
	os.Exit(-1)
}

// Setup is run at the beginning of a new session, before ConsumeClaim
func (consumer *Consumer) Setup(session sarama.ConsumerGroupSession) error {
	// Mark the consumer as ready
	log.Println("Setup for Plugin:", consumer.plugin)
	claims := session.Claims()
	// Get a map of partition and offsets to read
	for topic, partitions := range claims {
		log.Printf("Topics claimed: topic = %s, partition=%d", topic, partitions)
		if _, ok := aggregate_topics[topic]; ok {
			offset := aggregates.Offset(topic)
			for _, partition := range partitions {
				session.ResetOffset(topic, partition, offset, "restart")
			}
		}
	}
	close(consumer.ready)
	return nil
}

// Cleanup is run at the end of a session, once all ConsumeClaim goroutines have exited
func (consumer *Consumer) Cleanup(session sarama.ConsumerGroupSession) error {
	log.Println("Cleanup for Plugin:", consumer.plugin)
	return nil
}

// ConsumeClaim must start a consumer loop of ConsumerGroupClaim's Messages().
func (consumer *Consumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {

	// NOTE:
	// Do not move the code below to a goroutine.
	// The `ConsumeClaim` itself is called within a goroutine, see:
	// https://github.com/Shopify/sarama/blob/master/consumer_group.go#L27-L29
	log.Println("Claim for Plugin:", consumer.plugin)
	p := consumer.process.(process_plugin)

	for message := range claim.Messages() {
		log.Printf("Message claimed: value = %s, timestamp = %v, topic = %s, partition=%d offset=%d", string(message.Value), message.Timestamp, message.Topic, message.Partition, message.Offset)
		if message.Value == nil {
			session.MarkMessage(message, "")
			continue
		}
		if _, ok := aggregate_topics[message.Topic]; ok {
			aggregates.Consume(consumer.plugin, message)
		} else if !strings.Contains(message.Topic, "_rawstats") {
			msgi, err := p.Parse(message)
			if err == nil {
				var key string
				msg := msgi.(*cooked.Cooked_data)
				etopic := message.Topic + "_events"
				for _, rec := range msg.Events {
					key = rec.Asset_name
					msg_json, err := json.Marshal(rec)
					if err == nil {
						if !mykafka.Topic_exists(events_kp, etopic) {
							tc := mykafka.Get_topic_config(etopic)
							err := mykafka.Create_topic(events_kp, etopic, tc)
							if err != nil {
								log.Println("Cannot create topic :", etopic, err)
								continue
							}
						}
						mykafka.Push(events_kp, etopic, key, msg_json)
					}
				}
				stopic := message.Topic + "_rawstats"
				for _, rec := range msg.Stats {
					key = rec.Asset_name
					msg_json, err := json.Marshal(rec)
					if err == nil {
						mykafka.Push(stats_kp, stopic, key, msg_json)
					}
				}
				log.Println("Processed msg:", msg)
			} else {
				log.Println("Processing Error:", err)
			}
		} else {
			aggregates.Publish_aggregates(message.Value, consumer.data)
		}
		session.MarkMessage(message, "")
	}
	log.Println("Exiting ConsumeClaim:")

	return nil
}

func get_topics(plug string) []topic_cg_config {

	var list []topic_cg_config

	t1 := new(topic_cg_config)
	t1.topics = []string{}
	t1.topics = append(t1.topics, plug+"_data")
	t1.group = plug + "_Process_data"
	t1.offset = -1
	t1.create = false
	list = append(list, *t1)

	t1 = new(topic_cg_config)
	t1.topics = append(t1.topics, plug+"_data_rawstats")
	t1.offset = -1
	t1.create = true
	t1.group = plug + "_Process_rawstats"
	t1.offset = -1

	list = append(list, *t1)

	aggr_topics := aggregates.Topics_to_consume(plug)
	t1 = new(topic_cg_config)
	t1.topics = aggr_topics
	t1.group = my_node_id + "_Aggregate_data"
	t1.offset = -2
	t1.create = false
	list = append(list, *t1)

	for _, atopic := range aggr_topics {
		aggregate_topics[atopic] = true
	}
	return list
}
