# TC Core configs
# Only ports change for every new instance
PORT=8003
PROXYPORT=8002
APPPORT=8001
THINGSCAFE_SERVICE=http://localhost:8003/
# TC provision configs
PROVISIONING_PLUGIN_DIR=/home/tcafe/bin/plugins
PROVISIONING_PLUGINS=saasv
SKIPINITC="no"
WAIT_TIME="360"
THRESHOLD_TIME="300"
TC_WAIT_TIME="60"
# Database config
CASSANDRA=172.17.0.3
CASSANDRA_USER=admin
CASSANDRA_PASSWD=admin123
# Kafka Broker and log config
KAFKA_BROKERS=172.17.0.1:9092
KAFKA_VERBOSE="true"
# Collector related config
COLLECTORPORT=8000
COLLECTOR_NUM_PARTITIONS=10
COLLECTOR_PLUGINS=saasv
COLLECTOR_PLUGIN_DIR=/home/tcafe/bin/plugins
# Authentication for thingscafe service for SAASV
saasv_TCUSER=admin@saasv.com
saasv_TCPASSWD=saasv123
saasv_THINGSCAFE_COLLECT_URL=https://app.saasv.com/tccollect/v1/submit-data?
# Textlocal number which controller sends to
TEXTLOCALTELE=+919220592205
saasv_TEXTLOCALSMSPREFIX=ABCDE
saasv_TEXTLOCALSENDER=SAASV1
saasv_TEXTLOCALAPIKEY=GWbHzmC778o-k99apwwid8jVN5QULAIG5ieQ5vJsJo
SMSENABLED="yes"
