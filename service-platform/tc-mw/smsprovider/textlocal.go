//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package smsprovider

import (
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"gitlab.com/service-platform/tc-mw/utils"
)

const smsproviderurl = "https://api.textlocal.in/"

//var sms_ack chan string
var callback_fn func(msg []byte, req string) (bool, bool, []byte, int)

type app_message_params struct {
	app         string
	req_queue   map[string]*http.Request
	data_queue  map[string]string
	resp_queue  map[string]string
	chan_map    map[string]chan bool
	callback_fn func(msg []byte, req string) (bool, bool, []byte, int)
}

var req_queue map[string]*http.Request
var data_queue map[string]string
var resp_queue map[string]string
var callback_map map[string]*app_message_params

// Get env var or default
func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func Register_cb(app string, smsprefix string, cb func(msg []byte, req string) (bool, bool, []byte, int)) {
	mp := new(app_message_params)
	mp.app = app
	mp.req_queue = make(map[string]*http.Request)
	mp.resp_queue = make(map[string]string)
	mp.data_queue = make(map[string]string)
	mp.chan_map = make(map[string]chan bool)
	mp.callback_fn = cb

	callback_map[smsprefix] = mp
	//callback_fn = cb
}

func find_mp(app string) *app_message_params {
	for _, mp := range callback_map {
		if mp.app == app {
			return mp
		}
	}
	return nil
}

func Send_sms(app string, target string, data string, seq_number uint16) (bool, string, error) {

	api_key_string := app + "_TEXTLOCALAPIKEY"
	api_key := utils.Getenv(api_key_string, "")
	if api_key == "" {
		err := errors.New("API Key not set")
		return false, "API Key not set", err
	}
	sender_string := app + "_TEXTLOCALSENDER"
	sender := getEnv(sender_string, "")
	if sender == "" {
		err := errors.New(" Sender not set")
		return false, "Sender not set", err
	}
	mp := find_mp(app)
	if mp == nil {
		err := errors.New("Server Message params error")
		return false, "Message params not initialized", err
	}
	if _, ok := mp.req_queue[target]; ok {
		err := errors.New("One request already outstanding. Try after sometime")
		return false, "Waiting for response for outstanding request", err
	}

	sms_enable := getEnv("SMSENABLED", "no")
	sms_timeout := getEnv("SMSTIMEOUT", "180")
	sms_delay := getEnv("SMSDELAY", "180")
	//send_error := getEnv("SIMULATE_ERROR", "no")
	send_url := smsproviderurl + "send"
	req, err := http.NewRequest("POST", send_url, nil)

	if err != nil {
		log.Println(err)
		return false, "Server Error", err
	}

	q := req.URL.Query()
	q.Add("api_key", api_key)
	q.Add("message", data)
	q.Add("sender", sender)
	q.Add("numbers", target)
	req.URL.RawQuery = q.Encode()
	//qid := target + "_" + strconv.Itoa(int(seq_number))
	qid := target

	fmt.Println(req.URL.String(), ":", qid)

	if sms_enable == "no" {
		// Code used for simulation of errors
		sms_delay1, _ := strconv.Atoi(sms_delay)
		fmt.Println("Delaying by:", sms_delay1)
		timer1 := time.NewTimer(time.Duration(sms_delay1 * int(time.Second)))
		//time.Sleep(time.Duration(sms_delay1 * int(time.Second)))
		<-timer1.C
		err_int := rand.Intn(100) + 200
		if err_int > 270 {
			k := strings.Split(data, " ")
			position := strconv.Itoa(rand.Intn(len(k)))
			//"*Err 2 228 0 0 #49"
			err_string := "*Err " + position + " " + strconv.Itoa(err_int) + " 0 0 #49"
			if err_int > 290 {
				err = errors.New("Timed out")
			} else {
				err = errors.New("Command failed")
			}
			return false, err_string, err
		} else {
			return true, "*Cmd Accepted #16", nil
		}
	}

	retries := 0
	mp.req_queue[qid] = req
	mp.data_queue[qid] = data
	client := &http.Client{}

	var resp *http.Response

	for retries < 3 {
		resp, err = client.Do(req)
		if err == nil {
			fmt.Println(resp)
			//sms_ack = make(chan string, 1)
			sms_ack := make(chan bool, 1)
			mp.chan_map[qid] = sms_ack
			sms_timeout1, _ := strconv.Atoi(sms_timeout)
			defer func() {
				// If we try to close an already closed channel
				if err := recover(); err != nil {
					log.Println("Recover:", qid, err)
				}
			}()
			for {
				select {
				case <-time.After(time.Duration(sms_timeout1) * 1000 * time.Millisecond):
					log.Println("timed out waiting for ack", qid)
					delete(mp.chan_map, qid)
					close(sms_ack)
					delete(mp.data_queue, qid)
					delete(mp.req_queue, qid)
					err = errors.New("Timed out")
					return false, "Timed out waiting for Ack", err
				case success := <-sms_ack:
					cresp := mp.resp_queue[qid]
					delete(mp.chan_map, qid)
					close(sms_ack)
					delete(mp.resp_queue, qid)
					delete(mp.data_queue, qid)
					delete(mp.req_queue, qid)
					if success {
						log.Println("========Received SMS ack========", qid)
						return true, cresp, nil
					} else {
						log.Println("========Received SMS Nack========", qid)
						err = errors.New("Command failed")
						return false, cresp, err
					}
				}
			}
		} else {
			retries++
			log.Printf("Try %d, Sending SMS failed :%s", retries, err)
			continue
		}
	}
	return false, "SMS send failed ", err
}

type Message struct {
	Sender    string `schema:"sender"`
	Content   string `schema:"content"`
	Comments  string `schema:"comments"`
	Innumber  string `schema:"innumber"`
	Keyword   string `schema:"keyword"`
	email     string `schema:"email"`
	credits   string `schema:"credits"`
	custom1   string `schema:"custom1"`
	custom2   string `schema:"custom2"`
	custom3   string `schema:"custom3"`
	submit    string `schema:"submit"`
	rcvd      string `schema:"rcvd"`
	network   string `schema:"network"`
	msgid     string `schema:"msgId"`
	lastname  string `schema:"lastname"`
	firstname string `schema:"firstname"`
}

var decoder = schema.NewDecoder()

var Receive_message = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var mp *app_message_params
	var ok bool

	err := r.ParseForm()
	message := new(Message)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Println("Received Message Format error", err.Error(), r.URL, r.Form, r.PostForm)
		return
	} else {
		log.Println("Received Message :", r.URL, r.Form)
	}

	err = decoder.Decode(message, r.Form)

	if err != nil {
		log.Println("Received Message Decoding Error", err.Error(), r.URL, r.Form, r.PostForm)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	qid := message.Sender
	/*
		_, ok = (<-sms_ack)
		if !ok {
			log.Println("Wrong server. Redirecting")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	*/

	// Parse the message and get the sequence number
	smskwd := message.Keyword
	if mp, ok = callback_map[smskwd]; !ok {
		log.Println("Callback not registered", smskwd)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Println("Received message ", mp.app+":"+message.Comments)

	more, ok, _, num := mp.callback_fn([]byte(message.Comments), mp.data_queue[qid])

	mp.resp_queue[qid] = message.Comments
	if more {
		// The next message hopefully contains data we want
		return
	}

	defer func() {
		// If we try to send a message on closed channel
		if err := recover(); err != nil {
			log.Println("Recover:", qid, err)
		}
	}()

	sms_ack := mp.chan_map[qid]
	if ok {
		log.Println("Operation succeeded.", num)
		sms_ack <- true
	} else {
		log.Println("Bad response/Operation failed.")
		sms_ack <- false
	}
	//qid := message.Sender + "_" + strconv.Itoa(num)

	return
})

func SetRoutes(router *mux.Router) *mux.Router {
	s := router.PathPrefix("/smsp/v1/").Subrouter()
	s.HandleFunc("/receive-message/", Receive_message).Methods("POST")
	callback_map = make(map[string]*app_message_params)
	return router
}
