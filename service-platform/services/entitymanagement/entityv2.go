//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package entitymanagement

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.com/service-platform/common"
	objsvc "gitlab.com/service-platform/services/objectmanagement"
)

var default_entity_query_options common.Query_options

type Entity_info struct {
	Name                  string                 `json:"name"`
	Entity_id             gocql.UUID             `json:"entity_id"`
	Address               string                 `json:"address"`
	Address2              string                 `json:"address2"`
	City                  string                 `json:"city"`
	State                 string                 `json:"state"`
	Country               string                 `json:"country"`
	Zip                   string                 `json:"zip"`
	Domain                string                 `json:"domain"`
	Email                 string                 `json:"email"`
	Entity_type           uint                   `json:"entity_type"`
	Id                    gocql.UUID             `json:"id"`
	Serviced_by_entity_id gocql.UUID             `json:"serviced_by_entity_id"`
	Phone                 string                 `json:"phone"`
	Phones                []string               `json:"phones"`
	Kv                    map[string]interface{} `json:"kv"`
	Tags                  string                 `json:"tags"`
}

func (e *Entity_info) SetEntityId(id gocql.UUID) {
	e.Entity_id = id
}

func (e *Entity_info) GetEntityId() gocql.UUID {
	return (e.Entity_id)
}

func (e *Entity_info) SetId(id gocql.UUID) {
	e.Id = id
}

func (e *Entity_info) GetId() gocql.UUID {
	return (e.Id)
}

var Create_entity = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	var object Entity_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	obj.Init("entities", &qoptions)

	if err := svc.Create_object_svc(w, r, &obj, &object); err != nil {
		fmt.Println(err)
	}
	return
})

var List_entities = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj Entity_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_entity_query_options
	etype := common.Setup_qoptions_with_etype(r, &qoptions)
	delete(qoptions.Where, "user_id")
	if r.Method == "POST" {
		if strings.Contains(r.URL.Path, "child-entities") {
			obj.Init("child_entities", &qoptions)
			obj.Serviced_entity_flag = false
		} else {
			obj.Init("controlled_entities", &qoptions)
			obj.Serviced_entity_flag = true
		}
	} else {
		if etype == 2 {
			obj.Init("controlled_entities", &qoptions)
			obj.Serviced_entity_flag = true
		} else {
			obj.Init("child_entities", &qoptions)
			obj.Serviced_entity_flag = false
		}
	}

	if err := svc.List_objects_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
})

var Get_entity = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj Entity_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	/* TBD - How to check whether the user entity has permission to get this data */
	/* Check if the entity given here is part of the child_entities table ? */

	qoptions = default_entity_query_options
	etype := common.Setup_qoptions_with_etype(r, &qoptions)
	delete(qoptions.Where, "user_id")
	if etype == 2 {
		obj.Init("controlled_entities", &qoptions)
		obj.Serviced_entity_flag = true
	} else {
		obj.Serviced_entity_flag = false
		if strings.Contains(r.URL.Path, "by-name") {
			obj.Init("entities", &qoptions)
		} else {
			obj.Init("entity", &qoptions)
			delete(qoptions.Where, "entity_id")
		}
	}

	if err := svc.Get_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
	return
})

var Get_self = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj Entity_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_entity_query_options
	common.Setup_qoptions(r, &qoptions)
	/* For self, change the where check to own entity */
	if r.Method == "GET" {
		qoptions.Where["id"] = qoptions.Where["entity_id"]
	}
	delete(qoptions.Where, "entity_id")
	delete(qoptions.Where, "user_id")
	obj.Init("entity", &qoptions)

	if err := svc.List_objects_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
	return
})

var Delete_entity = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj Entity_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	obj.Init("entities", &qoptions)

	if err := svc.Delete_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
		http.NotFound(w, r)
		return
	}
	return
})

func SetRoutes(router *mux.Router) *mux.Router {
	s := router.PathPrefix("/entities").Subrouter()
	s.HandleFunc("/", List_entities).Methods("GET")
	s.HandleFunc("/list-entities", List_entities).Methods("GET")
	s.HandleFunc("/list-child-entities", List_entities).Methods("POST")
	s.HandleFunc("/list-controlled-entities", List_entities).Methods("POST")
	s.HandleFunc("/get-entity", Get_self).Methods("GET")
	s.HandleFunc("/get-entity", Get_entity).Methods("POST")
	s.HandleFunc("/get-entity-by-name", Get_entity).Methods("POST")
	s.HandleFunc("/delete-entity", Delete_entity).Methods("POST")
	s.HandleFunc("/create-entity", Create_entity).Methods("POST")
	s.HandleFunc("/modify-entity", Create_entity).Methods("POST")
	/*
		router.HandleFunc("/token-auth", controllers.Login).Methods("POST")
		router.Handle("/refresh-token-auth", negroni.New(negroni.HandlerFunc(controllers.RefreshToken))).Methods("GET")
		router.Handle("/logout", negroni.New(negroni.HandlerFunc(authentication.RequireTokenAuthentication), negroni.HandlerFunc(controllers.Logout))).Methods("GET")
	*/
	return router
}

func init() {
	/*
		Where           map[string]interface{} `json:"where"`
		Groupby         string                 `json:"groupby"`
		Orderby         map[string]string      `json:"orderby"`
		Limit           int                    `json:"limit"`
		Allow_filtering bool                   `json:"allow_filtering"`
		Aggregate       map[string]interface{} `json:"aggregate"`
	*/
	default_entity_query_options = common.Query_options{
		Limit: 50, Allow_filtering: true}

}
