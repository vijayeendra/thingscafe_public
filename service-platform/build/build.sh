# !/bin/bash

#   Copyright 2020 thingscafe.net
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http:#www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

export ROOT_DIR=~/go/src/gitlab.com/service-platform
export TCBUILD=~/go/src/gitlab.com/service-platform/build/debug
#export CGO_ENABLED=0

mkdir debug
mkdir production
build="debug"
if [ "$1" == "prod" ]
then
	build="production"
	export TCBUILD=~/go/src/gitlab.com/service-platform/build/production
	#go clean -x -cache
fi

echo "Building tc-core .."

cd $ROOT_DIR/tc-core

make clean 
make compile 
echo "Building tc-provision .."

cd $ROOT_DIR/tc-provision
cd plugins
./build_plugins.sh
cd ..
make clean 
make compile

echo "Building tc-collect .."

cd $ROOT_DIR/tc-collect/http

make clean 
make compile

echo "Building tc-collect-process with $build.."

cd $ROOT_DIR/tc-collect/process
cd plugins
./build_plugins.sh
cd ..

make clean 
make compile BUILD="$build"

cd $TCBUILD
last_commit=`git rev-parse --short HEAD`
tarfile="tcafe_sp_build.tar.gz_${last_commit}-$(date +'%m%d%y')"
binaries="plugins tc-core tc-collect tc-provision tc-collect-process"

tar -cvzf $tarfile $binaries

echo "Find ThingsCafe service-platform build in $BUILD/$tarfile"
