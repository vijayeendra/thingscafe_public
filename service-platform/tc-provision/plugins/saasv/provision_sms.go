//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

// This code is purely for reference. The code has been derived from a working plugin
// by changing names and references. The main idea is for developers to know what
// methods to provide and how they can actually go about building a provisioning
// plugin. Some of the assets, asset-types, params may not make sense but they are
// there just for reference.

package main

import (
	b64 "encoding/base64"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/howeyc/crc16"
	plugins "gitlab.com/service-platform/tc-mw/plugins"
	"gitlab.com/service-platform/tc-mw/smsprovider"
	tcclient "gitlab.com/service-platform/tc-mw/tc-client"
	"gitlab.com/service-platform/tc-mw/utils"
)

// "name":"MWS","mtype":"Water Supply","wsource":"River","capacity":"5","wd":"1000","dryrun":true,"cmeasure":true,"vmonitor":true,"tank":"Tank1","monsystem":"Plus System","vmin":"200","vmax":"450","submit":""}

var abc plugins.Provision_plugin

type vendor string

var Provision vendor

const MAX_SMS_LENGTH = 140

var seq_num int16 = 0

type apply_data struct {
	Site_id         string `json:"site_id"`
	Site_name       string `json:"site_name"`
	Asset_id        string `json:"asset_id"`
	Asset_name      string `json:"asset_name"`
	Parent_asset_id string `json:"parent_asset_id"`
	Type_name       string `json:"type_name"`
	Type_id         string `json:"type_id"`
}

type Pump_config struct {
	Name      string `json:"name"`
	Mtype     string `json:"mtype"`
	Wsource   string `json:"wsource"`
	Capacity  string `json:"capacity"`
	Wd        string `json:"wd"`
	Dryrun    bool   `json:"dryrun"`
	Cmeasure  bool   `json:"cmeasure"`
	Vmonitor  bool   `json:"vmonitor"`
	Tank      string `json:"tank"`
	Monsystem string `json:"monsystem"`
	Vmin      string `json:"vmin"`
	Vmax      string `json:"vmax"`
}

// {"name":"VCSwitching","type":"Switching","connection":"Double","swdelay":"250","stype":"Red","stexture":"Sandy","croptype":"100","sensorid":"10","submit":""}
type Switch_config struct {
	Name       string `json:"name"`
	Type       string `json:"type"`
	Connection string `json:"connection"`
	Swdelay    string `json:"swdelay"`
	Stype      string `json:"stype"`
	Stexture   string `json:"stexture"`
	Croptype   string `json:"croptype"`
	Sensorid   string `json:"sensorid"`
	Dh         string `json:"dh"`
}

// pipeline config{"Pumps":"{\"asset_type\":\"bcccb7e0-2582-11e9-92b1-97234f04ea82\",\"asset_type_name\":\"Pumps\",\"chosen_list\":[\"M1\",\"M2\"],\"alist\":[\"M1\",\"M2\",\"M3\",\"M4\"],\"subtype_list\":[],\"asset_id\":\"\",\"properties\":\"\"}","Switch":"{\"asset_type\":\"1242cfb0-2584-11e9-92b1-97234f04ea82\",\"asset_type_name\":\"Switch\",\"chosen_list\":[\"V3\",\"V4\",\"V5\"],\"alist\":[\"V1\",\"V2\",\"V3\",\"V4\",\"V5\",\"V6\",\"V7\",\"V8\",\"V9\",\"V10\",\"V11\",\"V12\",\"V13\",\"V14\",\"V15\",\"V16\",\"V17\",\"V18\",\"V19\",\"V20\",\"V21\",\"V22\",\"V23\",\"V24\",\"V25\",\"V26\",\"V27\",\"V28\",\"V29\",\"V30\"],\"subtype_list\":[],\"asset_id\":\"\",\"properties\":\"\"}"}

type Circuit_config struct {
	asset_map map[string]json.RawMessage
}

//{"name":"F1","tanks":["T1","T2"],"frequency":"24","duration":"300","switching_flag":"","submit":""}
type Fuse_profile struct {
	Name string `json:"name"`
}

// {"name":"FuseTime","method":"Time-based","filters_present":true,"filter_frequency":"10","filter_duration":"180","tanks":["T1","T2"],"filter_switching_flag":true,"nfeed":"Fertigation","duration":"","agitator":true,"nduration":"10","pswitch":true,"psdelay":"10","wfmeter":true,"wfmetertype":"Hunter","hunter_kfactor":"1","hunter_offset":"5","submit":""}
//{"name":"Ptime","method":"Time-based","filters_present":true,"filter_frequency":"1","filter_duration":"100","tanks":"T1","filter_switching_flag":true,"nfeed":"MCB","duration":"500","agitator":true,"nduration":"10","pswitch":false,"psdelay":"","wfmeter":true,"wfmetertype":"Arad","hunter_kfactor":"","hunter_offset":"","submit":""}
type Circuit_usage struct {
	Name                string   `json:"name"`
	Method              string   `json:"method"`
	Nfeed               string   `json:"nfeed"`
	Duration            string   `json:"duration"`
	Agitator            bool     `json:"agitator"`
	Nduration           string   `json:"nduration"`
	Pswitch             bool     `json:"pswitch"`
	Wfmeter             bool     `json:"wfmeter"`
	Wfmetertype         string   `json:"wfmetertype"`
	Psdelay             string   `json:"psdelay"`
	Hunter_kfactor      string   `json:"hunter_kfactor"`
	Hunter_offset       string   `json:"hunter_offset"`
	Tanks               []string `json:"tanks"`
	Fuses_present       bool     `json:"filters_present"`
	Fuse_frequency      string   `json:"filter_frequency"`
	Fuse_duration       string   `json:"filter_duration"`
	Fuse_switching_flag bool     `json:"filter_switching_flag"`
}

// convert config to SMS format
func (p vendor) Get_init_config(cinfo *tcclient.Asset_info) []string {

	// '{"site_id":"eb9ff760-256a-11e9-86da-0242ac110004","site_name":"Mandya Farmer Site 1","asset_id":"4a9f4451-4625-11e9-acc0-0242ac110005","type_name":"Circuit","type_id":"c7377400-2847-11e9-92b1-97234f04ea82"}'

	var configs []string
	smsconfig, _ := Generate_sms_cmds("InitC", cinfo, nil, nil)
	if smsconfig != "" {
		log.Print("SMS Config message", smsconfig)
		configs = append(configs, smsconfig)
	}
	return (configs)
}

func (p vendor) Send_operation_cmd(controller *tcclient.Asset_info, operation string, ainfo *tcclient.Asset_info, einfo *tcclient.Entity_info, alist []tcclient.Asset_info, cstate *utils.Controller_state, encode_flag string) (*utils.Config_response, error) {

	var sms_config []string
	var cresp *utils.Config_response
	var resp utils.Controller_response
	var err error

	cresp = new(utils.Config_response)

	config := p.Get_operation_cmd(operation, ainfo, einfo, alist)
	if config == nil {
		err = errors.New("Couldn't perform Operation")
		return nil, err
	}
	resp.Cmds = nil
	sms_config = append(sms_config, config...)
	resp.Cid = controller.Id
	resp.Mobile = cstate.Mobile
	resp.Name = controller.Name
	resp.Desc = controller.Description

	tp := p.Get_transport_params()
	if tp.Xport_type == "sms" {
		// Call this when we are not doing incomplete processing
		cmds, count, start_seq_num := p.Pack_messages(sms_config, tp.Xport_max_len)

		for _, cmd := range cmds {
			var mesg string
			var cmd1 []byte
			var ok bool

			log.Printf("%s", cmd)
			//log.Printf("%s", hex.Dump(cmd))
			if encode_flag == "1c" {
				cmd1 = Encode_data([]byte(cmd), "1c")
				fmt.Printf("String :%s", string(cmd1))
			} else {
				cmd1 = []byte(cmd)
			}

			mobile_num := cstate.Mobile
			retries := 1
			for retries > 0 {
				ok, mesg, err = smsprovider.Send_sms(saas_string, mobile_num, string(cmd1), start_seq_num)
				resp.Cmds = append(resp.Cmds, string(cmd))
				resp.Replies = append(resp.Replies, mesg)
				if !ok {
					retries--
					if retries == 0 {
						log.Println("Failed to send SMS to controller : ", mobile_num, mesg)
						resp.Status = http.StatusNotFound
						cresp.Response = append(cresp.Response, resp)
						return cresp, err
					}
					continue
				}
				err = nil
				break
			}
			count--
			if count == 0 {
				break
			}
		}
	}
	if err != nil {
		return nil, err
	}
	resp.Status = http.StatusOK
	cresp.Response = append(cresp.Response, resp)
	if operation == "operation:ResetC" {
		cstate.Initialized = true
	}
	return cresp, nil
}

// convert config to SMS format
func (p vendor) Get_operation_cmd(operation string, ainfo *tcclient.Asset_info, einfo *tcclient.Entity_info, alist []tcclient.Asset_info) []string {

	// '{"site_id":"eb9ff760-256a-11e9-86da-0242ac110004","site_name":"Mandya Farmer Site 1","asset_id":"4a9f4451-4625-11e9-acc0-0242ac110005","type_name":"Circuit","type_id":"c7377400-2847-11e9-92b1-97234f04ea82"}'

	var configs []string
	smsconfig, ok := Generate_sms_cmds(operation, ainfo, einfo, &alist)
	if ok && smsconfig != "" {
		log.Print("SMS Config message", smsconfig)
		configs = append(configs, smsconfig)
		return (configs)
	} else {
		log.Println("Operation message nil")
		return nil
	}
}

var pipeline_asset_type_id string
var valve_group_asset_type_id string
var valve_asset_type_id string
var saas_asset_types_map map[string]*[]tcclient.Asset_type_info

func (p vendor) Getenv() *utils.Plugin_env {
	env := new(utils.Plugin_env)
	env.Saas = saas_string
	user_str := saas_string + "_TCUSER"
	user_pwd := saas_string + "_TCPASSWD"
	root_eid := saas_string + "_ROOTEID"
	env.User = utils.Getenv(user_str, "")
	env.Password = utils.Getenv(user_pwd, "")
	env.Root_entity_id = utils.Getenv(root_eid, "")
	return (env)
}

var saas_string string = "saasv"
var saas_name utils.Saas_entity_name = "saasv"

func (p vendor) SetRoutes(router *mux.Router) *mux.Router {
	s := router.PathPrefix("/saasv/v1/").Subrouter()
	s.HandleFunc("/hello/", Hello).Methods("GET")
	return router
}

var Hello = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	coptions := make([]Controller_options, 0)
	for _, cp := range utils.Controller_options_map() {
		coption := cp.(*Controller_options)
		coptions = append(coptions, *coption)
	}
	resp_json, _ := json.Marshal(coptions)
	w.Write([]byte(string(resp_json)))
})

func (p vendor) Init(handle interface{}) int {

	root_eid_str := saas_string + "_ROOTEID"
	root_entity_id := utils.Getenv(root_eid_str, "be7afad0-9ee9-11e9-a550-c7f00d9bf1ef")
	smsp_string := saas_string + "_TEXTLOCALSMSPREFIX"
	smsprefix := utils.Getenv(smsp_string, "")
	if smsprefix == "" {
		log.Println("LocalSMS prefix env not defined", smsp_string)
		return -1
	}

	saas_name.Get_list_asset_types(handle, root_entity_id)
	// TBD - make all util functions interface specific
	pipeline_asset_type_id = (saas_name.Find_asset_type("Circuit")).Id
	valve_group_asset_type_id = (saas_name.Find_asset_type("Switch Group")).Id
	valve_asset_type_id = (saas_name.Find_asset_type("Switch")).Id
	log.Println("Asset type ids:", pipeline_asset_type_id, valve_group_asset_type_id, valve_asset_type_id)
	smsprovider.Register_cb(saas_string, smsprefix, p.Parse_response)
	return 0
}

func (p vendor) Send_full_config(site *tcclient.Entity_info, controller *tcclient.Asset_info, profiles *[]tcclient.Config_profile, assets *[]tcclient.Asset_info, user_config_flag bool, cstate *utils.Controller_state, incremental bool, encode_flag string) (*utils.Config_response, error) {

	var incomplete bool = false
	var cresp *utils.Config_response
	var resp utils.Controller_response
	var start_seq_num uint16
	var count uint8
	var start int = 0
	var cmds, sms_config2 []string
	var err error

	if cstate.Mobile == "" {
		log.Println("Controller Mobile number not configured. Aborting sync")
		err = errors.New("Controller Mobile number not configured. Aborting sync")
		return nil, err
	}
	if user_config_flag {
		if cstate.Last_user_config_pos >= 0 && cstate.Progress_flag == "Incomplete TimedOut" {
			incomplete = true
			start = cstate.Last_user_config_pos
			sms_config2 = cstate.Sms_user_config
		}
	} else {
		if cstate.Last_config_pos >= 0 && cstate.Progress_flag == "Incomplete TimedOut" {
			incomplete = true
			start = cstate.Last_config_pos
			sms_config2 = cstate.Sms_config
		}
	}

	resp.Cmds = nil

	//_ = Check_Init_state(handle, sync.Site_id, controller.Id)
	resp.Cid = controller.Id
	resp.Mobile = cstate.Mobile
	resp.Name = controller.Name
	resp.Desc = controller.Description
	cresp = new(utils.Config_response)

	cresp.Response = make([]utils.Controller_response, 0, 10)

	if !incomplete {
		sms_config2 = p.Get_full_config(site, controller, profiles, assets, user_config_flag, cstate, incremental)
		if len(sms_config2) == 0 {
			log.Println("No config to push. Aborting sync")
			err = errors.New("No config to push")
			resp.Status = http.StatusNotFound
			cresp.Response = append(cresp.Response, resp)
			return nil, err
		}
	}

	tp := p.Get_transport_params()

	pos := 0
	if tp.Xport_type == "sms" {
		cmds, count, start_seq_num = p.Pack_messages(sms_config2, tp.Xport_max_len)

		now := time.Now().UTC()
		log.Printf("Starting Transport : count:%d, to %s\n", count, cstate.Mobile)

		for _, cmd := range cmds {
			//log.Printf("%s", hex.Dump(cmd))
			var mesg string
			var cmd1 []byte
			var ok bool

			if pos < start {
				pos++
				count--
				fmt.Printf("Skipping synced config %d:%s\n", pos, cmd)
				continue
			}

			log.Printf("%s\n", cmd)
			mobile_num := cstate.Mobile
			if encode_flag == "1c" {
				cmd1 = Encode_data([]byte(cmd), "1c")
			} else {
				cmd1 = []byte(cmd)
			}
			retries := 1
			for retries > 0 {
				ok, mesg, err = smsprovider.Send_sms(saas_string, mobile_num, string(cmd1), start_seq_num)
				resp.Cmds = append(resp.Cmds, string(cmd))
				resp.Replies = append(resp.Replies, mesg)
				if !ok {
					retries--
					if retries == 0 {
						log.Println("Failed to send SMS to controller : ", mobile_num)
						resp.Status = http.StatusNotFound
						//cresp.Response = append(cresp.Response, resp)
						//return
						break
					}
					continue
				}
				err = nil
				//result += string(cmd) + " ======== "
				break
			}
			if !ok {
				break
			}
			count--
			pos++
			time_taken := time.Since(now).Round(time.Second).Seconds()
			if count == 0 {
				log.Printf("Complete. Time taken:%f/%d, commands left:%d \n", time_taken, tp.Xport_max_timeout, count)
				break
			}
			log.Printf("Time taken:%f/%d, commands left:%d \n", time_taken, tp.Xport_max_timeout, count)
			if int(time_taken) > tp.Xport_max_timeout {
				log.Printf("Time limit execeeded\n")
				break
			}
		}
	}

	if count > 0 {
		cresp.Statustext = "Tryagain:" + err.Error()
	} else {
		cresp.Statustext = "Done"
	}

	resp.Status = http.StatusOK
	if resp.Cmds != nil {
		if !user_config_flag {
			cstate.Last_config_pos = int(pos)

			if incomplete && err == nil || !incomplete {
				// Merge only if new commands are generated
				cstate.Sms_config = cmerge(cstate.Sms_config, sms_config2, -1)
			}
			//cstate.Sms_config = utils.Merge(cstate.Sms_config, cmds, 2)
			if count == 0 {
				cstate.Progress_flag = "Ok"
				cstate.Configured = true
				cstate.Last_config_pos = -1
				cstate.Last_sync_time = time.Now().UTC().Unix()
			} else if err != nil {
				if strings.Contains(err.Error(), "Timed") {
					cstate.Progress_flag = "Incomplete TimedOut"
				} else {
					cstate.Progress_flag = "Incomplete with Error"
				}
			} else {
				cstate.Progress_flag = "Incomplete"
			}
		} else {
			cstate.Last_user_config_pos = int(pos)

			if incomplete && err == nil || !incomplete {
				// Merge only if new commands are generated
				cstate.Sms_user_config = cmerge(cstate.Sms_user_config, sms_config2, -1)
				cstate.Last_usersync_time = time.Now().UTC().Unix()
			}

			//cstate.Sms_user_config = utils.Merge(cstate.Sms_user_config, cmds, 2)
			if count == 0 {
				cstate.Progress_flag = "Ok"
				cstate.Last_user_config_pos = -1
				cstate.Last_usersync_time = time.Now().UTC().Unix()
			} else if err != nil {
				if strings.Contains(err.Error(), "Timed") {
					cstate.Progress_flag = "Incomplete TimedOut"
				} else {
					cstate.Progress_flag = "Incomplete with Error"
				}
			} else {
				cstate.Progress_flag = "Incomplete"
			}
		}
		cresp.Response = append(cresp.Response, resp)
		fmt.Println("Controller State:", cstate)
	}
	return cresp, err
}

func (p vendor) Get_full_config(site *tcclient.Entity_info, controller *tcclient.Asset_info, profiles *[]tcclient.Config_profile, assets *[]tcclient.Asset_info, user_config_flag bool, cstate *utils.Controller_state, incremental bool) []string {
	var sms_config []string
	for iter := 0; iter < 2; iter++ {
		var group_flag string = ""

		if iter == 0 {
			group_flag = "group"
			if !user_config_flag {
				continue
			}
		}

		if user_config_flag {
			fmt.Println("Starting usersync. Last synced :", time.Unix(cstate.Last_usersync_time, 0), cstate.Last_usersync_time)
		} else {
			fmt.Println("Starting sync. Last synced :", time.Unix(cstate.Last_sync_time, 0), cstate.Last_sync_time)
		}

		for _, profile := range *profiles {
			if user_config_flag {
				if profile.Profile_type == "config" {
					continue
				}
				if incremental {
					if profile.Apply_time < cstate.Last_usersync_time {
						fmt.Printf("Skipping %s, apply=%s, secs=%d\n", profile.Name, time.Unix(profile.Apply_time, 0), profile.Apply_time)
						continue
					} else {
						fmt.Printf("Processing %s, apply=%s, secs=%d\n", profile.Name, time.Unix(profile.Apply_time, 0), profile.Apply_time)
					}
				}

				config := p.Get_config(site, controller, profile, profiles, assets, group_flag, cstate)
				if config != nil {
					sms_config = append(sms_config, config...)
				}
				/*
					config = Get_config(controller, profile, profiles, &assets, "", cstate)
					if config != nil {
						sms_config = append(sms_config, config...)
					}
				*/
			} else {
				if profile.Profile_type != "owner_config" &&
					profile.Profile_type != "group_config" {

					if incremental {
						if profile.Apply_time < cstate.Last_sync_time {
							fmt.Printf("Skipping %s, apply=%s, secs=%d\n", profile.Name, time.Unix(profile.Apply_time, 0), profile.Apply_time)
							continue
						} else {
							fmt.Printf("Processing %s, apply=%s, secs=%d\n", profile.Name, time.Unix(profile.Apply_time, 0), profile.Apply_time)
						}
					}
					config := p.Get_config(site, controller, profile, profiles, assets, "", cstate)
					if config == nil {
						continue
					}
					sms_config = append(sms_config, config...)
				}
			}
		}
	}
	if len(sms_config) == 0 {
		return nil
	}

	num_words := 2
	if user_config_flag {
		num_words = 0
	}
	sms_config2 := utils.Remove_duplicates(sms_config, num_words)
	return (sms_config2)
}

//
// convert config to SMS format
func (p vendor) Get_config(site *tcclient.Entity_info, controller *tcclient.Asset_info, profile tcclient.Config_profile, profile_list *[]tcclient.Config_profile, alist *[]tcclient.Asset_info, group_flag string, cstate *utils.Controller_state) []string {

	// '{"site_id":"eb9ff760-256a-11e9-86da-0242ac110004","site_name":"Mandya Farmer Site 1","asset_id":"4a9f4451-4625-11e9-acc0-0242ac110005","type_name":"Circuit","type_id":"c7377400-2847-11e9-92b1-97234f04ea82"}'

	var configs []string
	var smsconfig string = ""
	var done, more bool
	var found bool = false
	var ainfo *tcclient.Asset_info

	if len(profile.Apply_to) > 0 {
		sapply_to := utils.Sort_by_asset_name(profile.Apply_to)
		//for asset_id, _ := range profile.Apply_to {
		for _, asset := range sapply_to {
			if controller.Id == asset.Key {
				found = true
				ainfo = controller
			} else {
				found, ainfo = utils.Find_asset(asset.Key, alist)
			}
			if found {
				if profile.Profile_type_id == ainfo.Asset_type {
					smsconfig, done, more = Generate_sms_config(site, controller, profile, profile_list, ainfo, alist, smsconfig, group_flag, cstate)
					if smsconfig != "" {
						log.Print("SMS Config message", smsconfig)
						configs = append(configs, smsconfig)
					}
					if done {
						break
					}
					if !more {
						smsconfig = ""
					}
				}
			}
		}
	} else {
		// Handle config for profiles with no assets associated
		if profile.Cschema_name == "Switching Definition Profile" || profile.Cschema_name == "Switching Program" {
			smsconfig, done, more = Generate_sms_config(site, controller, profile, profile_list, nil, alist, smsconfig, group_flag, cstate)
			if smsconfig != "" {
				log.Print("SMS Config message", smsconfig)
				configs = append(configs, smsconfig)
			}
		}
	}

	// get motor-config profile
	// Get assets from apply_list matching site_id and
	// parent_asset(include in apply_set) == cid
	// If assets = null, then return;

	// Get controlled assets(cid) matching asset_type
	// For each asset, create config in SMS format
	// Store the SMS config in memory
	// Return handle to caller
	return (configs)
}

type Switching_definition_profile struct {
	//{\"dname\":\"D2\",\"method\":\"Time-based\",\"hours\":\"4\",\"minutes\":\"59\",\"nutreq\":true,\"soak\":\"200\",\"gap\":\"11\",\"pdelay\":\"11\",\"c1volflag\":false,\"c2volflag\":false,\"c3volflag\":false,\"c4volflag\":false,\"c1time\":\"300\",\"c2time\":\"400\",\"c3time\":\"\",\"c4time\":\"\"
	Name      string `json:"name"`
	Dname     string `json:"dname"`
	Method    string `json:"method"`
	Hours     string `json:"hours"`
	Minutes   string `json:"minutes"`
	Volume    string `json:"volume"`
	Nutreq    bool   `json:"nutreq"`
	Soak      string `json:"soak"`
	Gap       string `json:"gap"`
	Pdelay    string `json:"pdelay"`
	C1volflag bool   `json:"c1volflag"`
	C2volflag bool   `json:"c2volflag"`
	C3volflag bool   `json:"c3volflag"`
	C4volflag bool   `json:"c4volflag"`
	C1time    string `json:"c1time"`
	C1vol     string `json:"c1vol"`
	C2time    string `json:"c2time"`
	C2vol     string `json:"c2vol"`
	C3time    string `json:"c3time"`
	C3vol     string `json:"c3vol"`
	C4time    string `json:"c4time"`
	C4vol     string `json:"c4vol"`
}

type Switching_program struct {
	Name   string          `json:"name"`
	Dname  string          `json:"dname"`
	Porder []Program_order `json:"porder"`
}

type Program_order struct {
	Dprofile string `json:"Switching Definition Profile"`
	Asset    string `json:"Asset"`
}

type Switching_profile struct {
	Name    string `json:"name"`
	Method  string `json:"method"`
	Hours   string `json:"hours"`
	Minutes string `json:"minutes"`

	Volume string `json:"volume"`
}

type MCB_channel_profile struct {
	//	{"name":"Vtime","method":"Time-based","hours":"8","minutes":"0","volume":""}
	Name            string `json:"name"`
	Doser_present   bool   `json:"doser_present"`
	Doser_full_time bool   `json:"doser_full_time"`
	Pdelay          string `json:"pdelay"`
	Gap             string `json:"gap"`
	Dh1             string `json:"dh1"`
	Dh2             string `json:"dh2"`
	Dh3             string `json:"dh3"`
	Dh4             string `json:"dh4"`
}
type Controller_options struct {
	Name       string `json:"name"`
	Sdatetime  bool   `json:"sdatetime"`
	Multisched bool   `json:"multisched"`
	Rsensor    bool   `json:"rsensor"`
	Monitor    bool   `json:"monitor"`
}

type Switching_advanced_schedule struct {
	Name    string     `json:"name"`
	Sname   string     `json:"sname"`
	Caltime time.Time  `json:"caltime"`
	Program []prog_ref `json:"program"`
}

type prog_ref struct {
	Iprogram string `json:"Switching Program"`
}

type Switching_schedule struct {
	//{"name":"Daily","manner":"Scheduled","days":"1","hours":"","minutes":""}
	Name    string `json:"name"`
	Manner  string `json:"manner"`
	Days    string `json:"days"`
	Hours   string `json:"hours"`
	Minutes string `json:"minutes"`
	Sdays   string `json:"sdays"`
	Stime   string `json:"stime"`
}

type Nutrient_profile struct {
	//{"name":"N1","soak":"10","wnutrient":"10","rinse":"10","cycles":"2"}
	Name      string `json:"name"`
	Nver      string `json:"nver"`
	Soak      string `json:"soak"`
	Cycles    string `json:"cycles"`
	C1volflag bool   `json:"c1volflag"`
	C2volflag bool   `json:"c2volflag"`
	C3volflag bool   `json:"c3volflag"`
	C4volflag bool   `json:"c4volflag"`
	C1time    string `json:"c1time"`
	C1vol     string `json:"c1vol"`
	C2time    string `json:"c2time"`
	C2vol     string `json:"c2vol"`
	C3time    string `json:"c3time"`
	C3vol     string `json:"c3vol"`
	C4time    string `json:"c4time"`
	C4vol     string `json:"c4vol"`
	Wnutrient string `json:"wnutrient"`
	Rinse     string `json:"rinse"`
}

func Generate_sms_cmds(conf_type string, info interface{}, einfo *tcclient.Entity_info, list interface{}) (string, bool) {
	var ok bool = true
	var config string = ""

	switch conf_type {
	case "operation:SMSconf":
		{
			prefix_env := saas_string + "_TEXTLOCALSMSPREFIX"
			prefix := utils.Getenv(prefix_env, "9QTND")
			sender_env := saas_string + "_TEXTLOCALSENDER"
			sender := utils.Getenv(sender_env, "AVNIJL")
			number := utils.Getenv("TEXTLOCALTELE", "+919220592205")

			if number != "" && prefix != "" && sender != "" {
				config += "*AE "
				config += sender + " "
				config += sender + " "
				config += number + " "
				config += prefix + " "
			}

			if einfo != nil {
				phone_list := ""
				num_phones := 0
				for _, phone := range einfo.Phones {
					if phone != "" {
						num_phones++
						phone_list += strconv.Itoa(num_phones) + " "
						phone_list += phone + " "
						if num_phones == 3 {
							break
						}
					}
				}
				if num_phones > 0 {
					config += "*UP " + strconv.Itoa(num_phones) + " "
					config += phone_list
					config += strconv.Itoa(num_phones) + " "
				}
			}
		}
		break

	case "operation:Start":
		{
			ainfo, ok := info.(*tcclient.Asset_info)
			if ok && ainfo.Collmap != "" {
				atype_info := saas_name.Find_asset_type_by_id(ainfo.Asset_type)
				if atype_info.Name == "Circuit" {
					//config += "*BI " + ainfo.Name + " " + "G0" + " "
					config += "*BI " + ainfo.Name + " " + "G2" + " "
				}
			}
		}
		break

	case "operation:Stop":
		{
			ainfo, ok := info.(*tcclient.Asset_info)
			if ok && ainfo.Collmap != "" {
				atype_info := saas_name.Find_asset_type_by_id(ainfo.Asset_type)
				if atype_info.Name == "Circuit" {
					config += "*EI " + ainfo.Name + " "
				}
			}
		}
		break

	case "operation:Reset":
		{
			ainfo, ok := info.(*tcclient.Asset_info)
			if ok && ainfo.Collmap != "" {
				atype_info := saas_name.Find_asset_type_by_id(ainfo.Asset_type)
				if atype_info.Name == "Circuit" {
					config += "*RI " + ainfo.Name + " "
				}
			}
		}
		break

	case "operation:Reset Controller":
		{
			_, ok := info.(*tcclient.Asset_info)
			if ok {
				config += "*EF 6 "
			}
		}
		break

	case "getoperation:Get Voltage":
		{
			_, ok := info.(*tcclient.Asset_info)
			if ok {
				config += "*GV "
			}
		}
		break

	case "getoperation:Get History":
		{
			_, ok := info.(*tcclient.Asset_info)
			if ok {
				config += "*GH "
			}
		}
		break
	case "operation:Stop Switching":
		fallthrough
	case "operation:Stop Nutrient":
		{
			ainfo, ok := info.(*tcclient.Asset_info)
			if !ok {
				log.Print("Assert Error")
				return "", false
			}
			alist, _ := list.(*[]tcclient.Asset_info)
			log.Println("Gen SMS CMD", conf_type, alist)
			found, misc, _ := utils.Find_asset_collection(ainfo.Name, ainfo.Asset_type, alist, pipeline_asset_type_id)
			if found {
				for _, pinfo := range misc {
					config += "*RV " + pinfo.Name + " "
					config += ainfo.Name + " "
					if conf_type == "operation:Stop Nutrient" {
						config += "w1 n0 "
					} else {
						config += "w0 "
					}
				}
			} else {
				log.Println("Not found :", ainfo, misc)
			}
		}
		break

	case "operation:Reset History":
		{
			ainfo, ok := info.(*tcclient.Asset_info)
			if ok {
				atype_info := saas_name.Find_asset_type_by_id(ainfo.Asset_type)
				log.Println("Gen SMS CMD", conf_type, atype_info, ainfo)
				switch atype_info.Name {
				case "Pump":
					config += "*MH " + ainfo.Name
					break
				case "Circuit":
					config += "*PH " + ainfo.Name
					break
				case "Switch":
					config += "*VH " + ainfo.Name
					break
				}
			}
		}
		break
	}
	return config, ok
}

func Generate_sms_config(site *tcclient.Entity_info, controller *tcclient.Asset_info, profile tcclient.Config_profile, plist *[]tcclient.Config_profile, info interface{}, list interface{}, config string, group_flag string, cstate *utils.Controller_state) (string, bool, bool) {
	//var motor_config map[string]*json.RawMessage
	var conf_type string = profile.Cschema_name
	var config_json string = profile.Config
	var mconfig Pump_config
	var vconfig Switch_config
	var vc_prof MCB_channel_profile
	var pusage Circuit_usage
	var pconfig map[string]string
	//var config string = ""
	var nprof Nutrient_profile
	var virig Switching_profile
	var asset_list utils.Collection_list
	var sched Switching_schedule
	var done bool = false
	var more bool = false

	switch conf_type {

	case "Controller Options Profile":
		{
			var coptions *Controller_options
			ainfo, ok := info.(*tcclient.Asset_info)

			if !ok {
				return "", false, more
			} else {
				coptions = new(Controller_options)
				err := json.Unmarshal([]byte(config_json), coptions)
				if err != nil {
					log.Print("Config format Error:", config_json)
					return "", true, more
				}
			}

			curr_coptions, cok := utils.Controller_options(ainfo.Id).(*Controller_options)
			utils.Set_controller_options(ainfo.Id, coptions)
			coptions_json, _ := json.Marshal(coptions)
			cstate.Options = string(coptions_json)

			//controller_options_map[ainfo.Id] = coptions

			if coptions.Monitor {
				if coptions.Monitor == true {
					url, url1 := utils.Collect_url(saas_string, site.Name)
					config += "*SAASUR " + url + " ;"
					config += "*SAASUC " + url1 + ainfo.Id + " "
					config += "*SAASDE 2 1 " + ";" /* Suggest where this can be split */
				} else if coptions.Monitor == false {
					config += "*SAASDD "
				}
			} else if cok && curr_coptions.Monitor {
				config += "*SAASDD "
			}

			if coptions.Sdatetime {
				if coptions.Sdatetime == true {
					config += "*SAASIS T "
				} else if coptions.Sdatetime == false {
					config += "*SAASOS T "
				}
			} else if coptions.Multisched {
				if coptions.Multisched == true {
					config += "*SAASIS S "
				} else if coptions.Multisched == false {
					config += "*SAASOS S "
				}
			} else if cok {
				if curr_coptions.Sdatetime {
					config += "*SAASOS T "
				}
				if curr_coptions.Multisched {
					config += "*SAASOS S "
				}
			}

			if coptions.Rsensor {
				if coptions.Rsensor == true {
					config += "*SAASIS S1 "
				} else if coptions.Rsensor == false {
					config += "*SAASOS S1 "
				}
			} else if cok && curr_coptions.Rsensor {
				config += "*SAASOS S1 "
			}

		}
		break

	case "Pump Config Profile":
		{
			ainfo, ok := info.(*tcclient.Asset_info)
			if ok {
				err := json.Unmarshal([]byte(config_json), &mconfig)
				if err != nil {
					log.Print("Config format Error:", config_json)
					return "", true, more
				}
			}
			config += "*SASSAC "
			config += ainfo.Name + " "
			switch string(mconfig.Mtype) {
			case "Water Supply":
				config += "T2 "
				if mconfig.Tank == "Tank1" {
					config += "1 "
				} else if mconfig.Tank == "Tank2" {
					config += "2 "
				}
				break
			case "Switching":
				config += "T1 "
				break
			case "Fogger":
				config += "T3 "
				break
			case "Doser":
				config += "T4 "
				break
			}
			config += "H" + mconfig.Capacity + " "
			if mconfig.Vmonitor {
				config += "V1 " + mconfig.Vmin + " " + mconfig.Vmax + " "
			} else {
				config += "V0 "
			}
			if mconfig.Cmeasure {
				config += "C1 "
			} else {
				config += "C0 "
			}
			switch mconfig.Wsource {
			case "Bore":
				config += "W1 "
				break
			case "River":
				config += "W4 "
				break
			case "Open Well":
				config += "W2 "
				break
			case "Pond":
				config += "W3 "
				break
			case "Fertigation Tank":
				config += "W5 "
				break
			}
			config += mconfig.Wd + " "

			if ainfo.Mobile != "" {
				config += "R" + ainfo.Mobile + " "
			}
		}
		break
	case "Switch Config Profile":
		{
			err := json.Unmarshal([]byte(config_json), &vconfig)
			if err != nil {
				log.Print("Config format Error:", config_json)
				return "", true, more
			}
			ainfo, ok := info.(*tcclient.Asset_info)
			if !ok {
				log.Print("Assert Error")
				return "", false, more
			}
			config += "*SASSAC "
			config += ainfo.Name + " "
			switch vconfig.Type {

			case "Switching":
				config += "T2 "
				// Not supported
				//	config += "D" + vconfig.Swdelay
				if vconfig.Connection == "Double" {
					config += "D2 "
				} else {
					config += "D1 "
				}
				break
			case "Gate Switch":
				config += "T1 "
				break
			case "MCB":
				config += "T3 "
				break
			case "Fuse":
				config += "T4 "
				break
			case "Fogger":
				config += "T5 "
				break
			}
			if vconfig.Type == "Switching" {
				switch vconfig.Stype {
				case "Black":
					config += "S1 "
					break
				case "Red":
					config += "S2 "
					break
				case "Alluvial":
					config += "S3 "
					break
				case "Laterite":
					config += "S4 "
					break
				case "Mountain":
					config += "S5 "
					break
				case "Desert":
					config += "S6 "
					break
				}
				switch vconfig.Stexture {
				case "Clay":
					config += "T1 "
					break
				case "Silty":
					config += "T2 "
					break
				case "Sandy":
					config += "T3 "
					break
				case "Chalk":
					config += "T4 "
					break
				case "Peat":
					config += "T5 "
					break
				case "Loam":
					config += "T6 "
					break
				}
			}
			/*
				// Not supported
				config += "C" + vconfig.Croptype + " "

				if vconfig.Sensorid != "" {
					config += "S" + vconfig.Sensorid + " "
				}
			*/
		}
		break

	case "MCB Channel Profile":
		{
			err := json.Unmarshal([]byte(config_json), &vc_prof)
			if err != nil {
				log.Print("MCB profile format Error:", err, config_json)
				return "", true, more
			}

			var vc_len int = 0
			alist, _ := list.(*[]tcclient.Asset_info)
			for vc_id, vcdata := range profile.Apply_to {
				var adata apply_data
				found, ainfo := utils.Find_asset(vc_id, alist)
				if found {
					vcinfo := vcdata.(string)
					err := json.Unmarshal([]byte(vcinfo), &adata)
					if err == nil {
						if adata.Parent_asset_id == ainfo.Parent_asset_id {
							vc_len++
						}
					}
				}
			}

			if vc_len == 0 {
				log.Print("No MCB channels ", conf_type+":", profile.Name)
				return "", true, more
			}
			config = "*VC "
			config += "C" + strconv.Itoa(vc_len) + " "
			if vc_prof.Doser_present {
				config += "B1 "
				if vc_prof.Doser_full_time {
					config += "O1 "
				} else {
					config += "O0 "
				}
			} else {
				config += "B0 "
			}

			switch vc_len {
			case 4:
				config += "D " + vc_prof.Dh1 + " " + vc_prof.Dh2 + " " + vc_prof.Dh3 + " " + vc_prof.Dh4 + " "
				break
			case 3:
				config += "D " + vc_prof.Dh1 + " " + vc_prof.Dh2 + " " + vc_prof.Dh3 + " "
				break
			case 2:
				config += "D " + vc_prof.Dh1 + " " + vc_prof.Dh2 + " "
				break
			case 1:
				config += "D " + vc_prof.Dh1 + " "
				break
			}

			if vc_prof.Gap != "0" {
				config += "G" + vc_prof.Gap + " "
			}

			config += "T" + vc_prof.Pdelay + " "
			done = true
		}
		break

	case "Circuit Config Profile":
		{
			var vc_asset_list utils.Collection_list
			ainfo, ok := info.(*tcclient.Asset_info)
			if !ok {
				log.Print("Assert Error")
				return "", false, more
			}
			err := json.Unmarshal([]byte(ainfo.Collmap), &pconfig)
			if err != nil {
				log.Print("Collection Map format Error:", ainfo.Collmap)
				return "", false, more
			} else {
				asset_list.Chosen_list = make([]string, 32)
				vc_asset_list.Chosen_list = make([]string, 32)
				for asset_string, info := range pconfig {
					if asset_string == "Pump" {
						err := json.Unmarshal([]byte(info), &asset_list)
						if err != nil {
							log.Print("Circuit Config format Error:", err, info)
							return "", false, more
						}
					}
					if asset_string == "MCB" {
						err := json.Unmarshal([]byte(info), &vc_asset_list)
						if err != nil {
							log.Print("Circuit Config format Error:", err, info)
							return "", false, more
						}
					}
				}
			}

			err = json.Unmarshal([]byte(config_json), &pusage)
			if err != nil {
				log.Print("Config format Error:", err, config_json)
				return "", true, more
			}
			config += "*SASSAC "
			config += ainfo.Name + " "
			for _, motor := range asset_list.Chosen_list {
				config += motor + " "
			}
			if pusage.Fuses_present {
				config += "F1 "
				config += pusage.Fuse_frequency + " "
				config += pusage.Fuse_duration + " "
				if pusage.Fuse_switching_flag {
					config += "1 "
				} else {
					config += "0 "
				}
				for _, tank := range pusage.Tanks {
					config += string(tank) + " "
				}
			} else {
				config += "F0 "
			}

			if pusage.Nfeed == "Fertigation" {
				config += "R1 "
				if pusage.Agitator {
					config += "1 "
					config += pusage.Nduration + " "
				} else {
					config += "0 "
				}
			} else if pusage.Nfeed == "MCB" {
				num_vc := len(vc_asset_list.Chosen_list)
				if num_vc > 0 {
					config += "U1 "
					first := strings.Trim(vc_asset_list.Chosen_list[0], "VC")
					last := strings.Trim(vc_asset_list.Chosen_list[num_vc-1], "VC")
					config += first + " 1 " + last + " "
				} else {
					config += "U0 "
				}

				/*
					config += pusage.Duration + " "
					if pusage.Agitator {
						config += " 1 "
						config += pusage.Nduration + " "
					} else {
						config += " 0 "
					}
				*/
			} else if pusage.Nfeed == "Doser" {
				config += "D "
			} else {
				config += "R0 "
			}

			if pusage.Pswitch {
				config += "S1 "
				config += pusage.Psdelay + " "
			} else {
				config += "S0 "
			}
			if pusage.Wfmeter {
				config += "W1 1 "
				/*
					switch pusage.Wfmetertype {
					case "Arad":
						config += "1 "
						break
					case "Hunter":
						config += "2 "
						config += pusage.Hunter_kfactor + " "
						config += pusage.Hunter_offset + " "
						break
					}
				*/
			} else {
				config += "W0 "
			}
		}
		break

	case "Basic Schedule":
		{
			if group_flag == "group" {
				return "", true, more
			}
			err := json.Unmarshal([]byte(config_json), &sched)
			if err != nil {
				log.Print("Config format Error:", config_json)
				return "", true, more
			}
			ainfo, ok := info.(*tcclient.Asset_info)
			if !ok {
				log.Print("Assert Error")
				return "", false, more
			}
			if sched.Manner == "Continuous" {
				config = "*BI "
				config += ainfo.Name + " "
				config += "G0" + " "
			} else if sched.Manner == "Interval Based" {
				config = "*BI "
				config += ainfo.Name + " "
				config += "G1" + " " + sched.Days + " " + sched.Hours + " " + sched.Minutes + " "
			} else if sched.Manner == "Scheduled" {
				loc, _ := time.LoadLocation("India/Delhi")
				coptions, ok := utils.Controller_options(controller.Id).(*Controller_options)
				if ok {
					if !coptions.Sdatetime {
						return "", true, more
					}
				} else {
					log.Print("Controller Options not configured :", config_json)
					return "", false, more
				}
				config = "*BI "
				config += ainfo.Name + " "
				//stime := strings.Split(sched.Stime, ":")
				//log.Println("Adding schedule ", sched.Sdays, stime[0], stime[1])
				//config += "G1" + " " + sched.Sdays + " " + stime[0] + " " + stime[1] + " "
				t, _ := time.ParseInLocation(time.RFC3339, sched.Stime, loc)
				log.Println("Adding schedule ", sched.Sdays, t.Hour(), t.Minute())
				config += "G1" + " " + sched.Sdays + " " + strconv.Itoa(t.Hour()) + " " + strconv.Itoa(t.Minute()) + " "
			} else if sched.Manner == "Manual Start" {
				config = "*RI "
				config += ainfo.Name + " "
			} else if sched.Manner == "Manual Stop" {
				config = "*EI "
				config += ainfo.Name + " "
			}
		}
		break

	case "Switching Definition Profile":
		{
			if group_flag == "group" {
				return "", true, more
			}
			coptions, ok := utils.Controller_options(controller.Id).(*Controller_options)
			if ok {
				if !coptions.Multisched {
					return "", true, more
				}
			} else {
				log.Print("Controller Options not configured :", config_json)
				return "", true, more
			}

			defconfig := new(Switching_definition_profile)
			err := json.Unmarshal([]byte(config_json), &defconfig)
			if err != nil {
				log.Print("Config format Error:", config_json)
				return "", true, more
			}

			config = "*ID " + defconfig.Dname + " "
			var time_based bool = true
			if defconfig.Method == "Time-based" {
				config += "T" + defconfig.Hours + " " + defconfig.Minutes + " "
			} else if defconfig.Method == "Volume-based" {
				config += "V" + defconfig.Volume + " "
				time_based = false
			}

			if !defconfig.Nutreq {
				config += "N0" + " "
			} else {
				config += "N1" + " S" + defconfig.Soak + " "
				if time_based {
					config += "T "
				} else {
					config += "V "
				}

				if defconfig.Gap != "" {
					config += "G" + defconfig.Gap + " "
				} else {
					config += "G1 "
				}
				if defconfig.Pdelay != "" {
					config += "S" + defconfig.Pdelay + " "
				} else {
					config += "S0 "
				}

				if defconfig.C1volflag {
					if defconfig.C1vol != "" {
						config += "T1 " + defconfig.C1vol + " "
					}
				} else {
					if defconfig.C1time != "" {
						config += "T1 " + defconfig.C1time + " "
					}
				}
				if defconfig.C2volflag {
					if defconfig.C2vol != "" {
						config += "T2 " + defconfig.C2vol + " "
					}
				} else {
					if defconfig.C2time != "" {
						config += "T2 " + defconfig.C2time + " "
					}
				}
				if defconfig.C3volflag {
					if defconfig.C3vol != "" {
						config += "T3 " + defconfig.C3vol + " "
					}
				} else {
					if defconfig.C3time != "" {
						config += "T3 " + defconfig.C3time + " "
					}
				}
				if defconfig.C4volflag {
					if defconfig.C4vol != "" {
						config += "T4 " + defconfig.C4vol + " "
					}
				} else {
					if defconfig.C4time != "" {
						config += "T4 " + defconfig.C4time + " "
					}
				}
			}
		}
		break

	case "Switching Program":
		{
			if group_flag == "group" {
				return "", true, more
			}

			coptions, ok := utils.Controller_options(controller.Id).(*Controller_options)
			if ok {
				if !coptions.Multisched {
					return "", true, more
				}
			} else {
				log.Print("Controller Options not configured :", config_json)
				return "", true, more
			}

			var prconfig Switching_program
			var defconfig Switching_definition_profile

			err := json.Unmarshal([]byte(config_json), &prconfig)
			if err != nil {
				log.Print("Config format Error:", config_json)
				return "", true, more
			}

			if len(prconfig.Porder) == 0 {
				log.Print("Ill-formed program", prconfig)
				return "", true, more
			}

			config += "*IP " + prconfig.Dname + " "
			for _, item := range prconfig.Porder {
				defname := item.Dprofile
				def_profile := utils.Find_profile(plist, defname)
				if def_profile == nil {
					log.Print("Couldn't find profile for program ", defname)
					return "", true, more
				} else {
					err := json.Unmarshal([]byte(def_profile.Config), &defconfig)
					if err != nil {
						log.Print("Config format Error:", def_profile.Config)
						return "", true, more
					}
				}
				aname := item.Asset
				alist, _ := list.(*[]tcclient.Asset_info)
				found, p_ainfo := utils.Find_asset_by_name(aname, alist)
				if found {
					found, _, clist := utils.Find_asset_collection(p_ainfo.Name, p_ainfo.Asset_type, alist, valve_group_asset_type_id)

					if found {
						if aname == clist[0] {
							config += aname + " " + defconfig.Dname + " "
						}
					} else {
						config += aname + " " + defconfig.Dname + " "
					}
				}
			}
		}
		break

	case "Switching Advanced Schedule":
		{
			coptions, ok := utils.Controller_options(controller.Id).(*Controller_options)
			if ok {
				if !coptions.Multisched {
					return "", true, more
				}
			} else {
				log.Print("Controller Options not configured :", config_json)
				return "", true, more
			}

			var asched Switching_advanced_schedule
			var prconfig Switching_program

			err := json.Unmarshal([]byte(config_json), &asched)
			if err != nil {
				log.Print("Config format Error:", config_json)
				return "", true, more
			}

			prname := asched.Program[0].Iprogram
			pr_info := utils.Find_profile(plist, prname)
			if pr_info == nil {
				log.Print("Couldn't find profile for program ", prname)
				return "", true, more
			} else {
				err := json.Unmarshal([]byte(pr_info.Config), &prconfig)
				if err != nil {
					log.Print("Config format Error:", pr_info.Config)
					return "", true, more
				}
			}

			ainfo, ok := info.(*tcclient.Asset_info)
			if !ok {
				log.Print("Assert Error")
				return "", false, more
			}

			if group_flag == "group" {
				var done_map map[string]bool
				done_map = make(map[string]bool)
				for _, item := range prconfig.Porder {
					aname := item.Asset
					if done_map[aname] {
						continue
					}
					alist, _ := list.(*[]tcclient.Asset_info)
					found, p_ainfo := utils.Find_asset_by_name(aname, alist)
					if !found {
						continue
					} else {
						found, _, clist := utils.Find_asset_collection(p_ainfo.Name, p_ainfo.Asset_type, alist, valve_group_asset_type_id)

						if found {
							config += "*AG "
							config += ainfo.Name + " "
							for _, valve := range clist {
								config += valve + " "
								done_map[valve] = true
							}
						}
					}
				}
				return config, true, more
			}
			//loc, _ := time.LoadLocation("India/Delhi")

			// Generate scheduler config
			//t, _ = time.ParseInLocation(time.RFC3339, asched.Caltime, loc)
			config += "*PI " + ainfo.Name + " " + asched.Sname + " "
			t := asched.Caltime
			log.Println("Adding schedule ", t.Day(), t.Hour(), t.Minute())
			config += "D" + strconv.Itoa(t.Day()) + " T" + strconv.Itoa(t.Hour()) + " " + strconv.Itoa(t.Minute()) + " "
			config += prconfig.Dname + " "
			config += "*BI " + ainfo.Name + " " + "G0 "
			done = true
		}
		break

	case "Switch Profile":
		//case "Switch Group Profile":
		{
			var aname string = ""
			var clist []string = []string{}
			var alen int = 0

			coptions, ok := utils.Controller_options(controller.Id).(*Controller_options)
			if ok {
				if coptions.Multisched {
					return "", true, more
				}
			} else {
				log.Print("Controller Options not configured :", config_json)
				return "", true, more
			}

			ainfo, ok := info.(*tcclient.Asset_info)
			if !ok {
				log.Print("Assert Error")
				return "", false, more
			}
			alist, _ := list.(*[]tcclient.Asset_info)
			found, _, clist := utils.Find_asset_collection(ainfo.Name, ainfo.Asset_type, alist, valve_group_asset_type_id)
			aname = ainfo.Name
			if found {
				if aname != clist[0] {
					return "", false, more
				}
			}

			found, misc, _ := utils.Find_asset_collection(ainfo.Name, ainfo.Asset_type, alist, pipeline_asset_type_id)

			if !found {
				return "", false, more
			}

			err := json.Unmarshal([]byte(config_json), &virig)
			if err != nil {
				log.Print("Config format Error:", config_json)
				return "", true, more
			}

			if group_flag == "group" {
				alen = len(clist)
				if alen <= 1 {
					return "", false, more
				} else {
					// Group only if the group contains more than 1 valve
					for _, pinfo := range misc {
						config = "*AG "
						config += pinfo.Name + " "
						for _, valve := range clist {
							/*
								hours, _ := strconv.Atoi(virig.Hours)
								minutes, _ := strconv.Atoi(virig.Minutes)
								config += valve + " " + strconv.Itoa(hours*60+minutes) + " "
							*/
							config += valve + " "
						}
					}
					return config, done, more
				}
			}

			for _, pinfo := range misc {
				if virig.Method == "Time-based" {
					config += "*TI "
					config += pinfo.Name + " "
					config += aname + " " + virig.Hours + " " + virig.Minutes + " "
				} else {
					config += "*VI "
					config += pinfo.Name + " "
					config += aname + " " + (virig.Volume) + " "
				}
			}
		}
		break

	case "Nutrient Profile":
		{
			if group_flag == "group" {
				return "", true, more
			}

			coptions, ok := utils.Controller_options(controller.Id).(*Controller_options)
			if ok {
				if coptions.Multisched {
					return "", true, more
				}
			} else {
				log.Print("Controller Options not configured :", config_json)
				return "", true, more
			}

			var clist []string = []string{}

			err := json.Unmarshal([]byte(config_json), &nprof)
			if err != nil {
				log.Print("Config format Error:", config_json)
				return "", true, more
			}
			ainfo, _ := info.(*tcclient.Asset_info)
			alist, _ := list.(*[]tcclient.Asset_info)

			found, _, clist := utils.Find_asset_collection(ainfo.Name, ainfo.Asset_type, alist, valve_group_asset_type_id)
			if found {
				if ainfo.Name != clist[0] {
					return "", false, more
				}
			}
			config = "*AF "
			config += ainfo.Name + " "
			/*
				if ainfo.Subtype == "MCB" {
					config += "2 "
				} else {
					config += "1 "
				}
			*/
			if nprof.Nver == "v1" {
				config += nprof.Soak + " " + nprof.Wnutrient + " " + nprof.Rinse + " " + nprof.Cycles + " "
			} else {
				config += nprof.Soak + " "
				if nprof.C1volflag {
					config += nprof.C1vol + " "
				} else {
					config += nprof.C1time + " "
				}
				if nprof.C2volflag {
					if nprof.C2vol != "" {
						config += nprof.C2vol + " "
					}
				} else {
					if nprof.C2time != "" {
						config += nprof.C2time + " "
					}
				}
				if nprof.C3volflag {
					if nprof.C3vol != "" {
						config += nprof.C3vol + " "
					}
				} else {
					if nprof.C3time != "" {
						config += nprof.C3time + " "
					}
				}
				if nprof.C4volflag {
					if nprof.C4vol != "" {
						config += nprof.C4vol + " "
					}
				} else {
					if nprof.C4time != "" {
						config += nprof.C4time + " "
					}
				}
				config += nprof.Cycles + " "
			}
		}
		break
	default:
		break
	}
	return config, done, more
}

func Pack_messages_orig(msgs []string, max_length int) ([10][]byte, uint8, uint16) {
	var cmds [10][]byte
	var count uint8 = 0
	var list_cmds []byte = []byte{}
	var final_cmd []byte = []byte{}
	var available uint16 = uint16(max_length - 9)
	var msg_len uint16 = 0
	end_str := "*EN"

	start_seq_num := uint16(seq_num)
	for _, cmd := range msgs {
		var cmd_len uint16 = 0
		cmd_len = uint16(len(cmd))
		if int16(available) < int16(cmd_len) {
			b := make([]byte, 2)
			binary.BigEndian.PutUint16(b, uint16(msg_len))
			final_cmd = append(final_cmd, b[0])
			final_cmd = append(final_cmd, b[1])
			binary.BigEndian.PutUint16(b, uint16(seq_num))
			final_cmd = append(final_cmd, b[0])
			final_cmd = append(final_cmd, b[1])
			final_cmd = append(final_cmd, list_cmds...)
			final_cmd = append(final_cmd, end_str...)
			checksum := crc16.ChecksumCCITT(final_cmd)
			binary.BigEndian.PutUint16(b, uint16(checksum))
			final_cmd = append(final_cmd, b[0])
			final_cmd = append(final_cmd, b[1])
			cmds[count] = final_cmd
			count++
			seq_num++
			available = uint16(max_length - 9)
			msg_len = 0
			list_cmds = []byte{}
			final_cmd = []byte{}
		}
		list_cmds = append(list_cmds, cmd...)
		available -= cmd_len
		msg_len += cmd_len
	}
	b := make([]byte, 2)
	binary.BigEndian.PutUint16(b, uint16(msg_len))
	final_cmd = append(final_cmd, b[0])
	final_cmd = append(final_cmd, b[1])
	binary.BigEndian.PutUint16(b, uint16(seq_num))
	final_cmd = append(final_cmd, b[0])
	final_cmd = append(final_cmd, b[1])
	final_cmd = append(final_cmd, list_cmds...)
	final_cmd = append(final_cmd, end_str...)
	checksum := crc16.ChecksumCCITT(final_cmd)
	binary.BigEndian.PutUint16(b, uint16(checksum))
	final_cmd = append(final_cmd, b[0])
	final_cmd = append(final_cmd, b[1])
	cmds[count] = final_cmd
	count++
	seq_num++
	return cmds, count, start_seq_num
}

func (p vendor) Get_transport_params() *utils.Xport_params {
	var max_sms_len int

	xp := new(utils.Xport_params)
	xp.Xport_type = "sms"
	//	encode_string := "Encode" + cstate.Mobile
	encode_string := "Encode"
	encode_flag := "1c"

	perform_encode := utils.Getenv(encode_string, "true")
	//wait, _ := strconv.Atoi(utils.Getenv("WAIT_TIME", "300"))
	threshold, _ := strconv.Atoi(utils.Getenv("THRESHOLD_TIME", "270"))

	if perform_encode == "true" {
		if encode_flag == "1c" {
			max_sms_len = MAX_SMS_LENGTH * 3 / 4
		} else {
			max_sms_len = MAX_SMS_LENGTH
		}
	} else {
		max_sms_len = MAX_SMS_LENGTH
		encode_flag = ""
	}
	xp.Xport_max_len = max_sms_len
	xp.Xport_max_timeout = threshold
	xp.Encode = (perform_encode == "true")
	return xp
}

func (p vendor) Pack_messages(msgs []string, max_length int) ([]string, uint8, uint16) {
	var cmds [10]string
	var count uint8 = 0
	var list_cmds []byte = []byte{}
	var final_cmd []byte = []byte{}
	var available uint16 = uint16(max_length - 9)
	var msg_len uint16 = 0
	var msgs1 []string
	end_str := "*EN"

	start_seq_num := uint16(seq_num)
	// Preprocess to split long messages into smaller ones
	for _, cmd := range msgs {
		var cmd_len uint16 = 0
		cmd_len = uint16(len(cmd))
		if int16(max_length) < int16(cmd_len) {
			cmds := strings.Split(cmd, ";")
			if len(cmds) > 0 {
				for _, c := range cmds {
					if int16(max_length) >= int16(len(c)) {
						msgs1 = append(msgs1, c)
					} else {
						log.Printf("Skipping long command with separator:%s", c)
					}
				}
			} else {
				log.Printf("Skipping long command with no separator:%s", cmd)
				continue
			}
		} else {
			msgs1 = append(msgs1, cmd)
		}
	}

	for _, cmd := range msgs1 {
		var cmd_len uint16 = 0
		cmd_len = uint16(len(cmd))
		if int16(available) < int16(cmd_len) {
			if len(list_cmds) > 0 {
				final_cmd = append(final_cmd, list_cmds...)
				final_cmd = append(final_cmd, end_str...)
				cmds[count] = string(final_cmd)
				count++
				seq_num++
				available = uint16(max_length - 9)
				msg_len = 0
				list_cmds = []byte{}
				final_cmd = []byte{}
			}
		}
		list_cmds = append(list_cmds, cmd...)
		available -= cmd_len
		msg_len += cmd_len
	}
	if len(list_cmds) > 0 {
		final_cmd = append(final_cmd, list_cmds...)
		final_cmd = append(final_cmd, end_str...)
		cmds[count] = string(final_cmd)
		count++
		seq_num++
	}
	return cmds[:count], count, start_seq_num
}

func (p vendor) Parse_response(msg []byte, req string) (bool, bool, []byte, int) {
	return (Parse_response_lite(msg, req))
}

/* Unpack the message and parse the response and return the sequence
 * number(s) which got acked
 */
var Parse_response_lite = func(msg []byte, req string) (bool, bool, []byte, int) {

	var more bool = false
	msg_len := len(msg)

	fmt.Printf("Response to command %s - Len:%d\n", req, msg_len)
	//fmt.Printf("%s", hex.Dump(msg))

	message := string(msg[0:])

	if message[0] == 'P' || strings.Contains(message, "Solar Powered") {
		// Response to Get voltage
		return more, true, msg[0:], 0
	} else if strings.Contains(message, "*Cmd Accepted") {
		message = string(msg[0:13])
		//skip the #
		sequence_num := string(msg[13+2:])
		log.Printf("Seq %s Accepted:%s", sequence_num, message)
		seq, _ := strconv.Atoi(sequence_num)
		//more = check_for_more(req)
		return more, true, msg[0:], seq
	} else if strings.Contains(message, "*Err") {
		//case "*Err":
		words := strings.Fields(string(msg))
		message = words[0]
		efield, _ := strconv.Atoi(words[1])
		ecode, _ := strconv.Atoi(words[2])
		sequence_num, _ := strconv.Atoi(words[5][1:])
		log.Printf("Message %s Seq %d Error:%d Ecode:%d", message, sequence_num, efield, ecode)
		return more, false, msg[0:], int(sequence_num)
	} else {
		var gh_response map[string]json.RawMessage
		err := json.Unmarshal([]byte(message), &gh_response)
		if err != nil {
			log.Print("Error:Not in JSON format:", message)
		}
		return more, true, msg[0:], 0
	}
}

func check_for_more(req string) bool {
	var more bool = false
	if strings.Contains(req[0:3], "@#%&") {
		data, _ := b64.StdEncoding.DecodeString(req[4:])
		lenf := len(data)
		bdata := make([]byte, lenf)
		for i := 4; i < lenf; i++ {
			bdata[i] = ^(byte(data[i]))
		}
		req = string(bdata)
	}
	cmd := strings.Split(req, " ")
	switch cmd[0] {
	case "*GH":
		return true
	}
	return more
}

/* Unpack the message and parse the response and return the sequence
 * number(s) which got acked
 */
var Parse_response_original = func(msg []byte) (bool, []byte, int) {
	// Create a message which fits into length and return
	// an index if  there is more data to be sent.
	// Test with dummy_response
	//msg = dummy_response("*Error")

	msg_len := binary.BigEndian.Uint16(msg)
	if msg_len > 64 {
		log.Printf("Error Msg Len:%d\n", msg_len)
		return false, msg[0:], -1
	}

	fmt.Printf("Len:%d\n", msg_len)
	fmt.Printf("%s", hex.Dump(msg))
	calc_crc := crc16.ChecksumCCITT(msg[0 : msg_len-2])
	received_crc := binary.BigEndian.Uint16(msg[msg_len-2:])

	if received_crc != calc_crc {
		log.Print("CRC error", received_crc, calc_crc)
		return false, msg[4 : msg_len-2], -1
	}

	sequence_num := int(binary.BigEndian.Uint16(msg[2:]))
	//message := string(msg[4 : msg_len-2])
	message := string(msg[4 : msg_len-2])
	if strings.Contains(message, "*Command Accepted") {
		log.Printf("Seq %d Accepted:%s", sequence_num, message)
		return true, msg[4 : msg_len-2], sequence_num
	} else {
		message = string(msg[4 : msg_len-8])
	}

	log.Printf("Seq %d:%s", sequence_num, message)

	switch message {
	case "*Err":
		efield := binary.BigEndian.Uint16(msg[4+6:])
		ferror1 := binary.BigEndian.Uint16(msg[4+6+2:])
		ferror2 := binary.BigEndian.Uint16(msg[4+6+2+2:])
		log.Print("Field error from Controller:", efield, ferror1, ferror2)
		return false, msg[4 : msg_len-2], sequence_num
	case "*EX":
		cfield := binary.BigEndian.Uint16(msg[4+3:])
		cerror1 := binary.BigEndian.Uint16(msg[4+3+2:])
		cerror2 := binary.BigEndian.Uint16(msg[4+3+2+2:])
		log.Print("Execution error from Controller:", cfield, cerror1, cerror2)
		return false, msg[4 : msg_len-2], sequence_num
	case "*ST":
		sfield := binary.BigEndian.Uint16(msg[4+3:])
		serror1 := binary.BigEndian.Uint16(msg[4+3+2:])
		serror2 := binary.BigEndian.Uint16(msg[4+3+2+2:])
		//message = message + ":" + sfield + ":" + serror1 + ":" + serror2
		log.Print("Status from Controller:", sfield, serror1, serror2)
		return false, msg[4 : msg_len-2], sequence_num
	case "*WR":
		wfield := binary.BigEndian.Uint16(msg[4+3:])
		werror1 := binary.BigEndian.Uint16(msg[4+3+2:])
		werror2 := binary.BigEndian.Uint16(msg[4+3+2+2:])
		log.Print("Warning from Controller:", wfield, werror1, werror2)
		return false, msg[4 : msg_len-2], sequence_num
	default:
		break
	}
	return false, msg[4 : msg_len-8], -1
}

func generate_sms(handle string, length int) {
	// Create a message which fits into length and return
	// an index if  there is more data to be sent.

}

func dummy_response(message string) []byte {
	// Create a message which fits into length and return
	// an index if  there is more data to be sent.
	var final_cmd []byte = []byte{}
	var msg_len uint16 = 0

	seq_num := 65535

	if strings.Contains(message, "*Command Accepted") {
		msg_len = uint16(6 + len(message))
		b := make([]byte, 2)
		binary.BigEndian.PutUint16(b, uint16(msg_len))
		final_cmd = append(final_cmd, b[0])
		final_cmd = append(final_cmd, b[1])
		binary.BigEndian.PutUint16(b, uint16(seq_num))
		final_cmd = append(final_cmd, b[0])
		final_cmd = append(final_cmd, b[1])
		final_cmd = append(final_cmd, []byte(message)...)
		checksum := crc16.ChecksumCCITT(final_cmd)
		binary.BigEndian.PutUint16(b, uint16(checksum))
		final_cmd = append(final_cmd, b[0])
		final_cmd = append(final_cmd, b[1])
		return final_cmd
	}
	msg_len = uint16(12 + len(message))
	b := make([]byte, 2)
	binary.BigEndian.PutUint16(b, uint16(msg_len))
	final_cmd = append(final_cmd, b[0])
	final_cmd = append(final_cmd, b[1])
	binary.BigEndian.PutUint16(b, uint16(seq_num))
	final_cmd = append(final_cmd, b[0])
	final_cmd = append(final_cmd, b[1])
	final_cmd = append(final_cmd, []byte(message)...)
	efield := 199
	ferror1 := 299
	ferror2 := 399
	binary.BigEndian.PutUint16(b, uint16(efield))
	final_cmd = append(final_cmd, b[0])
	final_cmd = append(final_cmd, b[1])
	binary.BigEndian.PutUint16(b, uint16(ferror1))
	final_cmd = append(final_cmd, b[0])
	final_cmd = append(final_cmd, b[1])
	binary.BigEndian.PutUint16(b, uint16(ferror2))
	final_cmd = append(final_cmd, b[0])
	final_cmd = append(final_cmd, b[1])

	checksum := crc16.ChecksumCCITT(final_cmd)
	binary.BigEndian.PutUint16(b, uint16(checksum))

	final_cmd = append(final_cmd, b[0])
	final_cmd = append(final_cmd, b[1])
	return final_cmd
}

func get_num_check_strings(cmd string) int {

	cmd1 := cmd[:3]

	switch cmd1 {
	case "*VI", "*BI", "*TI", "*PI":
		return 0
	case "*AG":
		return 0
	default:
		return 2
	}
}

func cmerge(elements1 []string, elements2 []string, num int) []string {
	// Use map to record duplicates as we find them.
	var check_num bool = false
	if num < 0 {
		check_num = true
	}
	encountered := map[string]string{}
	result := []string{}

	/*
		if len(elements1) == 0 {
			return string([]byte(elements2))
		}
	*/

	for v := range elements2 {
		cmd := elements2[v]

		if check_num {
			num = get_num_check_strings(cmd)
		}

		if num > 0 {
			k := strings.Split(cmd, " ")
			cmd = strings.Join(k[:num], " ")
		}
		encountered[cmd] = elements2[v]
	}
	log.Print("Merge Encountered:", encountered)

	for v := range elements1 {
		cmd := elements1[v]
		if check_num {
			num = get_num_check_strings(cmd)
		}
		if num > 0 {
			k := strings.Split(cmd, " ")
			cmd = strings.Join(k[:num], " ")
		}
		if org, ok := encountered[cmd]; ok {
			// Push element from the new array
			delete(encountered, cmd)
			result = append(result, org)
		} else {
			// Record this element as an encountered element.
			// Append to result slice.
			result = append(result, elements1[v])
		}
	}
	log.Print("Result Encountered:", result, encountered)

	for v := range elements2 {
		cmd := elements2[v]
		//cmd := elements2[v]
		if check_num {
			num = get_num_check_strings(cmd)
		}
		if num > 0 {
			k := strings.Split(cmd, " ")
			cmd = strings.Join(k[:num], " ")
		}
		if _, ok := encountered[cmd]; ok {
			result = append(result, elements2[v])
		}
	}

	// Return the new slice.
	return result
}

func Encode_data(data []byte, method string) []byte {
	if method == "1c" {
		lenf := len(data)
		//log.Print("Org: ", data)
		bdata := make([]byte, lenf)
		for i := 0; i < lenf; i++ {
			bdata[i] = ^(byte(data[i]))
		}
		//log.Print("1c: ", bdata)
		//b64data := b64.StdEncoding.EncodeToString([]byte(bdata))
		urldata := b64.URLEncoding.EncodeToString([]byte(bdata))
		//log.Print("b64: ", b64data)
		//final := append([]byte("@#%&"), []byte(b64data)...)
		final := append([]byte("@#%&"), []byte(urldata)...)
		//log.Print("Final: ", string(final))
		return final
	}
	return []byte{}
}
