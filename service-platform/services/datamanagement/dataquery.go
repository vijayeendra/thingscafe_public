//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package datamanagement

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/service-platform/common"
	objsvc "gitlab.com/service-platform/services/objectmanagement"
)

/*
 * data profile contain profile for a certain kind of profile
 * described in the profile below
 */
type Event_record struct {
	Resource    string `json:"id"`
	Seq         int    `json:"seq"`
	Day         string `json:"day"`
	Ts          int64  `json:"ts"`
	Asset_type  string `json:"asset_type"`
	Asset_name  string `json:"asset_name"`
	Entity_name string `json:"entity_name"`
	Description string `json:"description"`
	Severity    string `json:"severity"`
	Details     string `json:"details"`
	App         string `json:"app"`
}

type Data_query struct {
	Data_type   string                `json:"data_type"`
	Resource    string                `json:"resource"`
	Entity_name string                `json:"entity_name"`
	Asset_name  string                `json:"asset_name"`
	Start_time  string                `json:"start_time"`
	End_time    string                `json:"end_time"`
	Severity    []string              `json:"severity"`
	Foptions    common.Filter_options `json:"foptions"`
	Pcontext    []byte                `json:"pcontext"`
}

type Stats_query struct {
	Data_type        string                `json:"data_type"`
	Resource         string                `json:"resource"`
	Entity_name      string                `json:"entity_name"`
	Asset_name       string                `json:"asset_name"`
	Aggregation_type string                `json:"aggregation_type"`
	Start_time       string                `json:"start_time"`
	End_time         string                `json:"end_time"`
	Foptions         common.Filter_options `json:"foptions"`
	Pcontext         []byte                `json:"pcontext"`
}

type Data_query_response struct {
	Data_type  string                   `json:"data_type"`
	Start_time string                   `json:"start_time"`
	End_time   string                   `json:"end_time"`
	Count      int                      `json:"count"`
	Pcontext   []byte                   `json:"pcontext"`
	Records    []map[string]interface{} `json:"records"`
}

type Stats_query_response struct {
	Data_type  string                   `json:"data_type"`
	Start_time string                   `json:"start_time"`
	End_time   string                   `json:"end_time"`
	Count      int                      `json:"count"`
	Pcontext   []byte                   `json:"pcontext"`
	Records    []map[string]interface{} `json:"records"`
}

/*

func (e *Event_record) SetEntityId(id gocql.UUID) {
	e.Entity_id = id
}

func (e *Event_record) GetEntityId() gocql.UUID {
	return (e.Entity_id)
}

func (e *Event_record) SetId(id gocql.UUID) {
	e.Id = id
}

func (e *Event_record) GetId() gocql.UUID {
	return (e.Id)
}
*/

var Query_stats = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options
	var dq Stats_query

	qoptions = default_data_query_options
	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")

	saas := common.Get_saas_entity(&qoptions)

	/*
		if r.Method == "POST" {
			delete(qoptions.Where, "entity_id")
		} else {
			obj.Init("config_profiles_by_entity", &qoptions)
		}
	*/

	var stime, etime time.Time
	if r.Method == "POST" {
		err, _, _ := obj.Unmarshal(r, &dq)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		if dq.Entity_name == "" {
			http.Error(w, err.Error(), 500)
			return
		}
		if dq.Start_time != "" {
			stime, _ = time.Parse(time.RFC3339, dq.Start_time)
		} else {
			err = errors.New("Start time not specified")
			http.Error(w, err.Error(), 500)
			return
		}
		if dq.End_time != "" {
			etime, _ = time.Parse(time.RFC3339, dq.End_time)
		} else {
			etime = time.Now().UTC()
		}

		if dq.Foptions.Allow_filtering == false {
			qoptions.Allow_filtering = false
		} else {
			qoptions.Allow_filtering = true
		}

		if dq.Foptions.Limit != 0 {
			qoptions.Pagesize = dq.Foptions.Limit
		}

		qoptions.Orderby = dq.Foptions.Orderby
		qoptions.Groupby = dq.Foptions.Groupby

		where := new(common.Query_where)
		where.Operator = ">="
		where.Lhs = "ts"
		where.Rhs = stime.UnixNano() / 1000000
		qoptions.Where_complex = append(qoptions.Where_complex, *where)

		where = new(common.Query_where)
		where.Operator = "<="
		where.Lhs = "ts"
		where.Rhs = etime.UnixNano() / 1000000
		qoptions.Where_complex = append(qoptions.Where_complex, *where)

		/*
			Data_type   string `json:"data_type"`
			Id          string `json:"asset_id"`
			Entity_Name string `json:"entity_name"`
			Start_time  string `json:"start_time"`
			End_time    string `json:"end_time"`
			Misc        string `json:"misc"`
		*/

	} else {
		err := errors.New("Not supported")
		http.Error(w, err.Error(), 500)
		return
	}
	table := saas + "_counters"
	if dq.Data_type != "" {
		table = saas + "_" + dq.Data_type
	}
	qoptions.Where["entity_name"] = saas + ":" + dq.Entity_name
	qoptions.Where["asset_name"] = dq.Asset_name
	qoptions.Where["aggregation_type"] = dq.Aggregation_type

	if strings.Contains(r.URL.Path, "by-asset") {
		qoptions.Where["resource_id"] = dq.Resource
	}

	obj.Init(table, &qoptions)
	obj.Set_keyspace("thingscafe_data")

	if err, payload, pstate := svc.List_paged_objects_svc(w, r, &obj, dq.Pcontext); err != nil {
		fmt.Println(err)
	} else {

		sqr := new(Stats_query_response)
		sqr.Data_type = dq.Data_type
		sqr.Start_time = fmt.Sprintf("%v", stime.Round(0))
		sqr.End_time = fmt.Sprintf("%v", etime.Round(0))
		sqr.Records = make([]map[string]interface{}, qoptions.Limit)
		err := json.Unmarshal(payload.([]byte), &sqr.Records)

		if err != nil {
			http.Error(w, err.Error(), 500)
		} else {
			sqr.Count = len(sqr.Records)
			sqr.Pcontext = pstate.([]byte)
			log.Println(sqr)
			ojson, _ := json.Marshal(sqr)
			_ = ojson
			//		w.Write(payload.([]byte))
			w.Write(ojson)
		}
	}
})

var Query_events = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options
	var dq Data_query

	qoptions = default_data_query_options
	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")

	saas := common.Get_saas_entity(&qoptions)

	/*
		if r.Method == "POST" {
			delete(qoptions.Where, "entity_id")
		} else {
			obj.Init("config_profiles_by_entity", &qoptions)
		}
	*/

	var stime, etime time.Time
	if r.Method == "POST" {
		err, _, _ := obj.Unmarshal(r, &dq)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		if dq.Entity_name == "" {
			http.Error(w, err.Error(), 500)
			return
		}
		if dq.Start_time != "" {
			stime, _ = time.Parse(time.RFC3339, dq.Start_time)
		} else {
			err = errors.New("Start time not specified")
			http.Error(w, err.Error(), 500)
			return
		}
		if dq.End_time != "" {
			etime, _ = time.Parse(time.RFC3339, dq.End_time)
		} else {
			etime = time.Now().UTC()
		}

		if dq.Foptions.Allow_filtering == false {
			qoptions.Allow_filtering = false
		} else {
			qoptions.Allow_filtering = true
		}

		if dq.Foptions.Limit == 0 {
			qoptions.Pagesize = 10
		} else {
			qoptions.Pagesize = dq.Foptions.Limit
		}

		qoptions.Orderby = dq.Foptions.Orderby
		qoptions.Groupby = dq.Foptions.Groupby

		if len(dq.Severity) == 1 {
			qoptions.Where["severity"] = dq.Severity[0]
		} else {
			where := new(common.Query_where)
			where.Operator = "in"
			where.Lhs = "severity"
			rhs := make([]string, 0, 6)
			if len(dq.Severity) == 0 {
				rhs = append(rhs, "info", "minor", "major", "warning", "critical")
			} else {
				for _, sev := range dq.Severity {
					rhs = append(rhs, sev)
				}
			}
			where.Rhs = rhs
			qoptions.Where_complex = append(qoptions.Where_complex, *where)
		}

		where := new(common.Query_where)
		where.Operator = ">="
		where.Lhs = "ts"
		where.Rhs = stime.UnixNano() / 1000000
		qoptions.Where_complex = append(qoptions.Where_complex, *where)

		where = new(common.Query_where)
		where.Operator = "<="
		where.Lhs = "ts"
		where.Rhs = etime.UnixNano() / 1000000
		qoptions.Where_complex = append(qoptions.Where_complex, *where)

		/*
			Data_type   string `json:"data_type"`
			Id          string `json:"asset_id"`
			Entity_Name string `json:"entity_name"`
			Start_time  string `json:"start_time"`
			End_time    string `json:"end_time"`
			Misc        string `json:"misc"`
		*/

	} else {
		err := errors.New("Not supported")
		http.Error(w, err.Error(), 500)
		return
	}
	table := saas + "_events"
	if dq.Data_type != "" {
		table = saas + "_" + dq.Data_type
	}
	qoptions.Where["entity_name"] = saas + ":" + dq.Entity_name

	if strings.Contains(r.URL.Path, "by-asset") {
		table = table + "_by_asset"
		qoptions.Where["resource"] = dq.Resource
	}

	obj.Init(table, &qoptions)
	obj.Set_keyspace("thingscafe_data")

	if err, payload, pstate := svc.List_paged_objects_svc(w, r, &obj, dq.Pcontext); err != nil {
		fmt.Println(err)
	} else {

		dqr := new(Data_query_response)
		dqr.Data_type = dq.Data_type
		dqr.Start_time = fmt.Sprintf("%v", stime.Round(0))
		dqr.End_time = fmt.Sprintf("%v", etime.Round(0))
		dqr.Records = make([]map[string]interface{}, qoptions.Limit)
		err := json.Unmarshal(payload.([]byte), &dqr.Records)

		if err != nil {
			http.Error(w, err.Error(), 500)
		} else {
			dqr.Count = len(dqr.Records)
			dqr.Pcontext = pstate.([]byte)
			log.Println(dqr)
			ojson, _ := json.Marshal(dqr)
			_ = ojson
			//		w.Write(payload.([]byte))
			w.Write(ojson)
		}
	}
})

var Get_event = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj config_profile_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_data_query_options
	common.Setup_qoptions(r, &qoptions)
	saas := common.Get_saas_entity(&qoptions)

	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")
	table := saas + "_" + "events_by_entity"
	if strings.Contains(r.URL.Path, "by-asset") {
		table = saas + "_" + "events"
	}

	obj.Init(table, &qoptions)
	obj.Set_keyspace("thingscafe_data")

	if err := svc.Get_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
	return
})

func SetRoutes(router *mux.Router) *mux.Router {
	s := router.PathPrefix("/data").Subrouter()
	s.HandleFunc("/query-events", Query_events).Methods("GET")
	s.HandleFunc("/query-events", Query_events).Methods("POST")
	s.HandleFunc("/query-stats", Query_stats).Methods("POST")
	s.HandleFunc("/query-stats-by-asset", Query_stats).Methods("POST")
	s.HandleFunc("/query-events-by-asset", Query_events).Methods("POST")
	s.HandleFunc("/get-event", Get_event).Methods("GET")
	return router
}

var default_data_query_options common.Query_options
