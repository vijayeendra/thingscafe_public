# PWA_dynamic_manifest
Using open source code as the reference and changed it
achieve this. The corresponding license is in this
directory. The original code is wl directory.

Using this to dynamically return manifest.json based on 
url used. This is used to support whitelabeling the 
PWA

To support whitelabeling within PWA for name, 
just add resources in 
/var/www/html/tcafe/assets/icons.<name>

Also a specific image for logo - <name>.png in /var/www/html/tcafe/assets

When you access url as <yourdomainname>/<name>, the 
specific icons and logo would be used in PWA.
