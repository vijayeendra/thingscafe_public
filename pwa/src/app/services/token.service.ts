//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { from } from 'rxjs';

const TOKEN_KEY = "X-Auth-Token";
const LOGINAS_TOKEN_KEY = "LoginAs-Token";

@Injectable(
)

export class TokenService {
  constructor(private global: GlobalService,
	private storage: Storage) {

	  this.global.ready()
	  .subscribe(ready => {
		  if (!ready) {
			  return
		  }
	  });
    }

	getToken(){
		return this.storage.ready().then(() => {
			return (this.storage.get(LOGINAS_TOKEN_KEY)
			.then (resp => { 
				if (resp) {
					return resp;
				} else {
					return this.storage.get(TOKEN_KEY)
					.then(
					  data => {
						return data;
					  },
					  error => console.error(error)
					);
				}
			},
			err => {
				console.error(err);
			}));
		});
	}

	getTokenAsObservable() {
		return from(this.getToken());
	}
}
