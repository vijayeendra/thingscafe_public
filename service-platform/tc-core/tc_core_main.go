//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gocql/gocql"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/service-platform/db/Cassandra"
	assetsvc "gitlab.com/service-platform/services/assetmanagement"
	configsvc "gitlab.com/service-platform/services/configmanagement"
	datasvc "gitlab.com/service-platform/services/datamanagement"
	entitysvc "gitlab.com/service-platform/services/entitymanagement"
	"gitlab.com/service-platform/services/myauth"
	usersvc "gitlab.com/service-platform/services/usermanagement"
)

type Product struct {
	Id          int
	Name        string
	Slug        string
	Description string
}

var products = []Product{
	Product{Id: 1, Name: "Hover Shooters", Slug: "hover-shooters", Description: "Shoot your way to the top on 14 different hoverboards"},
	Product{Id: 2, Name: "Ocean Explorer", Slug: "ocean-explorer", Description: "Explore the depths of the sea in this one of a kind underwater experience"},
	Product{Id: 3, Name: "Dinosaur Park", Slug: "dinosaur-park", Description: "Go back 65 million years in the past and ride a T-Rex"},
	Product{Id: 4, Name: "Cars VR", Slug: "cars-vr", Description: "Get behind the wheel of the fastest cars in the world."},
	Product{Id: 5, Name: "Robin Hood", Slug: "robin-hood", Description: "Pick up the bow and arrow and master the art of archery"},
	Product{Id: 6, Name: "Real World VR", Slug: "real-world-vr", Description: "Explore the seven wonders of the world in VR"},
}

var ProductsHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	payload, _ := json.Marshal(products)

	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(payload))
})

type db interface {
	initialize()
	deferclose()
}

var Cassandrasession *gocql.Session

var repo db

var CassandraDB bool = true

// Get env var or default
func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func main() {

	port := getEnv("PORT", "8082")
	wait, _ := strconv.Atoi(getEnv("TC_WAIT_TIME", "60"))
	addr := ":" + port

	Cassandra.Initialize()
	Cassandrasession = Cassandra.Session

	defer Cassandrasession.Close()

	r := mux.NewRouter()

	r.Handle("/tc/", http.FileServer(http.Dir("./views/")))
	r.PathPrefix("tc/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	r = myauth.SetAuthenticationRoutes(r)
	r = usersvc.SetRoutes(r)
	s := r.PathPrefix("/tc/v1").Subrouter()
	// Separate router for services
	s = entitysvc.SetRoutes(s)
	s = assetsvc.SetRoutes(s)
	s = configsvc.SetRoutes(s)
	s = usersvc.SetRoutes(s)
	s = datasvc.SetRoutes(s)
	//r.HandleFunc("/get-token", basicauth.AuthToken).Methods("GET")
	//s.Use(myauth.Authorizerequest, handlers.RecoveryHandler())
	s.Use(myauth.Authorizerequest)
	//s.Use(myauth.Authorizerequest, myauth.PerformBasicAuth, handlers.RecoveryHandler())
	logRouter := handlers.LoggingHandler(os.Stdout, r)

	//cors optionsGoes Below
	corsOpts := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodGet, //http methods for your app
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
			http.MethodHead,
		},

		AllowedHeaders: []string{
			"*", //or you can your header key values which you are using in your application

		},
	})

	// Initialize interfaces for service to acces DB
	serv := &http.Server{
		Addr:           addr,
		Handler:        corsOpts.Handler(logRouter),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   time.Duration(wait * int(time.Second)),
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(serv.ListenAndServe())
	//log.Fatal(http.ListenAndServe(addr, corsOpts.Handler(logRouter)))
	//log.Fatal(http.ListenAndServe(":8080", r))
}

/*
// Logs incoming requests, including response status.
func Logger(out io.Writer, h http.Handler) http.Handler {
	logger := log.New(out, "", 0)
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		o := &responseObserver{ResponseWriter: w}
		h.ServeHTTP(o, r)
		addr := r.RemoteAddr
		if i := strings.LastIndex(addr, ":"); i != -1 {
			addr = addr[:i]
		}
		logger.Printf("%s - - [%s] %q %d %d %q %q",
			addr,
			time.Now().Format("02/Jan/2006:15:04:05 -0700"),
			fmt.Sprintf("%s %s %s", r.Method, r.URL, r.Proto),
			o.status,
			o.written,
			r.Referer(),
			r.UserAgent())
	})
}

type responseObserver struct {
	http.ResponseWriter
	status      int
	written     int64
	wroteHeader bool
}

func (o *responseObserver) Write(p []byte) (n int, err error) {
	if !o.wroteHeader {
		o.WriteHeader(http.StatusOK)
	}
	n, err = o.ResponseWriter.Write(p)
	o.written += int64(n)
	return
}

func (o *responseObserver) WriteHeader(code int) {
	o.ResponseWriter.WriteHeader(code)
	if o.wroteHeader {
		return
	}
	o.wroteHeader = true
	o.status = code
}
*/
