//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package tc_mw_plugin

import (
	"github.com/gorilla/mux"
	tcclient "gitlab.com/service-platform/tc-mw/tc-client"
	"gitlab.com/service-platform/tc-mw/utils"
)

type Provision_plugin interface {
	Init(handle interface{}) int
	Getenv() *utils.Plugin_env
	Get_init_config(cinfo *tcclient.Asset_info) []string
	Send_full_config(site *tcclient.Entity_info, controller *tcclient.Asset_info, profiles *[]tcclient.Config_profile, assets *[]tcclient.Asset_info, user_config_flag bool, cstate *utils.Controller_state, incremental bool, encode_flag string) (*utils.Config_response, error)
	Send_operation_cmd(controller *tcclient.Asset_info, operation string, ainfo *tcclient.Asset_info, einfo *tcclient.Entity_info, alist []tcclient.Asset_info, cstate *utils.Controller_state, encode_flag string) (*utils.Config_response, error)
	Get_full_config(site *tcclient.Entity_info, controller *tcclient.Asset_info, profiles *[]tcclient.Config_profile, assets *[]tcclient.Asset_info, user_config_flag bool, cstate *utils.Controller_state, incremental bool) []string
	Get_config(site *tcclient.Entity_info, controller *tcclient.Asset_info, profile tcclient.Config_profile, profile_list *[]tcclient.Config_profile, alist *[]tcclient.Asset_info, group_flag string, cstate *utils.Controller_state) []string
	Get_operation_cmd(operation string, ainfo *tcclient.Asset_info, einfo *tcclient.Entity_info, alist []tcclient.Asset_info) []string
	Get_transport_params() *utils.Xport_params
	Pack_messages(msgs []string, max_length int) ([]string, uint8, uint16)
	Parse_response(msg []byte, req string) (bool, bool, []byte, int)
	SetRoutes(router *mux.Router) *mux.Router
}
