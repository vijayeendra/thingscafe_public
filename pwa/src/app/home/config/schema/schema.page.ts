//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { Input, Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, RouterEvent, NavigationEnd } from '@angular/router';
import { AlertController, NavController, ModalController } from '@ionic/angular';
import { Observable } from  'rxjs/Rx';
import { map, filter } from 'rxjs/operators';
import { Cache, CacheService} from 'ionic-cache-observable';
import { ProfileService, config_profile} from '../../../services/profile.service';
import { SchemaService, config_schema } from '../../../services/schema.service';
import { GlobalService } from '../../../services/global.service';
import {CustomValidator} from '../../../common/custom-validator';
import  * as moment from 'moment';

class apply_data {
	site_id:string;
	site_name:string;
    asset_id:string;
    asset_name:string;
    type_name:string;
    type_id:string;
}

export class dynlist_data {
	dynlist_field:string;
	dynlist_oper:string;
	dynlist_item:string;
    dynlist_list:any;
}

class schema_item {
	  name:string;
	  type:string;
	  label:string;
	  placeholder:string;
	  function:string;
	  options:string[];
	  only_if_key:string;
	  only_if_value:string;
	  data:any;
}

@Component({
  selector: 'app-schema',
  templateUrl: './schema.page.html',
  styleUrls: ['./schema.page.scss'],
})


export class SchemaPage implements OnInit {

	@Input() profile:any;
	@Input() schema:any;
	@Input() schema_name:string;
	@Input() action:string;
	@Input() applyset:any[];
	@Input() profiles:any;

	//config:any[] = JSON.parse(input_json);
	public data$: Observable<config_schema>;   
	config:any[];
	configForm: FormGroup;
	private cache: Cache<any>;
	private scache: Cache<any>;

	submitAttempt: boolean = false;
	items:FormArray;
	title:string;
	date:string;
	dynlist_options	: dynlist_data[][];
    num_dynoptions :number;
	dynlist_current	: any;
	dynlist_current_oper	: any;

	constructor( private profilesvc: ProfileService,
		  private schemasvc: SchemaService,
		  private cachesvc: CacheService,
		  private alertCtrl: AlertController, 
		  private modalCtrl: ModalController,
	      private global : GlobalService, 
		  private router: Router,
		  private customvalidator: CustomValidator, 
		  private formBuilder: FormBuilder, 
		  private aroute: ActivatedRoute,
		  private navCtrl:NavController) {

		  this.title = "Loading ..";
		  this.action = "";
	      this.num_dynoptions = 0;
		  this.dynlist_options = new Array();
		  this.dynlist_current = new Map();
		  this.dynlist_current_oper = new Map();
	}

	ngOnInit() {
	   if (this.schema) {
		 this.schema_name = this.schema.name;
	   } 
	
	   this.date = moment(new Date().toISOString()).locale('es').format();

	   console.log("Loading schema for", this.schema_name);
	   this.config = [];
	   if (this.action == "Add") {
		 // Profile is null
		 this.title = "New Profile";
	   } else {
		 this.title = this.profile.name;
	   }
	   if (!this.schema) {
		 this.schemasvc.ready().subscribe(resp => {
		  if (resp == true) {
			  //		this.load_schema(this.schema_name);
			  // this.title = "Loading Apply list"
			  let schema = this.schemasvc.get_schema_local(this.schema_name);
			  if (schema != null) {
				if (this.action == "Apply" || this.action == "viewApply") {

					let success = this.createapplyconfig(schema.cschema_json);
					if (! success) {
						this.cancel();
						this.title = "Apply list is empty"
						return;
					}
				}  else {
					this.config = JSON.parse(schema.cschema_json);
				}
				console.log("Config schema:",this.config);
				this.configForm = this.creategroup(schema);
				this.check_dependencies();
			  }
		  }
	   });
	  } else {
		  this.config = JSON.parse(this.schema.cschema_json);
		  console.log("Config schema:",this.config);
		  this.configForm = this.creategroup(this.schema);
		  this.check_dependencies();
	  }
	}

	private load_apply_set_schema(schema?:any):any {

	    if (!schema) 
		  schema = this.schema;

		let ptype = schema.cschema_type;
		let ptype_id = schema.cschema_type_id;

		if (ptype != "config" && 
			ptype != "group_config" &&
			ptype != "owner_config") {
			return null;
		}

	    let subtype = "";
		if (schema.kv)
			subtype = schema.kv['apply_subtype']

		let resp=[];
		let parent_list=[];
		for (let item of this.applyset) {
		  if (item.asset_type == ptype_id ) {
			  if (subtype && subtype != ""){
				if (subtype == item.subtype) {
					let root_asset = this.find_parent_asset(item);
					if (root_asset && ! parent_list.includes(root_asset.id)) {
						  parent_list.push(root_asset.id);
						  resp.push(root_asset);
					}
					resp.push(item);
				}
			  } else {
				  let root_asset = this.find_parent_asset(item);
				  if (root_asset && ! parent_list.includes(root_asset.id)) {
						parent_list.push(root_asset.id);
						resp.push(root_asset);
				  }
				  resp.push(item);
			  }
		  }
		}
		return[parent_list, resp];
	}

	private load_apply_set():any {

		let profile = this.profile;

		let ptype = profile.profile_type;
		let ptype_id = profile.profile_type_id;

		if (ptype != "config" && 
			ptype != "group_config" &&
			ptype != "owner_config") {
			return null;
		}

	    let subtype = "";
		let config = JSON.parse(profile.config);
		if (config.type) 
			subtype = config.type
		else if (config.mtype) 
			subtype = config.mtype
		else if (profile.kv)
			subtype = profile.kv['apply_subtype']

		let resp=[];
		let parent_list=[];

		for (let item of this.applyset) {
		  if (item.asset_type == ptype_id ) {
			  if (subtype && subtype != ""){
				if (subtype == item.subtype) {
					let root_asset = this.find_parent_asset(item);
					if (root_asset && ! parent_list.includes(root_asset.id)) {
						  parent_list.push(root_asset.id);
						  resp.push(root_asset);
					}
					resp.push(item);
				}
			  } else {
				  let root_asset = this.find_parent_asset(item);
				  if (root_asset && ! parent_list.includes(root_asset.id)) {
						parent_list.push(root_asset.id);
						resp.push(root_asset);
				  }
				  resp.push(item);
			  }
		  }
		}

		if (profile.kv) {
			if (profile.kv["apply_property"] == "multiple") {
				// Dont check for already applied assets as 
				// multiple applies are allowed
				return[parent_list, resp];
			}
		}

		let applied_assets = this.get_applied_assets();
		let final_list = resp.filter( ( el ) => !applied_assets.has( el.id ) );
		return[parent_list, final_list];
	}

	private find_parent_asset(asset:any):any {
		for (let item of this.applyset) {
			if (asset.parent_asset_id == item.id)
				return item;
		}
	}

	private find_asset(id:string):any {
		for (let item of this.applyset) {
			if (id == item.id)
				return item;
		}
	    return null
	}


	private createapplyconfig(aschema:string) {
		let input_schema = JSON.parse(aschema);
		let [parentlist, applyset] = this.load_apply_set();

		if (applyset.length < 1) {
			return false;
		}

		for (let item of applyset) {
			let item_schema:schema_item = new schema_item;

			if (parentlist.length > 0) {
				if (parentlist.includes(item.id)) {
				  item_schema.type = "checkbox";
				  item_schema.label = "Controller " + item.description;
				} else {
				  item_schema.type = "dcheckbox";
				  item_schema.only_if_key = item.parent_asset_id;
				  item_schema.only_if_value = "true";
					//item_schema.label = item.name;
				  item_schema.label = item.name + ":" + item.description
				}
			} else {
				  item_schema.type = "checkbox";
				  if (! item.root_asset_flag) {
					item_schema.label = item.name + ":" + item.description
					  //item_schema.label = item.name;
				  } else {
					item_schema.label = item.description;
				  }
			}
			item_schema.name = item.id;
			item_schema.data = item;
			this.config.push(item_schema);
			// One for order
		}
		if (this.action != 'viewApply') {
			let item_schema:schema_item = new schema_item;

			item_schema.type = input_schema[1].type;
			item_schema.name = input_schema[1].name;
			item_schema.label = input_schema[1].label;
			item_schema.function = input_schema[1].function;
			this.config.push(item_schema);
		}
		return true;
	}


	private load_schemas(id?:string, view?:string) {
	  if (id == null)
		  return ([]);
	  if (view == null)
		  view = "owned";

	  const dataObservable = this.schemasvc.get_schemas()
		  .map(response => {
			  if (! response ) {
				  return ([]);
			  }
			  return (response);
		  });

	  this.cachesvc.register('schemas', dataObservable)
	  .subscribe((cache) => {
		  this.scache = cache;
		  this.data$ = this.scache.get$;
		  /*
		  this.data$.subscribe(resp => {
			  if (resp) {
				  this.title = resp.name;
				  this.config = JSON.parse(resp.cschema_json);
				  console.log("Config schema:",this.config);
				  if (this.action == "Apply") {
					  this.createapplyconfig();
				  } 
				  this.configForm = this.creategroup();
			  }
		  });
		   */
		  // Refresh the cache immediately.
		  this.scache.refresh().subscribe();
	  });
	}

	private load_schema(name?:string, view?:string) {
		if (name == null)
			return ([]);
		if (view == null)
			view = "owned";

		const dataObservable = this.schemasvc.get_schema(
			name)
			.map(response => {
				if (! response ) {
					return ([]);
				}
				return (response);
			  //return ({...response, profile_id : this.profile.id});
			});

		this.cachesvc.register('schema' + name, dataObservable)
		.subscribe((cache) => {
			this.cache = cache;
			this.data$ = this.cache.get$;
			this.data$.subscribe(resp => {
				if (resp && this.config.length == 0)  {
					this.title = resp.name;
					if (this.action == "Apply") {
						this.createapplyconfig(resp.cschema_json);
					}  else {
						this.config = JSON.parse(resp.cschema_json);
					}
					console.log("Config schema:",this.config);
					this.configForm = this.creategroup(resp);
				}
			});
			// Refresh the cache immediately.
			//this.cache.refresh().subscribe();
		});
	}

	private creategroup(cschema:any):FormGroup {
	  /*
	   this.configForm = this.formBuilder.group({
			items: this.formBuilder.array([this.createItem()])
		});
	  const group = this.formBuilder.group({
	  },{ updateOn: 'blur' });
	   */
	  const group = this.formBuilder.group({
	  });
	  
		//let items = group.get('items') as FormArray;
	  var i:number = 0;
	  var value:any;
	  var disabled:boolean = false;
	  var objectForm:any;

	  this.config.forEach(control => {
		  value = '';
		  disabled = false;
		  if (control.type != "button") {
			if (control.type == "dcheckbox" || control.type == "checkbox") {
				value = false;
				if (control.type == "dcheckbox")
				  disabled = true;
		   }
		   if (control.type == "dynlist" || control.type == "cdynlist") {
			   let index = this.num_dynoptions;
			   this.dynlist_options[index] = new Array();
			   value = "";
			   /*
			   if (this.action == "Modify" || this.action == "Copy") {
				   value = this.profile.umconfig[control.name + " Name"];
			   }
			   group.addControl(control.name + " Name", this.formBuilder.control(value, Validators.required));
			   this.create_dynoptions(control, index);
			   this.create_dynoptions(control, index).subscribe(ctl => {
				*/
				 this.create_dynoptions(control, index);
				 this.num_dynoptions++;
				 if (this.action == "Modify" || this.action == "Copy" || this.action == "View") {
					 let dynrecs = this.profile.umconfig[control.name];
					 for (let rec of dynrecs) {
						 objectForm = this.formBuilder.group({ });
						 for (const [key, value] of Object.entries(rec)) {
							let item = key;
							objectForm.addControl(item, this.formBuilder.control(value, Validators.required));
						 }
					   /*
						 group.addControl(control.name, this.formBuilder.array([objectForm]));
						*/
						 let farray = group.get(control.name) as FormArray;
						 if (!farray) {
							 group.addControl(control.name, this.formBuilder.array([objectForm]));
						 } else {
							 farray.push(objectForm);
						 }
					 }
				 } else {
					 objectForm = this.formBuilder.group({ });
					 for (let obj of this.dynlist_options[index]) {
						objectForm.addControl(obj.dynlist_item, this.formBuilder.control("", Validators.required));
					 }
					 group.addControl(control.name, this.formBuilder.array([objectForm]));
				 }
				 this.get_dynlist_options(control);
			   //});
			  return;
		   }
		   if ( control.property && control.property.includes("unique")) {
			 if (control.type.includes("select")) {
				 control.options = this.get_unused_options(control);
				 if (this.action == "Modify" || this.action == "View") {
				   value = this.profile.umconfig[control.name];
				   control.options = control.options.concat(value).sort(this.global.customsort);
				 } else {
					value = control.options[0];
				 }
			 }
		   }
		   if (this.action == "View" || this.action == "Modify" || this.action == "Copy") {
				if (this.action == "Copy" ) {
				   value = this.profile.umconfig[control.name];
				   if (control.name == "name") {
					value = '';
				   }
				   if (control.type == "datetime" || control.type == "cdatetime" || control.type == "time" || control.type == "ctime" )  {
					  value = this.date;
				   }
				   if ( control.property && control.property.includes("unique")) {
					 if (control.type.includes("select")) {
						value = control.options[0];
					 }
				   }
				} else if (this.action == "Modify" && control.name == "name") {
					value = this.profile.umconfig[control.name];
					disabled = true;
				} else {
					value = this.profile.umconfig[control.name];
				}
			} else if (this.action == 'Add') {
				if (control.type == "cselect" || control.type == "cinput" || control.type == "cmselect" || control.type == "cradio" || control.type == "dcheckbox" || control.type == "cdatetime" || control.type == "ctime"  ) {
					disabled = true;
				} 
				if (control.type == "select" && control.options.length > 1) {
					value = control.options[0];
				}
				if (control.type == "cmselect")  {
				  value = [];
				}
				if (control.type == "datetime" || control.type == "cdatetime" || control.type == "time" || control.type == "ctime" )  {
				  value = this.date;
				}
			} else if (this.action == 'Apply' || this.action == 'viewApply') {
				if (this.profile.apply_to) {
					let alist = this.profile.apply_to;
					let emap = this.buildMap(alist)
					for (let entry of Array.from(emap.values())) {
						let adata =  JSON.parse(entry.toString());
						if (adata["asset_id"] === control.data.id) {
							  disabled = false;
							  value = true;
							  break;
						}
					}
				}
			}
		  }
		  var validation_list:any
		  if ((this.action !== "View") && (this.action !== "viewApply")) {
			  validation_list = this.create_validations(control, this.action == "Modify");
		  } 
		  
		  if ((this.action == "Apply") || (this.action == "viewApply")) {
			  /*
			  const namegroup = this.formBuilder.group({
			  });
			  namegroup.addControl("selection", this.formBuilder.control(value));
			  namegroup.addControl("order", this.formBuilder.control(""));
			  group.addControl(control.name, namegroup);
			  */
			  group.addControl(control.name, this.formBuilder.control({value:value, disabled:disabled}, Validators.compose(validation_list)));
		  } else {
			  group.addControl(control.name, this.formBuilder.control({value:value, disabled:disabled}, Validators.compose(validation_list)));
		  }
	  });
	  return group;
	}


	create_dynoptions(control:any, index:number):Observable<any> {
		let dyn_rec = new(dynlist_data);
		let dyn_rec2 = new(dynlist_data);

		for (let obj of control.objects) {
		  let objs = obj.split(":", 3);
		  switch(objs[1]) {
				  //case 'Profile' : 
		   case 'ProfileAsset1' : {
				let entity_id = this.global.user_entity;
				let resp = this.profilesvc.get_profiles(entity_id)
			      .map(response => {
					  if (! response || ! response.length) {
						  return response;
					  }
					  dyn_rec.dynlist_field = control.name;
					  dyn_rec.dynlist_oper = objs[0];
					  dyn_rec.dynlist_item = objs[2];
					  dyn_rec.dynlist_list = [];
					  let asset_list = [];
					  for (let profile of response) {
						if (profile.cschema_name != objs[2]) {
						  continue;
						}
						dyn_rec.dynlist_list.push(profile);
						if (objs[1] == 'ProfileAsset') {
							if (profile.apply_to) {
							  let emap = this.buildMap(profile.apply_to)
							  for (let entry of Array.from(emap.values())) {
								  let adata =  JSON.parse(entry.toString());
								  let asset = this.find_asset(adata["asset_id"]);
								  if (asset != null && asset.asset_type == profile.profile_type_id) {
									  asset_list.push(asset);
								  }
							  }
							}
						}
					  }
					  this.dynlist_options[index].push(dyn_rec);
					  if (objs[1] == 'Profile') {
						  return(response);
					  }
					  var uniq = [ ...new Set(asset_list) ];
					  //dyn_rec2.dynlist_list.push(uniq);
					  dyn_rec2.dynlist_list = uniq;
					  dyn_rec2.dynlist_field = control.name;
					  dyn_rec.dynlist_oper = objs[0];
					  dyn_rec2.dynlist_item = "Asset";

					  if (dyn_rec2.dynlist_list && (dyn_rec2.dynlist_list).length > 0) {
						  this.dynlist_options[index].push(dyn_rec2);
					  } else {
						  let schema = this.schemasvc.get_schema_local(objs[2]);
						  if (schema != null) {
							let [parentlist, applyset] = this.load_apply_set_schema(schema);
							dyn_rec2.dynlist_list = applyset;
							this.dynlist_options[index].push(dyn_rec2);
						  }
					  }
					  return(response);
				});
			    return(resp);
		   }
		   break;

		   case 'Profile' : 
		   case 'ProfileAsset' : {
			  dyn_rec.dynlist_field = control.name;
			  dyn_rec.dynlist_oper = objs[0];
			  dyn_rec.dynlist_item = objs[2];
			  dyn_rec.dynlist_list = [];
			  let asset_list = [];
			  for (let profile of this.profiles) {
				if (profile.cschema_name != objs[2]) {
				  continue;
				}
				dyn_rec.dynlist_list.push(profile);
				if (objs[1] == 'ProfileAsset') {
					if (profile.apply_to) {
					  let emap = this.buildMap(profile.apply_to)
					  for (let entry of Array.from(emap.values())) {
						  let adata =  JSON.parse(entry.toString());
						  let asset = this.find_asset(adata["asset_id"]);
						  if (asset != null && asset.asset_type == profile.profile_type_id) {
							  asset_list.push(asset);
						  }
					  }
					}
				}
			  }
			  this.dynlist_options[index].push(dyn_rec);
			  if (objs[1] == 'Profile') {
				  return(objs);
			  }
			  var uniq = [ ...new Set(asset_list) ];
			  //dyn_rec2.dynlist_list.push(uniq);
			  dyn_rec2.dynlist_list = uniq;
			  dyn_rec2.dynlist_field = control.name;
			  dyn_rec.dynlist_oper = objs[0];
			  dyn_rec2.dynlist_item = "Asset";

			  if (dyn_rec2.dynlist_list && (dyn_rec2.dynlist_list).length > 0) {
				  this.dynlist_options[index].push(dyn_rec2);
			  } else {
				  let schema = this.schemasvc.get_schema_local(objs[2]);
				  if (schema != null) {
					let [parentlist, applyset] = this.load_apply_set_schema(schema);
					dyn_rec2.dynlist_list = applyset;
					this.dynlist_options[index].push(dyn_rec2);
				  }
			  }
			  return(objs);
		   }
		   break;

		   case 'Asset' : {
			 break;
		   }
		}
	  }
	}

	get_applied_assets(){
		let appl_list = new Set();

		for (let profile of this.profiles) {
			if (profile.cschema_id != this.profile.cschema_id) {
				continue;
			}
			if (profile.id == this.profile.id) {
				continue;
			}

			let alist = profile.apply_to;
			if (! alist) {
				continue;
			}
			let emap = this.buildMap(alist)
			for (let entry of Array.from(emap.values())) {
				let adata =  JSON.parse(entry.toString());
				if (adata["asset_id"]) {
					appl_list.add(adata["asset_id"]); 
				}
			}
		}
		return appl_list;
	}

	get_unused_options(control:any, options?:any){
	    var olist:any;
	    olist = [];
		if (control.type == "select" || control.type == "cselect" ||
			control.type == "mselect" || control.type == "cmselect") {
			olist = control.options;
			for (let profile of this.profiles) {
				if (profile.cschema_id != this.schema.id) {
					continue;
				}
				let pconfig = JSON.parse(profile.config); 
			    let cvalue = pconfig[control.name];
			    olist = olist.filter( ( el ) => !cvalue.includes( el ) );
			}
		}
		return olist;
	}


	create_validations(control:any, modify_flag:boolean):any[]{
		var validation_list:any[] = [];

		if (control.type == "input" || control.type == "cinput") {
			//	validation_list = this.customvalidator.create_validations("", control.valuetype)
			if (control.valuetype == "email") {
				validation_list = this.customvalidator.create_validations("", control.valuetype)
			} else if (control.valuetype == "text") {
				validation_list.push(Validators.minLength(control.minlength))
				validation_list.push(Validators.maxLength(control.maxlength))
			} else if (control.valuetype == "number") {
				validation_list.push(Validators.min(control.min))
				validation_list.push(Validators.max(control.max))
			}
		}
		if (control.valuetype == "datetime") {
		}

		if (modify_flag) 
			return (validation_list);
		if (control.optional == false) {
			if (control.type == "checkbox" || control.type == "dcheckbox"  ||
				control.valuetype.includes("time")) {
				validation_list.push(Validators.required);
				//validation_list.push(Validators.requiredTrue);
			} else if (control.type == "radio" || control.type == "cradio") {
				validation_list.push(Validators.required);
			} else if (control.type !== "button") {
				validation_list.push(Validators.required);
			}
		}
		return (validation_list);
	}

  buildMap(obj) {
		let map = new Map(Object.entries(obj));
		return map;
  }

  next() {
	  //	  this.fillupSlider.slideNext();
  }

  prev() {	
	  //this.fillupSlider.slidePrev();
  }

  save() {
	console.log("Saved");
	console.log(this.configForm.value);
	  //this.modalCtrl.dismiss(this.configForm.getRawValue());
	this.modalCtrl.dismiss(this.configForm.value);
  }

  cancel() {
	console.log("Cancelled");
	this.modalCtrl.dismiss(null);
  }

  apply() {
	console.log("Saved");
	console.log(this.configForm.value);
	this.modalCtrl.dismiss(this.configForm.getRawValue());
  }

  check_dependencies() {
	  /*
	if (this.action == "Add") {
	  return;
	}
	   */
	for (let conf of this.config) {
		this.onSelectionChanged(conf);
	}
  }

  onSelectionChanged(field:any) {
	console.log(field);
	let formf = this.configForm.get(field.name);
	if (! formf)
	  return;
	let value = formf.value;

	for (let conf of this.config) {
		if (conf.name == field.name || ! conf.only_if_key)
			continue;
		let formc = this.configForm.get(conf.name)

		if (! formc)
		  continue;

		if (conf.only_if_key == field.name) {
			if ((field.type).includes("radio") ||
				(field.type).includes("checkbox")) {
				if (value === JSON.parse(conf.only_if_value)) {
					  formc.enable();
					  let validation_list = this.create_validations(conf, false);
					  formc.setValidators(Validators.compose(validation_list));
					//this.configForm.get(conf.name).enable();
				} else {
				  formc.disable();
					//this.configForm.get(conf.name).disable();
				}
				/*
				if (this.action == "Apply")  {
				  // Disable the controller selection
				  this.configForm.get(field.name).disable();
				}
				 */
			} else {
				if (value === conf.only_if_value) { 
				  formc.enable();
				  let validation_list = this.create_validations(conf, false);
				  formc.setValidators(Validators.compose(validation_list));
					//this.configForm.get(conf.name).enable();
				} else {
				  formc.disable();
					//this.configForm.get(conf.name).disable();
				}
			}
		} 
	}
  }

  private getGroup(control, index?) {
	/*
	  let fg = this.configForm.get(control);
	  let fx = <FormGroup> fg.controls[index].control;
	  return(fx);
	 */
  }

  private getArray(control) {
	  let fa = <FormArray> this.configForm.get(control);
	  return(fa);
  }

  private getvalues(i:any) {
	let a = Array.from(i.values());
	return a;
  }

  private get_dynlist_options(field) {
	var oper:string = "";
	let list = new Array();
	for (let [key, item] of this.dynlist_options.entries()) {
	  for (let [k, option] of item.entries()) {
		if (option.dynlist_field == field.name) {
			list.push(option);
			if (oper == "")
				oper = option.dynlist_oper;
		}
	  }
	}
	if (list.length > 0) {
		this.dynlist_current.set(field.name, list);
		if (this.action != "View")  {
			this.dynlist_current_oper.set(field.name, new Array(oper));
		}
	}
  }

	/*
  private getcurrentvalue(i:any, option:any) {
	  let control = <FormArray>(this.getArray());
	  let g = control.controls[i]['controls'];
	  let alist = g['alist'].value;
	  return (alist.includes(option));
  }

  private getvalue(i:any) {
	  let control = <FormArray>(this.getControls());
	  let g = control.controls[i]['controls'];
	  return (g['alist'].value);
  }

  private getname(i:any) {
	  let control = <FormArray>(this.getControls(i));
	  let g = control.controls[i]['controls'];
      return (g['Name'].value);
  }
	 */

  private rmItem(cfg:any, index:number) {
	 let farray = this.configForm.get(cfg.name) as FormArray;
	 farray.removeAt(index);
  }

  private addItem(cfg:any) {
	 let farray = this.configForm.get(cfg.name) as FormArray;
	 var item = farray.controls[0] as FormGroup;
	 let ctls = item.controls;
	 let objectForm = this.formBuilder.group({ });
	 for (let item of Object.keys(ctls)) {
		objectForm.addControl(item, this.formBuilder.control("", Validators.required));
	 }
	 for (let item of this.dynlist_options[0]) {
	  console.log(item.dynlist_list);
	 }
	  /*
	 Array.from(this.dynlist_options).map(obj => {
		objectForm.addControl(obj[1].dynlist_item, this.formBuilder.control(""));
	 });
	 for (let object of cfg.objects) {
		objectForm.addControl(object, this.formBuilder.control(""));
	 }
	 */
	 farray.push(objectForm);
  }
}
