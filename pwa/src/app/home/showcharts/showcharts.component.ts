//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { AfterViewInit, NgModule, Input, Component, OnInit , Directive, ViewChildren, ViewChild, QueryList, ElementRef} from '@angular/core';
import { Chart } from 'chart.js';
import { GlobalService } from '../../services/global.service';
import { take, map, filter } from 'rxjs/operators';
import { CommonModule } from "@angular/common";
import { IonicModule } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Observable } from  'rxjs/Rx';
import { Cache, CacheService} from 'ionic-cache-observable';


class local_pginfo {
  count:number;
  pageSize:number;
  limit:number;
  offset:number;
}

class ds_info {
	ds:dataset[];
	chart:Chart;
	index:number;
	yval:string[] ;
	counter:string[];
	xval:string;
	xlabel:string;
	ylabel:string;
	title:string ;
	type:string;
	xlabels:string[];
	xunit:string;
	num_samples:number;
	tags:string[];
}

class chart_info {
  id : string;
  index : number;
}

interface dataset {
  label:any;
  labels:any;
  active:any;
  data:any;
  backgroundColor:any; 
  borderColor:any;
  hoverBackgroundColor:any;
  borderWidth:any;
  stack:any;
  fill:boolean;
  spanGaps:boolean;
  type:string;
  pointHoverRadius: number;
  pointHoverBackgroundColor: string;
  pointHoverBorderColor: string;
  pointHoverBorderWidth: number;
}

interface coord {
  x:any;
  y:any;
}

	/*
@Directive({selector: 'canvas'})
export class Canvas {
  @Input() id : string;
}
	 */

@Component({
  selector: 'app-showcharts',
  templateUrl: './showcharts.component.html',
  styleUrls: ['./showcharts.component.scss']
})


export class ShowchartsComponent implements AfterViewInit {
  @Input() resource_type:any;
  @Input() getmore_cb: (d:any, x:any, count:number, period:string) => Observable<any>;
  @Input() cb_arg:any;
  @Input() charts_input:any;

  @ViewChildren('chartref') charts ;
  //  @ViewChildren(Canvas) charts : QueryList<any>;
  /*
  @ViewChild('myTable') table: any;
  @ViewChild("barCanvas") barCanvas: ElementRef;
  @ViewChild("lineCanvas") lineCanvas: ElementRef;
   */
  serializedCharts: string = '';


  private scache: Cache<any>;
  public columns : any;
  public rows : any = [];
  period:string = "today";
  expanded: any = {};   
  data:any = {};
  backuprows = [];
  chart_list = [];
  num_charts = 0;
  ds_list:ds_info[] = [];

  readonly headerHeight = 50;
  readonly rowHeight = 30;
  readonly pageLimit = 10;
  isLoading: boolean;
  enableSummary : boolean = true;
  summaryPosition : string = "top";
  totalCount:number = 100;
  offset:number=0;
  num_samples:number=10;
  bgndColor =  [
		"rgba(255, 99, 132, 0.2)",
		"rgba(54, 162, 235, 0.2)",
		"rgba(255, 206, 86, 0.2)",
		"rgba(75, 192, 192, 0.2)",
		"rgba(153, 102, 255, 0.2)",
		"rgba(255, 159, 64, 0.2)"
	  ];

  borderColor = [
		"rgba(255,99,132,1)",
		"rgba(54, 162, 235, 1)",
		"rgba(255, 206, 86, 1)",
		"rgba(75, 192, 192, 1)",
		"rgba(153, 102, 255, 1)",
		"rgba(255, 159, 64, 1)"
	  ];
  hoverBackgroundColor=  ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56"];

		  
  constructor(private modalCtrl: ModalController, 
			  private cachesvc: CacheService,
			  private el: ElementRef,
	          private global: GlobalService ) { }

  ionViewOnEnter() {
	this.ngOnInit();
  }

  ngAfterViewInit() {
    this.calculateSerializedCharts();
    this.charts.changes.subscribe((r) => { this.calculateSerializedCharts(); });
  }

  calculateSerializedCharts() {
	this.serializedCharts = this.charts.map(p => {
	  console.log("new chart:", p);
	})
  }


  ngOnInit() {
		this.global.ready()
	      .pipe(take(1),)
		  .subscribe(resp => {

			  //this.add_chart();
			  //this.create_charts();
			 this.num_samples = this.add_charts();
			 this.getNext();
		});
  }

  cache: any = {};


  getNext(size?:number) {
	    if (! size) {
		  size = this.num_samples;
	    }
	  const dObservable = this.getmore_cb(
		  null, this.cb_arg, size, this.period)
		    .pipe(take(1),)
		    .map(resp1 => {
			 
			 if (!resp1) {
				 return;
			 }

			  //this.data = resp1;
			 let resp = resp1.records.map(rec => {
				 let local_time = new Date(rec.ts);
				 let tm = local_time.toLocaleString();
				 let ltime = new Date(tm)
				 return ({...rec, local_time : local_time.toLocaleString(), ltime:local_time});
			 });
			 resp1.records = resp;
			 return(resp1);
			  //	 this.update_charts();
		});
		/*
		e => {
			console.log("Error in getting more entries");
			this.totalCount = 0;
			this.data = null;
			this.update_charts();
		});
		 */
	    let key = this.cb_arg.input["asset_name"] + this.cb_arg.input["resource"] + this.period;
		this.cachesvc.register(key, dObservable)
	    .mergeMap((cache: Cache<any>) => {
            // We can call refresh already. The refreshed value will be
            // passed to the get$ BehaviorSubject.
			this.scache = cache; 
            
			this.scache.refresh().subscribe(
			res => {
				if (!res) {
				  return;
				}
				this.data = res;
				this.update_charts();
			},
			e => {
				console.log("Error in getting more entries");
				this.totalCount = 0;
				this.data = null;
				this.update_charts();
			});
			return this.scache.get$;
        }).subscribe((resp) => {
            // This is a long-lived Observable that will be triggered when the
            // Cache is first initialized with the locally stored value,
            // and whenever refresh is called.
			 if (!resp) {
				 return;
			 }
			this.data = resp;
			this.update_charts();
		});
  }

  reset_data( ) {
  }


  close() {
	this.modalCtrl.dismiss(null);
  }

  
  create_dataset(label :string, bgndColor? :any, borderColor? :any,
	               hoverBackgroundColor? :any) :dataset {
	  var dataset1:dataset  = <dataset> {}; 
	  dataset1.label = label;
	  
	  dataset1.data = [];
	  dataset1.backgroundColor = bgndColor;
	  dataset1.borderColor= borderColor[0];
	  dataset1.hoverBackgroundColor = hoverBackgroundColor;
	  dataset1.borderWidth= 1;
	  dataset1.stack = label
	  return dataset1
  }

	//bar_chart(index: number, datasets : dataset[], xlabels: string[], title: string, xlabelstring :string, ylabelstring: string) {

	bar_chart(dsi:ds_info) {
	  var data:any = {};

	  data.labels = [];
	  data.datasets = dsi.ds;
	//data.labels.push(...labels);
		/*
	  for (let ds of datasets) {
			data.datasets.push(ds);
	  }
		 */
		if (dsi.xlabels) {
			data.labels.push(...dsi.xlabels);
		}
	  
		let k = this.charts.toArray()[dsi.index];
		k = k.nativeElement.children["0"].children["0"].children[0];
		console.log(k);
		//dsi.chart = new Chart(this.charts.toArray()[dsi.index].nativeElement.firstElementChild, {
	  dsi.chart = new Chart(k, {
		  //dsi.chart = new Chart(this.barCanvas.nativeElement, {
		type: dsi.type,
		data: data,
			options : {
			maintainAspectRatio: true,
			padding : {
			  top : 50,
			  bottom : 20
			},
			tooltips : {
			  intersect: false,
			  mode: 'index',
			  enabled : true
			},
			legend: {
				display: false,
				fullwidth: true
			},
				title : {
					display :true,
					text : dsi.title
				},
				scales: {
						xAxes: [{
							type: 'time',
							time: {
								minUnit : 'minute',
								unit : dsi.xunit,
								unitStepSize : 15,
								displayFormats : {
								day : "MMM D",
								week : "ll"
								}
							},
							display: true,
							distribution: 'series',
							bounds: 'ticks',
							scaleLabel: {
								display: true,
								labelString: dsi.xlabel
							},
							ticks: {
								source : 'data',
								autoskip : true,
								major: {
									fontStyle: 'bold',
									fontColor: '#FF0000'
								}
							}
						}],
					/*
						xAxes: [{
								display: true,
								scaleLabel: {
									display: true,
									labelString: xlabelstring
								},
						        stacked:true,
								barPercentage: 0.5,
								barThickness: 6,
								maxBarThickness: 8,
								minBarLength: 2,
								gridLines: {
										offsetGridLines: true
								}
						}],
					 */
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: dsi.ylabel
							},
							ticks: {
						        stacked:true
							  //suggestedMin: 0,
							  //	suggestedMax: 100
							}
						}]
				}
			}
	  });
  }

	//doughnut_chart(index: number, datasets : dataset[], title: string) {
	doughnut_chart(dsi:ds_info) {
	  var data:any = {};

	  data.labels = [];
	  data.datasets = dsi.ds;

	  let k = this.charts.toArray()[dsi.index];
	  k = k.nativeElement.children["0"].children["0"].children["0"];
	  dsi.chart = new Chart(k, {
		  //dsi.chart = new Chart(this.doughnutCanvas.nativeElement, {
		type: "doughnut",
			  tooltips : {
				intersect: false,
				mode: 'index',
				enabled : true
			  },
			  data: data,
			  options : {
				  legend: {
					  display: false,
					  fullwidth: false
				  },
				  title : {
					  display :true,
					  position :"top",
					text : dsi.title
				  },
				  tooltips: {
					callbacks: {
					  label: function(tooltipItem, data) {
						var dataset = data.datasets[tooltipItem.datasetIndex];
						var index = tooltipItem.index;
						return dataset.labels[index] + ': ' + dataset.data[index];
					  }
				  }
				}
			  }
	  });
  }



  update_charts() {
	for (let dsi of this.ds_list) {
		  let i = 0;
		  for (let ds of dsi.ds) {
		   ds.data = this.get_values(dsi, i);
		   i++;
		  }
		
		this.create_charts();
		dsi.chart.update();
	  }
  }

  create_charts() {
	  for (let dsi of this.ds_list) {
			/*
			for (let ds of dsi.ds) {
			 ds.data = this.get_values(dsi);
			}
			 */
		    if (dsi.chart) {
				continue;
			}
			switch (dsi.type) {
				case "bar" :
					this.bar_chart(dsi);
					break;
				case "line" :
					this.bar_chart(dsi);
					break;
				case "doughnut" :
					this.doughnut_chart(dsi);
					break;
				case "pie" :
					break;
			}
		}
  }

  add_charts() {
	let num_samples = 10;
	for (let chart of this.charts_input) {
		 let ch = new(chart_info)
		 ch.index = this.num_charts;
		 ch.id = chart.title
		 this.chart_list.push(ch);
		 let dsi = new(ds_info);
		 dsi.ds = []
		 dsi.xval = chart.xval
		 dsi.yval = chart.yval
		 dsi.xlabel = chart.xlabel;
		 dsi.ylabel = chart.ylabel;
		 dsi.title = chart.title;
		 dsi.type = chart.type;
		 dsi.counter = chart.counter;
		 dsi.xlabels = chart.xlabels;
		 dsi.num_samples = chart.num_samples;
		 dsi.tags = chart.tags;
		 dsi.xunit = chart.xunit;
		 dsi.index = this.num_charts;
		 let i = dsi.index;
		 for (let dsn of chart.ds_names) {
			 let colors = this.colors(i);
			 let ds = this.create_dataset(dsn, colors[0], colors[1], colors[2]);
			 dsi.ds.push(ds);
			 i++
		 }
		 this.ds_list.push(dsi);
		 this.num_charts++;
		 if (dsi.num_samples > num_samples) {
			 num_samples = dsi.num_samples;
		 }
	}
	return num_samples;
  }

		/* Sample chart
  add_chart() {
		 let ch = new(chart_info)
		 ch.index = this.num_charts;
		 ch.id = "Test Bar" + ch.index;
		 this.chart_list.push(ch);

		 let dsi = new(ds_info);
		 dsi.ylabel = "Minutes";
		 dsi.xval = "ltime";
		 dsi.xlabel = "Time";
		 dsi.title = "Run/Scheduled";
		 dsi.type = "bar";
		 dsi.index = this.num_charts;
		 dsi.counter = []
		 dsi.counter.push("runtime");
		 dsi.yval = []
		 dsi.yval.push("sum");

		// ds.data = this.get_values("runtime", "ltime", "avg");
		 let ds = this.create_dataset("Runtime", this.bgndColor, this.borderColor, this.hoverBackgroundColor);
		 dsi.ds = []
		 dsi.ds.push(ds);
		 dsi.counter.push("schtime");
		 ds = this.create_dataset("Scheduled", this.bgndColor, this.borderColor, this.hoverBackgroundColor);
		 dsi.yval.push("max");
		 dsi.ds.push(ds);
		 this.ds_list.push(dsi);

	//bar_chart(index: number, datasets : dataset[], xlabels: string[], title: string, xlabelstring :string, ylabelstring: string) 

		 this.num_charts++;
  }
		*/

	get_values(dsi:ds_info, yindex:number) {
		const findInMap = (map, val) => {
		  for (let [k, v] of Object.entries(map)) {
			  if (val.includes(v)) {
				  return true;
			  }
		  }
		  return false;
		}
		const findkeyInMap = (map, val) => {
		  let list = []
		  for (let [k, v] of Object.entries(map)) {
			  if (val.includes(k)) {
				  list.push(k);
			  }
		  }
		  if (list.length > 0) {
			return {alist:list, found:true};
		  }
		  return {alist:list, found:false};
		}

		let list = [];
		if (! this.data) {
			return null;
		}

		for (let entry of this.data.records) {
		  let {alist, found} = findkeyInMap(entry.aggregation_table, dsi.counter)

			if (!found) {
				continue;
			}

			if (dsi.tags) {
				if (entry.kv) {
					if (! findInMap(entry.kv, dsi.tags)) {
						continue;
					}
				} else {
					continue;
				}
			}
			var co_ord1:coord = <coord>{};
			if (dsi.xval) {
				co_ord1.x = entry[dsi.xval]
				co_ord1.y = entry.aggregation_table[dsi.counter[yindex]][dsi.yval[yindex]];
				list.push(co_ord1);
			} else {
				list.push(entry.aggregation_table[dsi.counter[yindex]][dsi.yval[yindex]]);
			}
		}
		return list
	}

  periodChange(event?) {
	  if (event) {
		  this.period = event.target.value;
		  console.log("Selected :", this.period);
	  }
	  this.getNext();
	  //this.scache.refresh().subscribe();
  }
   
  colors(index:number) {
	var bgndColor =  [
		  "rgba(255, 99, 132, 0.5)",
		  "rgba(255, 159, 64, 0.5)",
		  "rgba(54, 162, 235, 0.5)",
		  "rgba(255, 206, 86, 0.5)",
		  "rgba(75, 192, 192, 0.5)",
		  "rgba(153, 102, 255, 0.5)",
		  "rgba(201, 203, 207, 0.5)"
		];

	var borderColor = [
		  "rgba(255,99,132,1)",
		  "rgba(255, 206, 86, 1)",
		  "rgba(54, 162, 235, 1)",
		  "rgba(255, 159, 64, 1)",
		  "rgba(75, 192, 192, 1)",
		  "rgba(153, 102, 255, 1)",
		  "rgba(201, 203, 207, 1)"
		];
	  //var hoverBackgroundColor=  ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56"];
	  var hoverBackgroundColor = [
		'#4dc9f6',
		'#f67019',
		'#f53794',
		'#537bc4',
		'#acc236',
		'#166a8f',
		'#00a950',
		'#58595b',
		'#8549ba'
	  ];

	index = index % 7;
	return [bgndColor[index], borderColor[index], hoverBackgroundColor[index]];
  }
}


@NgModule({
   imports: [IonicModule,CommonModule, NgxDatatableModule],
   exports: [ShowchartsComponent],
   declarations: [ShowchartsComponent],
   providers: [],
})

export class ShowchartsComponentModule {
}
