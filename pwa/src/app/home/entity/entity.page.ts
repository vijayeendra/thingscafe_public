//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { environment, tcapp } from '../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { } from '@angular/core';
import { ActivatedRoute, Router, RouterEvent, NavigationEnd } from '@angular/router';
import { EntityService, entity_info} from '../../services/entity.service';
import { GlobalService } from '../../services/global.service';
import { UserService, user_info, user_cred} from '../../services/user.service';
import { LoginService} from '../../services/login.service';
import { ProvisionService, push_config_input } from '../../services/provision.service';
import { ProfileService} from '../../services/profile.service';
import { AssetService, asset_info} from '../../services/asset.service';
import { ActionSheetController, ToastController, LoadingController, AlertController, NavController, ModalController } from '@ionic/angular';
import { Cache, CacheService} from 'ionic-cache-observable';
import { Subject, Observable } from  'rxjs/Rx';
import { first, map, filter } from 'rxjs/operators';
import { takeUntil } from  'rxjs/operators';
import {EntityFillPage} from './entity-fill/entity-fill.page';

const TOKEN_KEY = "X-Auth-Token";
const USER_NAME = "USER_NAME";
const USER = "USER";
const HOST_URL = "HOST_URL";
const ENTITY_ID = "ENTITY_ID";
const USER_ENTITY_TYPE = "USER_ENTITY_TYPE";
const REFRESH_TOKEN_KEY = "X-Refresh-Token";
const LOGINAS_TOKEN_KEY = "LoginAs-Token";

class loginAs_input {
	user : string;
	refresh_token : string;
}



@Component({
  selector: 'app-entity',
  templateUrl: './entity.page.html',
  styleUrls: ['./entity.page.scss'],
})


export class EntityPage implements OnInit {

	private action:string;
	loginAs:boolean;
	loadinganimation:any;
    carray:string[];
	private unsubscribe$ = new Subject();
	constructor(private entitysvc:EntityService,
		private assetsvc: AssetService,
		private storage: Storage,
		private profilesvc: ProfileService,
	    public  global : GlobalService, 
		private loginsvc: LoginService,
		private usersvc: UserService,
		private provsvc: ProvisionService,
		private cachesvc: CacheService,
		private alertCtrl: AlertController, 
		private modalCtrl: ModalController,
		private loadCtrl: LoadingController,
		private toastCtrl: ToastController,
		private actionCtrl: ActionSheetController,
		private router: Router,
		private aroute: ActivatedRoute,
		private navCtrl:NavController) {

		this.keys = ["name", "type", "phone", "configure"];
		this.title = "Managed Entities";
		this.loginAs = false;
		this.currentview = "";
		this.carray = [];
	    this.loaded = false;
	}

	//rows:any[];

	public data$: Observable<entity_info[]>;   
	public entitydata$: Observable<entity_info>;   
	//public adata$: Observable<asset_info[]>;   
	private entities:any[];
	private cache: Cache<any[]>;
	private ecache: Cache<any>;
	private keys : string[];
	public entity_types : string[];
	private current_entity:string;
	private current_entity_type:number;
	private current_entity_info:any;
	private view:string;
	public title:string;
	private currentview:string;
	private mypriv:string;
	private refresh_token:string;
	private loaded:boolean;

	ngOnInit() {
		this.ViewLoad();
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad");
		this.ViewLoad();
	}

	ionViewWillEnter(){
		console.log("ionViewWillEnter");
	}

	ionViewDidEnter(){
		console.log("DidEnter");
	}

	ionViewWillLeave(){
		console.log("ionViewWillLeave");
	}
	
	ionViewDidLeave(){
		console.log("ionViewDidLeave");
		this.carray.pop();
	}

	async ActOnSelect(type:any, data:any) {
	  var actionSheet:any;
	  if (data.isChecked ) {
			let buttons = [];
			if (this.mypriv === "SPAdmin") {
				buttons.push({
						text: 'Add Administrator',
						role: 'destructive',
						icon: 'person-add',
						handler: () => {
							console.log('Add admin');
							this.handle_operation('Addadmin', type, data);
						}
				});
				buttons.push({
						text: 'Create New Admin Password',
						icon: 'hammer',
						handler: () => {
						console.log('Change Admin password');
						this.handle_operation('Createpw', type, data);
						}
				});
				buttons.push({
						text: 'Delete Administrator',
						role: 'destructive',
						icon: 'trash',
						handler: () => {
							console.log('Del admin');
							this.handle_operation('Deladmin', type, data);
						}
				});
			}
			if (buttons.length > 0) {
				buttons.push({
					text: 'Cancel',
					icon: 'close',
					role: 'cancel',
					handler: () => {
					console.log('Cancel clicked');
					actionSheet.dismiss();
					}
				});
				actionSheet = await this.actionCtrl.create({
				header: 'Operations',
				buttons: buttons});
				await actionSheet.present();
				await actionSheet.onWillDismiss();
				data.isChecked = false;
			}
	  }
	}

	ViewLoad() {

		let id = this.aroute.snapshot.paramMap.get('id');
		let etype = this.aroute.snapshot.paramMap.get('type');
		let context = this.aroute.snapshot.paramMap.get('context');
		this.entitysvc.ready()
	      .pipe(takeUntil(this.unsubscribe$),)
		  .subscribe(resp => {
			if (resp == true) {
				this.loginAs = this.global.loginAs;
				if (id == null) {
					id = this.global.user_entity;
				}
				if (etype == null) {
					etype = String(this.global.user_entity_type);
				}
				if (this.global.user_entity_type <= 2) {
					this.mypriv = "SPAdmin";
					if (this.global.user_entity_type == 2) {
						this.view = "controlled";
					}
				} else {
					this.mypriv = "Other";
				}

				if ((etype != "") && (id != "")) {
					this.current_entity_type = +etype;
					this.current_entity = id;
					this.entity_types = this.entitysvc.get_entity_child_type(this.current_entity_type);
					this.load_linked_entity_page(id, etype);
				} else {
					this.load_entity_page();
				}
				if (context) {
				  this.carray = context.split(',');
				}
			    this.unsubscribe$.next();
			}
		});
	}


	public entity_info_by_name(name:string):entity_info {
		if (this.entities) {
			for(let item of this.entities ) {
				if (item.name == name) {
					return item;
				}
			}
		} else {
			return null;
		}
	}

	private create_entity(data:any):Observable<any>{

		var entity:entity_info = new entity_info(data);
		return this.entitysvc.create_entity(entity, this.action == "Modify");
	}

	private load_entity_page(id?:string, view?:string) {
		if (! id || id == "")
			id = this.current_entity;
		if (!view || view == "")
			view = this.view;

		this.title = "Managed Entities";
	    this.currentview = "Entityview";
	   
	    // Get entity
		const dObservable = this.entitysvc.get_entity(id)
			.map(
			entry => {
				var entity_type_str:string;
				if (! entry ) {
					//this.load_linked_asset_page(id, "owned");
					return ([]);
				} else {
					if (entry.kv && entry.kv["state"] == "deleted") {
						return;
					}
				}
			    this.current_entity_info = entry;
				entity_type_str = this.entitysvc.get_entity_type_str(this.current_entity_info.entity_type);
			    return ({...entry, type :  entity_type_str});
			},
			err => {
				var splitted = err.split(":", 2);
				console.log(splitted);
			}
			);

		this.cachesvc.register('entityinf' + id, dObservable)
		.subscribe((cache) => {
			this.ecache = cache;
			this.entitydata$ = this.ecache.get$;
			// Refresh the cache immediately.
			this.ecache.refresh().subscribe();
		});
		if (this.ecache) {
			this.ecache.refresh().subscribe();
		}

	    // Get child entities
		const dataObservable = this.entitysvc.get_entities(
			id, view)
			.map(response => {
				if (! response || ! response.length) {
					//this.load_linked_asset_page(id, "owned");
					return ([]);
				}
				this.entities = response.filter(item => (!item.kv || item.kv["state"] != "deleted")); 
				//this.entities = response;
				return this.entities.map((item) => {
					var entity_type_str:string;
					//item.name = "<a>" + item.name + "</a>";
					entity_type_str = this.entitysvc.get_entity_type_str(item.entity_type);
					return ({...item, type : entity_type_str});
				});
		});
		this.cachesvc.register('entities' + id, dataObservable)
		.subscribe((cache) => {
			this.cache = cache;
			this.data$ = this.cache.get$;
			// Refresh the cache immediately.
			this.cache.refresh().subscribe();
		});
		if (this.cache) {
			this.cache.refresh().subscribe();
		}
	}

	private checkifSPAdmin():Observable<boolean> {
		if (Number(this.global.user.role) == 0 && this.global.user_entity_type_string == "SERVICE_PROVIDER") {
			return Observable.of(true);
		} else  {
			return Observable.of(false);
		}
	  /*
		this.entitysvc.get_stored_value(USER).then(resp => { 
			let user = resp;
			this.entitysvc.get_stored_value(USER_ENTITY_TYPE).then(resp => { 
				let user_entity_type = resp;
			    if (user['role'] == 0 && user_entity_type == "SERVICE_PROVIDER") {
					return Observable.of(true);
				} else  {
					return Observable.of(false);
				}
			});
		});
		return Observable.of(false);
	   */
	}

	onRefresh(refresher): void {
    // Check if the cache has registered.
		this.entitysvc.ngOnInit();
		this.entitysvc.ready().subscribe(resp => {
			if (this.cache) {
				this.cache.refresh().subscribe(() => {
					// Refresh completed, 
					if (this.ecache) {
						this.ecache.refresh().subscribe(() => {
							refresher.target.complete();
						});
					} else {
						refresher.target.complete();
					}
				}, (err) => {
					// Something went wrong!
					// Log the error and cancel refresher animation.
					console.error('Refresh failed!', err);
					refresher.target.cancel();
					});
			} else {
				  // Cache is not registered yet, so cancel refresher animation.
					refresher.target.cancel();
			}
		});
		setTimeout(() => { refresher.target.complete(); }, 4000);
	}

	load_linked_asset_page(einfo?:any, atype?:string) {
		// Wait for Asset service to be ready
	  //	this.router.navigate(['/home/asset', { id: id, type: atype }]);

	    this.currentview = "Assetview";
	    let carray = [];
		if (this.carray.length > 0) {
			carray = this.carray;
		} 
	    carray.push(einfo.name);

	  	this.navCtrl.navigateForward(['/home/asset', { id: einfo.id, type: atype, priv:this.mypriv, context:carray }]);
	}

	load_linked_entity_page(id?:string, etype?:string) {
		if (etype != null) {
			if (etype == '4' && this.currentview == "Entityview") {
				this.load_linked_asset_page(id, "owned");
			} else {
				this.currentview = "Entityview";
				this.load_entity_page(id, "owned");
			}
		} else {
			//this.navCtrl.navigateForward('/home');
			this.load_entity_page(id, "owned");
		}
	}

	async present_loading(message:string, duration:number) {

	  if (duration > 0) {
		  const loading = await this.loadCtrl.create({
			message: message,
			duration: duration * 1000,
			spinner: 'crescent'
		  });
		  await loading.present();
		  return loading;
	  } else {
		  const loading = await this.loadCtrl.create({
			message: message,
			duration: 200000,
			spinner: 'dots'
		  });
		  await loading.present();
		  return loading;
	  }
	 
	}

	async handle_operation(action:string, etype_str?:any, data?:any) {

	if (action == "Createpw") {
		this.loadinganimation = this.present_loading("Creating password", 5);
		let uinfo = await (this.usersvc.find_user(data.email).toPromise());
		let ucred = new(user_cred);
		ucred.id = uinfo.id;
		var randomstring = Math.random().toString(36).slice(-8);
		ucred.password = randomstring;
		this.usersvc.create_user_cred(ucred, true) // Modify password
		.subscribe(resp3 => {
				this.loadinganimation.then(loading => {
					loading.dismiss();
				});
				let header = 'Please note down password: for user:' + uinfo.email + "=>" + ucred.password;
			this.alertCtrl.create({
					header: header,
					buttons: ['Dismiss']
				}).then(alert => alert.present());
				console.log("Create new password success:", uinfo.email);
		},
		e => {
			this.presenterror(e, "Create new password", e);
		});
	} else if (action == "Addadmin") {
		this.loadinganimation = this.present_loading("Adding", 5);
			// Create user
			let uinfo = new(user_info);
			uinfo.email = data.email;
			uinfo.entity_id = data.id;
			uinfo.saas_entity_name = tcapp.saas;
			uinfo.role = "ADMINISTRATOR";
			uinfo.first_name = "Admin";
			uinfo.last_name = data.name;
			uinfo.kv = new Map();
			uinfo.kv["entity_type"] = data.entity_type.toString();
			this.usersvc.create_user(uinfo).subscribe(resp2 => {
				this.loadinganimation.then(loading => {
					loading.dismiss();
				});
				let ucred = new(user_cred);
				ucred.id = resp2.body.id;
				var randomstring = Math.random().toString(36).slice(-8);
				ucred.password = randomstring;
				this.usersvc.create_user_cred(ucred)
				.subscribe(resp3 => {
						let header = 'Please note down password: for user:' + uinfo.email + "=>" + ucred.password;
					this.alertCtrl.create({
							header: header,
							buttons: ['Dismiss']
						}).then(alert => alert.present());
					console.log("Create user success:", uinfo.email);
				},
				e => {
					this.presenterror(e, "Create Administrator password");
				});
			},
			e => {
					this.presenterror(e, "Add Administrator", e);
		});
	} else if (action == "Deladmin") {
		this.loadinganimation = this.present_loading("Deleting user " +  data.email, 10);
			try {
				let uinfo = await (this.usersvc.find_user(data.email).toPromise());
				let cred_delete = await (this.usersvc.delete_user_object(uinfo, "user-cred").toPromise());
				let user_del = await (this.usersvc.delete_user_object(uinfo, "user").toPromise());
			} catch(error) {
				console.log("User not present or error during delete");
				this.presenterror(error, "Deleting Administrator", "User not present");
			}

			this.loadinganimation.then(loading => {
				loading.dismiss();
			});
	} else if (action == "LoginAs") {
		let input = new(loginAs_input);
		this.loadinganimation = this.present_loading("Performing " +  action, 5);
		input.refresh_token = this.global.refresh_token;
		input.user = data.email;
		//input.user = "admin@" + (data.domain).toLowerCase(); 
		this.loginsvc.loginAs(input).subscribe(resp => {
			  console.log("LoginAs:", resp);

			this.storage.set(LOGINAS_TOKEN_KEY, resp.access_token).then(res => {
				  this.usersvc.store_user_auth(resp);
				  this.entitysvc.loginAs_func(res);
				  this.usersvc.setloginstatus(true);
				  //				  this.navCtrl.navigateRoot('/home');
				  this.loadinganimation.then(loading => {
					  loading.dismiss();
				  });
				  this.loginAs = true;
			      this.global.loginAs = true;
			      this.global.loginAs_token = res;
				  this.global.loginAs_func(data);
				  this.navCtrl.navigateForward(['/home/entity', { id: data.id, type: data.entity_type  }]);

			});
		},
		e => {
			this.presenterror(e, action);
		});

	} else if (action == "LogoutAs") {
		this.loadinganimation = this.present_loading("Performing " +  action, 5);
		this.entitysvc.logoutAs_func();
		this.storage.remove(LOGINAS_TOKEN_KEY).then( res => {
			this.loginAs = false;
			this.global.loginAs = false;
			this.usersvc.delete_user_auth(true);
			this.loadinganimation.then(loading => {
				loading.dismiss();
			});
			this.navCtrl.navigateForward(['/settings', { action: "login"}]);
		  //this.navCtrl.navigateForward(['/home/entity']);
			//this.navCtrl.navigateRoot('/home');
		});
	} else if (action == "Sync") {
		var header:string;
		this.loadinganimation = this.present_loading("Performing " +  action, -1);
		let l = new(push_config_input);
		l.site_id = data.id;
		l.access_token = this.global.token;
		this.provsvc.push_config(l).subscribe(resp1 => {
			header = action + ' succeeded';
			let message = "";
			for (let cresp of resp1.body.response) {
				message = cresp.desc + "\n"
				if (cresp.cmds != null) {
					for (let msg of cresp.cmds) {
						message = message + msg + "\n"; 
					}
				}
				if (cresp.replies != null) {
					for (let msg of cresp.replies) {
						message = message + msg + "\n"; 
					}
				}
			}
			  //this.loadCtrl.dismiss();
			this.loadinganimation.then(loading => {
				loading.dismiss();
			});

			if (this.global.debug) {
			  this.alertCtrl.create({
				  header: header,
				  buttons: [
					  {
						text: 'Dismiss',
						role: 'cancel',
						cssClass: 'secondary',
						handler: () => {
						  console.log('Confirm Cancel');
						}
					  }, {
						text: 'Show',
						handler: () => {
							this.presentToast("Commands/Response", message);
						}
					  }
				  ]
			  }).then(alert => alert.present());
			}
		},
		e => {
			this.presenterror(e, action);
		});
	} else if (action == "Delete") {
		let children_present = false;
		if (data.entity_type <= 3) {
			children_present = this.check_for_child_entities(data.id);
			children_present = true;
		} else if (data.entity_type == 4) {
			//children_present = this.check_for_child_assets(data.id);
			children_present = true;
		}
		if (children_present) {
			const alert = await this.alertCtrl.create({
			  header: 'Warning!',
				message: 'Message <strong>This entity may have children entities/assets. Please delete them first</strong>!!!',
			  buttons: [
				{
				  text: 'Okay',
				  role: 'okay',
				  handler: () => {
					console.log('Confirm Okay');
					// Call delete profile here
					return true;
				  }
				}
			  ]
			});
			await alert.present();
			let resp = await alert.onDidDismiss();
			//return;
		}
		const alert = await this.alertCtrl.create({
		  header: 'Warning!',
		  message: 'Message <strong>Are you sure you want to delete ?</strong>!!!',
		  buttons: [
			{
			  text: 'Cancel',
			  role: 'cancel',
			  cssClass: 'secondary',
			  handler: (blah) => {
				console.log('Confirm Cancel: blah');
				return true;
			  }
			}, {
			  text: 'Okay',
			  role: 'okay',
			  handler: () => {
				console.log('Confirm Okay');
				// Call delete profile here
				return true;
			  }
			}
		  ]
		});
		await alert.present();
		let resp = await alert.onDidDismiss();
		console.log('Delete :', resp.role, data.id);  
		if (resp.role == 'okay') {
			this.loadinganimation = this.present_loading("Deleting user " +  data.email, 10);
			try {
			let uinfo = await (this.usersvc.find_user(data.email).toPromise());
			let cred_delete = await (this.usersvc.delete_user_object(uinfo, "user-cred").toPromise());
			let user_del = await (this.usersvc.delete_user_object(uinfo, "user").toPromise());
			} catch(error) {
				console.log("User not present or error during delete");
			}

			this.loadinganimation.then(loading => {
				loading.dismiss();
			});

			this.loadinganimation = this.present_loading("Performing " +  action, 8);
			// Dont delete it but mark state as deleted.
			if (!tcapp.force_delete_entity) {
				data.kv = new Map();
				data.kv["state"] = "deleted";
				let name = data.name;
				data.name = data.name + "_backup";
			    this.entitysvc.create_entity(data, false)
				  .subscribe(resp1 => {
					data.name = name;
					this.entitysvc.delete_entity(data)
					  .subscribe(resp1 => {
						this.loadinganimation.then(loading => {
							loading.dismiss();
						});
						//this.load_entity_page(this.current_entity, "owned");
						this.cache.refresh().subscribe();
					},
					e => {
						this.presenterror(e, action);
					});
				},
				e => {
					this.presenterror(e, action);
				});
			} else {

				this.entitysvc.delete_entity(data)
				  .subscribe(resp1 => {
					this.loadinganimation.then(loading => {
						loading.dismiss();
					});
					//this.load_entity_page(this.current_entity, "owned");
					this.cache.refresh().subscribe();
				},
				e => {
					this.presenterror(e, action);
				});
			}
		}
		return;
	  } else if (action == "Modify") {
		  this.action = action;
		  this.presentModal(data, etype_str)
	  } else if (action == "Copy") {
		  this.action = action;
		  delete(data.id);
		  this.presentModal(data, etype_str)
	  } else if (action == "Add") {
		  this.action = action;
		  this.presentModal(this.current_entity, etype_str)
	  } 
	}

	check_for_child_entities(entity_id:string){
		let children_present:boolean = false;
		for (let entity of this.entities) {
			if (entity.entity_id == entity_id) {
				children_present = true;
				return children_present;
			}
		}
		return children_present;
	}

	async presenterror(e:any, action?:string, msg?:string) {
		let message = "";
		if (e != "") {
			console.error(e)

			if ( e.response) {
			  for (let cresp of e.response) {
				  message = cresp.desc + "\n"
				  if (cresp.cmds != null) {
					  for (let msg of cresp.cmds) {
						  message = message + msg + "\n"; 
					  }
				  }
				  if (cresp.replies != null) {
					  for (let msg of cresp.replies) {
						  message = message + msg + "\n"; 
					  }
				  }
			  }
			}
		}
		  //this.loadCtrl.dismiss();

		let header = action + ' failed';

		if (msg) {
			header = action + ' failed due to ' + msg ;
		}

		this.loadinganimation.then(loading => {
			loading.dismiss();
		});

		if (message != "") {
		  if (this.global.debug) {
			this.alertCtrl.create({
				header: header,
				buttons: [
					{
					  text: 'Dismiss',
					  role: 'cancel',
					  cssClass: 'secondary',
					  handler: () => {
						console.log('Confirm Cancel');
					  }
					}, {
					  text: 'Show',
					  handler: () => {
							if (e) {
							  this.presentToast("Commands/Response", message);
						  }
					  }
					}
				]
			}).then(alert => alert.present());
			return;
		  }
		} 

		this.alertCtrl.create({
				  header: header,
				  message: e.error,
				  buttons: ['Dismiss']
		}).then(alert => alert.present());
	}

	async presentToast(header:string, msg:any) {
		const toast = await this.toastCtrl.create({
		  message: header + '\n' + msg,
		  color:"dark",
		  cssClass:"toast",
		  showCloseButton: true,
		  translucent: true,
		  position : 'bottom'
		});
		toast.present();
	}

	async presentModal(entity:any, etype_str?:any) {
	  var parent_entity:any = null;

	  if (entity)  {
		console.log("Selected option:", entity['name'], this.action);
	  }

	  if (this.action == "Add") {
		  parent_entity = this.current_entity_info;
		  entity = null;
	  } else {
		  parent_entity = this.current_entity_info;
	  }

	  const modal = await this.modalCtrl.create({
		  component: EntityFillPage,
		  //	component: DetailPage,
		  componentProps: {entity:entity, etype_str:etype_str, action:this.action, parent_entity:parent_entity, entities:this.entities}
	  });

	  let resp = await modal.present();
	  let data = await modal.onDidDismiss();
	  console.log(this.action, ' DATA:', data.data);  
	  if (data.data) {
		  this.loadinganimation = this.present_loading("Performing " +  this.action, 10);
		  if (this.action == "Copy" && data.data.email == entity.email) {
			 console.log(this.action + "Duplicate email");
			 this.presenterror("", this.action, "Duplicate email");
			 return;
		  }
		  this.create_entity(data.data).subscribe(resp1 => {
			this.loadinganimation.then(loading => {
				loading.dismiss();
			});
			if (this.action == "Add" || this.action == "Copy") {
				// Add admin user
				this.cache.refresh().subscribe();
			    if (resp1.body.entity_type == 4) {
				   return;
				}
			    let entity_id = resp1.body.id;
					this.loadinganimation = this.present_loading("", 5);
					console.log("Creating user");
					/*
					let einfo = this.entity_info_by_name(data.data["name"]);
					if (! einfo) {
					  console.log("Cannot get entity info");
					  return;
					}
					 */
					// Create user
					let uinfo = new(user_info);
					uinfo.email = resp1.body.email;
					uinfo.entity_id = entity_id;
					uinfo.saas_entity_name = tcapp.saas;
					uinfo.role = "ADMINISTRATOR";
					uinfo.first_name = "Admin";
					uinfo.last_name = resp1.body.name;
				    uinfo.kv = new Map()
				    uinfo.kv["entity_type"] = (resp1.body.entity_type).toString()
					this.usersvc.create_user(uinfo).subscribe(resp2 => {
						this.loadinganimation.then(loading => {
							loading.dismiss();
						});
						let ucred = new(user_cred);
						ucred.id = resp2.body.id;
						var randomstring = Math.random().toString(36).slice(-8);
						ucred.password = randomstring;
						this.usersvc.create_user_cred(ucred)
						.subscribe(resp3 => {
							  let header = 'Please note down password: for user:' + uinfo.email + "=>" + ucred.password;
							this.alertCtrl.create({
								  header: header,
								  buttons: ['Dismiss']
							  }).then(alert => alert.present());
							console.log("Create user success:", uinfo.email);
						},
						e => {
							this.presenterror(e, this.action);
						});
					},
					e => {
						this.presenterror(e, this.action);
				});
				this.cache.refresh().subscribe();
			}
		},
		e => {
			this.presenterror(e, this.action);
		});
	  }
	  return(resp);
	}

	drilldown(e, arg1) {
		console.log(e.target);
		this.load_linked_asset_page(arg1, "owned");
	}

	labelClick(e, arg1) {
		console.log(e.target);
		let carray = [];
	    if (this.carray.length > 0) {
			carray = this.carray;
		}  
		carray.push(arg1.name);

		if (this.currentview == "Entityview") {
			//			this.router.navigateByUrl('/home/entity;id=$arg1;type=$arg2');
			this.navCtrl.navigateForward(['/home/entity', { id: arg1.id, type: arg1.entity_type , context:carray}]);
		}
	}
}
