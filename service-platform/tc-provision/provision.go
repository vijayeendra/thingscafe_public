//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	plugindefs "gitlab.com/service-platform/tc-mw/plugins"
	tcclient "gitlab.com/service-platform/tc-mw/tc-client"
	utils "gitlab.com/service-platform/tc-mw/utils"

	"github.com/gorilla/mux"
	sarama "gopkg.in/Shopify/sarama.v2"
)

// Consumer represents a Sarama consumer group consumer
type Consumer struct {
	ready   chan bool
	plugin  string
	client  interface{}
	process interface{}
}

var abc plugindefs.Provision_plugin

/*
 * Config profile contain profile for a certain kind of profile
 * described in the profile below
 */
type sync_config_input struct {
	Site_id   string /* asset id */
	Asset_id  string /* asset id */
	Operation string /* Operation */
	Debug     bool
	Encode    string
	Flags     string /* incremental or full */
}

type sync_config_site_input struct {
	Site_name       string /* asset id */
	Controller_name string /* asset id */
	Debug           bool
	Encode          string
	Flags           string /* incremental or full */
}

type asset_operation_input struct {
	Site_id   string /* Site id */
	Asset_id  string /* asset id */
	Operation string /* Operation */
	Debug     bool
	Encode    string
}

type sync_bulk_user_config_input struct {
	Debug     bool
	Operation string /* Operation */
	Encode    string
}

const MAX_SMS_LENGTH = 140

var controller_site_map map[string][]tcclient.Asset_info
var controller_asset_map map[string][]tcclient.Asset_info
var controller_state_map map[string]*utils.Controller_state

//var controller_initialized map[string]bool
//var controller_configured map[string]bool
var controller_sms_config map[string]interface{}
var prov_plugin_map map[string]interface{}

// Get all user-config profiles based on access token
//

func push_config(sync *sync_config_input, w http.ResponseWriter, r *http.Request) {
	//var sync sync_config_input
	var saas_name string
	var cresp *utils.Config_response
	var controllers []tcclient.Asset_info
	var controller *tcclient.Asset_info = nil
	var assets []tcclient.Asset_info
	var ok bool
	var incremental bool = true

	/*
		qp := new(utils.Query_string)
		err := utils.Get_query_params(r, qp)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		} else {
			if qp.App == "" {
					err := errors.New("App name missing in query params")
					http.Error(w, err.Error(), 500)
					return
			} else {
				saas_name = qp.App
			}
		}
	*/

	if sync.Asset_id == "" {
		err := errors.New("Asset id missing in input")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	handle, user_info, err := tcclient.Get_user_from_request(r, w)
	if handle == nil {
		//http.Error(w, err.Error(), 500)
		if err != nil {
			http.Error(w, err.Error(), 500)
		}
		return
	}
	saas_name = user_info.Saas_entity_name

	profiles := tcclient.Get_list_profiles(handle, user_info.Entity_id)
	if profiles == nil {
		err = errors.New("No config profiles created by user")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	cstate, a := Check_Init_state(handle, sync.Site_id, sync.Asset_id)
	if a == nil || cstate == nil {
		err = errors.New("Couldn't find entity related to site")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	if controllers, ok = controller_site_map[sync.Site_id]; !ok {
		log.Println("Cannot find Controller. Aborting config", sync.Site_id)
		err = errors.New("Cannot find Controller. Aborting config")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	for _, ctlr := range controllers {
		if ctlr.Id == sync.Asset_id {
			controller = &ctlr
			break
		}
	}

	var user_config_flag bool = false
	if strings.Contains(r.URL.Path, "push-user-config") {
		user_config_flag = true
	}

	if strings.Contains(sync.Flags, "full") {
		incremental = false
	}

	if controller == nil {
		log.Println("Cannot find Controller. Aborting config", sync.Site_id)
		err = errors.New("Cannot find Controller. Aborting config")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	if assets, ok = controller_asset_map[sync.Asset_id]; !ok {
		log.Println("No assets under controller", sync.Asset_id)
		err = errors.New("No assets under controller. Aborting config")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	//	cstate := controller_state_map[controller.Id]

	if !cstate.Initialized {
		log.Println("Controller not initialized. Aborting sync")
		err = errors.New("Controller not initialized;Aborting sync")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	} else if user_config_flag {
		if !cstate.Configured {
			log.Println("Controller not configured. Aborting sync")
			err = errors.New("Controller not configured;Aborting sync")
			w.Header().Set("Access-Control-Allow-Origin", "*")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}
	entity := tcclient.Get_entity(handle, sync.Site_id)

	encode_flag := sync.Encode

	plugin_proc := prov_plugin_map[saas_name].(plugindefs.Provision_plugin)

	cresp, err = plugin_proc.Send_full_config(entity, controller, profiles, &assets, user_config_flag, cstate, incremental, encode_flag)

	w.Header().Set("Access-Control-Allow-Origin", "*")
	if err != nil {
		if cresp != nil {
			w.WriteHeader(http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusNotFound)
		}
	} else {
		w.WriteHeader(http.StatusOK)
	}

	// Publish cstate
	msg_json, err := json.Marshal(cstate)
	topic := "prov_cstate_compact"
	prov_publish(topic, controller.Id, msg_json)

	if cresp != nil {
		w.Header().Set("Content-Type", "application/json")
		resp_json, err := json.Marshal(cresp)
		fmt.Println("Marshal result:", err, string(resp_json))
		w.Write([]byte(string(resp_json)))
	}

	return
}

var Push_config_site = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var sync_site sync_config_site_input

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	log.Println(string(body))
	err = json.Unmarshal(body, &sync_site)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	handle, user_info, err := tcclient.Get_user_from_request(r, w)
	if handle == nil {
		//http.Error(w, err.Error(), 500)
		if err != nil {
			http.Error(w, err.Error(), 500)
		}
		return
	}

	site := tcclient.Get_site(handle, user_info.Entity_id, sync_site.Site_name)
	controller := tcclient.Get_controller(handle, site.Id, sync_site.Controller_name)
	sync := new(sync_config_input)
	sync.Site_id = site.Id
	sync.Asset_id = controller.Id
	sync.Debug = sync_site.Debug
	sync.Encode = "1c"
	sync.Flags = sync_site.Flags

	push_config(sync, w, r)
	return
})

var Push_config = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var sync sync_config_input

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	log.Println(string(body))
	err = json.Unmarshal(body, &sync)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if sync.Asset_id == "" {
		err = errors.New("Asset id missing in input")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	push_config(&sync, w, r)
	return
})

var Get_controller_state = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var cs_input asset_operation_input
	var cstate *utils.Controller_state
	var ok bool

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	log.Println(string(body))
	err = json.Unmarshal(body, &cs_input)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if !cs_input.Debug {
		err = errors.New("Operation not supported")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if cstate, ok = controller_state_map[cs_input.Asset_id]; !ok {
		log.Println("Controller not initialized. No state")
		err = errors.New("Controller not initialized;")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp_json, err := json.Marshal(cstate)
	fmt.Println("Response Marshalling Error:", err)
	w.Write([]byte(string(resp_json)))

	return
})

var Perform_asset_operation = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var aop_input asset_operation_input
	var cresp *utils.Config_response
	var assets []tcclient.Asset_info
	var saas_name string
	var entity *tcclient.Entity_info

	handle, user_info, err := tcclient.Get_user_from_request(r, w)
	if handle == nil {
		if err != nil {
			http.Error(w, err.Error(), 500)
		}
		return
	}
	saas_name = user_info.Saas_entity_name

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	log.Println(string(body))
	err = json.Unmarshal(body, &aop_input)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if len(controller_site_map) == 0 {
		controller_site_map = make(map[string][]tcclient.Asset_info)
		controller_asset_map = make(map[string][]tcclient.Asset_info)
		controller_state_map = make(map[string]*utils.Controller_state)
		/*
			if len(controller_initialized) == 0 {
				controller_initialized = make(map[string]bool)
				controller_configured = make(map[string]bool)
			}
		*/
	} else {
		if aop_input.Operation != "operation:ResetC" {
			if _, ok := controller_site_map[aop_input.Site_id]; !ok {
				err = errors.New("Controller not configured")
				http.Error(w, err.Error(), 500)
				return
			}
		}
	}

	// Get phone numbers from customer Entity
	pentity := tcclient.Get_entity(handle, aop_input.Site_id)
	if pentity != nil {
		entity = tcclient.Get_entity(handle, pentity.Entity_id)
	}

	if entity == nil {
		err = errors.New("Cannot find entity")
		http.Error(w, err.Error(), 500)
		return
	}

	cstate, asset := Check_Init_state(handle, aop_input.Site_id, aop_input.Asset_id)

	if asset != nil && cstate != nil {
		//cstate := controller_state_map[asset.Id]
		if aop_input.Operation == "operation:ResetC" {
			cstate.Initialized = true
			cstate.Configured = false
			cstate.Mobile = asset.Mobile // Reload mobile number on reset
			cstate.Last_sync_time = 0
			cstate.Last_usersync_time = 0
			cstate.Sms_config = nil
			cstate.Sms_user_config = nil
			cstate.Last_config_pos = -1
			cstate.Last_user_config_pos = -1
			cstate.Site = pentity.Name
			delete(controller_asset_map, asset.Id)

			log.Println("Controller:State Reset", asset.Name, ":::", cstate)
			log.Println("Controller:Mobile:", asset.Id, ":::", cstate.Mobile)

			//controller_initialized[aop_input.Asset_id] = true
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.WriteHeader(http.StatusOK)
			return
		}

		plugin_proc := prov_plugin_map[saas_name].(plugindefs.Provision_plugin)
		encode_flag := aop_input.Encode

		for _, controller := range controller_site_map[aop_input.Site_id] {
			var ok bool

			if asset.Parent_asset_id != controller.Id {
				if asset.Id != controller.Id {
					continue
				}
			}
			if assets, ok = controller_asset_map[controller.Id]; !ok {
				log.Println("No assets under controller", controller.Id)
				err = errors.New("No assets under controller. Aborting config")
				w.Header().Set("Access-Control-Allow-Origin", "*")
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}

			cresp, err = plugin_proc.Send_operation_cmd(&controller, aop_input.Operation, asset, entity, assets, cstate, encode_flag)
			if err != nil {
				log.Println("Send operation command failed", controller.Id)
				http.Error(w, err.Error(), 500)
				return
			}
		}
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		if aop_input.Debug {
			resp_json, err := json.Marshal(cresp)
			fmt.Println("Response Marshalling Error:", err)
			w.Write([]byte(string(resp_json)))
		}
	} else {
		err = errors.New("Asset not found")
		http.Error(w, err.Error(), 500)
		return
	}
	return
})

func Check_Init_state(handle interface{}, site_id string, asset_id string) (*utils.Controller_state, *tcclient.Asset_info) {
	var asset *tcclient.Asset_info = nil
	var cstate *utils.Controller_state = nil

	if len(controller_site_map) == 0 {
		controller_site_map = make(map[string][]tcclient.Asset_info)
		controller_asset_map = make(map[string][]tcclient.Asset_info)
		controller_state_map = make(map[string]*utils.Controller_state)
	} else if controller_site_map[site_id] != nil {
		//	controller_site_map[site_id] = nil
	}

	assets := tcclient.Get_list_assets(handle, site_id)

	pentity := tcclient.Get_entity(handle, site_id)

	if pentity == nil {
		return nil, nil
	}

	for _, a := range *assets {
		var parent_id string
		if a.Root_asset_flag == true {
			if a.Mobile[0] == '+' {
				a.Mobile = a.Mobile[1:]
			}
			_, ok := controller_state_map[a.Id]
			if !ok {
				cfg := new(utils.Controller_state)
				controller_state_map[a.Id] = cfg

				cfg.Initialized = true
				cfg.Configured = false
				cfg.Mobile = a.Mobile
				cfg.Name = a.Name
				cfg.Id = a.Id
				cfg.Last_config_pos = -1
				cfg.Last_user_config_pos = -1
				cfg.Site = pentity.Name
				log.Println("Controller:State Initialized", a.Name, ":::", cfg)
				log.Println("Controller:Mobile:", a.Id, ":::", a.Mobile)
				controller_site_map[site_id] = append(controller_site_map[site_id], a)
				controller_asset_map[a.Id] = nil
			}
		} else {
			controller_asset_map[a.Parent_asset_id] = append(controller_asset_map[a.Parent_asset_id], a)
		}
		if a.Id == asset_id {
			asset = new(tcclient.Asset_info)
			*asset = a
			if a.Root_asset_flag {
				cstate = controller_state_map[a.Id]
			} else {
				parent_id = a.Parent_asset_id
				cstate = controller_state_map[parent_id]
			}
		}
	}

	return cstate, asset
}

var root_entity_id string

func SetRoutes(router *mux.Router) *mux.Router {
	s := router.PathPrefix("/v1/").Subrouter()
	s.HandleFunc("/push-config", Push_config).Methods("POST")
	s.HandleFunc("/push-config-site", Push_config_site).Methods("POST")
	s.HandleFunc("/push-user-config-site", Push_config_site).Methods("POST")
	s.HandleFunc("/push-user-config", Push_config).Methods("POST")
	//s.HandleFunc("/push-bulk-user-config", Push_user_config).Methods("POST")
	s.HandleFunc("/asset-operation", Perform_asset_operation).Methods("POST")
	s.HandleFunc("/get-controller-state", Get_controller_state).Methods("POST")
	Plugin_init(router)

	return router
}

func load_plugins() {
	/*
		//Traverse plugin directory and load plugins
			files, err := ioutil.ReadDir(plugin_dir)
			if err != nil {
				log.Fatal(err)
				return
			}

			for _, file := range files {
				if strings.Contains(file.Name(), ".so") {
					fmt.Println("Adding plugin file.Name()")
					plugins = append(plugins, file.Name())
			}
	*/
}

func Plugin_init(r *mux.Router) {
	//var count int = 0
	//var err error = nil

	p := utils.Getenv("PROVISIONING_PLUGINS", "")

	if p == "" {
		log.Panicf("No plugins defined")
		return
	}

	tcurl := utils.Getenv("THINGSCAFE_SERVICE", "http://localhost:8083/")
	plugin_dir := utils.Getenv("PROVISIONING_PLUGIN_DIR", "./plugins")
	/*
		ctx, cancel := context.WithCancel(context.Background())
		wg := &sync.WaitGroup{}
	*/

	plugins := strings.Split(p, ",")
	prov_plugin_map = make(map[string]interface{})

	for _, plug := range plugins {
		var proc interface{}
		log.Println("Loading config plugin ", plug)
		// Load plugin
		mod := plugin_dir + "/" + plug + "_prov.so"
		proc, err := utils.Load_plugin(mod, "Provision")
		if err != nil {
			continue
		}

		/* Setup the plugin
		 */
		plugin_proc := proc.(plugindefs.Provision_plugin)
		env := plugin_proc.Getenv()
		err, handle := tcclient.Login(tcurl+"tc/", env.Saas, env.User, env.Password, "v1")
		if err != nil {
			log.Println("Plugin access to Thingscafe service failed :", plug)
			continue
		}

		if plugin_proc.Init(handle) < 0 {
			log.Println("Plugin Init failed :", plug)
			continue
		}
		plugin_proc.SetRoutes(r)
		//smsprovider.Register_cb(plug, env.Smsprefix, plugin_proc.Parse_response)
		/*
		 * Setup a new Sarama consumer group
		 */
		prov_plugin_map[plug] = proc
		/*
			topics := plug + "_config"
			group := plug + "_Prov_data"
			client_id := plug + "_config"
			khandle := mykafka.InitConsumerg(group, "range", client_id, -2)
			if err != nil {
				log.Println("Error creating consumer group client: %v", err)
				continue
			}

			consumer := Consumer{
				ready:   make(chan bool),
				plugin:  plug,
				client:  khandle,
				process: proc,
			}

			go func() {
				wg.Add(1)
				defer wg.Done()
				for {
					log.Printf("Starting Client Consume : %v", topics)
					if err := khandle.Consume(ctx, strings.Split(topics, ","), &consumer); err != nil {
						log.Printf("Error from consumer: %v", err)
					}
					// check if context was cancelled, signaling that the consumer should stop
					if ctx.Err() != nil {
						log.Printf("Closing Client due to Context Error : %v", ctx.Err)
						khandle.Close()
						return
					}
					consumer.ready = make(chan bool)
				}
			}()
			<-consumer.ready // Await till the consumer has been set up
			count++
			log.Println("Consumers up for plugin ", plug)
		*/
	}

	/*
		go func() {
			if count > 0 || err == nil {
				log.Println("Consumers up and running!...")
				sigterm := make(chan os.Signal, 1)
				signal.Notify(sigterm, syscall.SIGINT, syscall.SIGTERM)
				select {
				case <-ctx.Done():
					log.Println("terminating: context cancelled")
				case <-sigterm:
					log.Println("terminating: via signal")
					cancel()
				}
			}
		}()
	*/
}

// Setup is run at the beginning of a new session, before ConsumeClaim
func (consumer *Consumer) Setup(sarama.ConsumerGroupSession) error {
	// Mark the consumer as ready
	log.Println("Setup for Plugin:", consumer.plugin)
	close(consumer.ready)
	return nil
}

// Cleanup is run at the end of a session, once all ConsumeClaim goroutines have exited
func (consumer *Consumer) Cleanup(sarama.ConsumerGroupSession) error {
	log.Println("Cleanup for Plugin:", consumer.plugin)
	return nil
}

// ConsumeClaim must start a consumer loop of ConsumerGroupClaim's Messages().
func (consumer *Consumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	log.Println("Claim for Plugin:", consumer.plugin)
	for message := range claim.Messages() {
		log.Printf("Message claimed: value = %s, timestamp = %v, topic = %s, partition=%d", string(message.Value), message.Timestamp, message.Topic, message.Partition)
	}
	return nil
}
