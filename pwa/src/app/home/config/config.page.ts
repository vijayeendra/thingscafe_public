//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { environment, tcapp } from '../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterEvent, NavigationEnd } from '@angular/router';
import { EntityService, entity_info} from '../../services/entity.service';
import { AssetService, asset_info} from '../../services/asset.service';
import { GlobalService } from '../../services/global.service';
import { ProfileService, config_profile} from '../../services/profile.service';
import { SchemaService, config_schema} from '../../services/schema.service';
import { ProvisionService, push_config_input } from '../../services/provision.service';
import { ToastController, LoadingController, AlertController, NavController, ModalController , MenuController} from '@ionic/angular';
import { Cache, CacheService} from 'ionic-cache-observable';
import { Observable } from  'rxjs/Rx';
import { map, filter } from 'rxjs/operators';
import { SchemaPage } from './schema/schema.page';
import { take } from  'rxjs/operators';

export class apply_data {
	site_id:string;
	site_name:string;
    asset_id:string;
    asset_name:string;
    parent_asset_id:string;
    type_name:string;
    type_id:string;
}

@Component({
  selector: 'app-config',
  templateUrl: './config.page.html',
  styleUrls: ['./config.page.scss'],
})

export class ConfigPage implements OnInit {
	public action:string;
	private selectedsite:any;
	public cprofiles:config_profile[];   
	public data$: Observable<config_profile[]>;   
	//public adata$: Observable<asset_info[]>;   
	public sites$: Observable<any[]>;   
	public assets$: Observable<any[]>;   
	public assets: any[];   
	private cache: Cache<any[]>;
	private pacache: Cache<any[]>;
	private assetcache: Cache<any[]>;
	private sitecache: Cache<any[]>;
	private keys : string[];
	public title:string;
    public schema_list:any[];
	private user_entity_type:number;
    private view_type:string;
    carray:string[];
	loadinganimation:any;

	constructor(private entitysvc:EntityService,
		private assetsvc: AssetService,
		private profilesvc: ProfileService,
	    private global : GlobalService, 
		private cachesvc: CacheService,
		private provsvc: ProvisionService,
		private schemasvc: SchemaService,
		private alertCtrl: AlertController, 
		private loadCtrl: LoadingController,
		private toastCtrl: ToastController,
		private modalCtrl: ModalController,
		private menuCtrl: MenuController,
		private router: Router,
		private aroute: ActivatedRoute,
		private navCtrl:NavController) {
		this.title = "Config Profiles";
		/*
		this.aroute.params.subscribe(params => {
		  console.log(params);
		  if (params['id']) { 
		    this.load_linked_profiles_page(params['id']);
		  } 
		});
		this.load_linked_profiles_page();
		 */
		this.menuCtrl.enable(true, 'first');
	    this.carray = [];
	    this.cprofiles = [];
	}

	ngOnInit() {
		this.ViewLoad();
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad");
		this.ViewLoad();
	}

	ionViewWillEnter(){
		console.log("ionViewWillEnter");
	}

	ionViewDidEnter(){
		console.log("DidEnter");
	}

	ionViewWillLeave(){
		console.log("ionViewWillLeave");
	}

	ionViewDidLeave(){
		console.log("ionViewDidLeave");
		this.carray.pop();
	}

	ViewLoad() {

		this.schemasvc.ready().subscribe(resp => {
		  if (resp == true) {
			let entity_id = this.aroute.snapshot.paramMap.get('entity_id');
			let asset_id = this.aroute.snapshot.paramMap.get('asset_id');
			let type = this.aroute.snapshot.paramMap.get('type');
			let atype_id = this.aroute.snapshot.paramMap.get('atype_id');
			let controller_id = this.aroute.snapshot.paramMap.get('controller_id');
			let carray = this.aroute.snapshot.paramMap.get('context');

			if (carray.length > 0) {
			  let ccarray = carray.split(',');
			  this.carray = ccarray.slice(-3, ccarray.length);
			}

			this.profilesvc.ready()
			  .pipe(take(2),)
			  .subscribe(resp => {
				if (resp == true) {
				  if (asset_id && atype_id) {
					//this.user_entity_type = this.global.user_entity_type;
					  this.load_profiles_page(entity_id, atype_id);
					  this.load_entities();
					  this.load_profiles_page_by_asset(entity_id, asset_id);
					  this.schema_list = this.schemasvc.schema_list;
					  this.schema_list = this.schemasvc.schema_list.filter(schema => schema.cschema_type_id == atype_id);
					  this.view_type = "AssetContextual"
				  } else if (atype_id) {
					  this.load_linked_profiles_page(this.global.user_entity, atype_id);
					  this.schema_list = this.schemasvc.schema_list.filter(schema => schema.cschema_type_id == atype_id);
					  this.view_type = "Contextual"
				  } else {
					  this.schema_list = this.schemasvc.schema_list;
					  this.load_linked_profiles_page();
					  this.view_type = "Root"
				  }
			  }
			});
			//this.profilesvc.refresh();
		  }
		});
	}

	load_linked_profiles_page(entity_id?:string, id?:string) {
		this.profilesvc.ready().subscribe(resp => {
			if (resp == true) {
				// Sites to be presented for applying configuration
				this.user_entity_type = this.global.user_entity_type;
				this.load_entities();
				if (entity_id && id) {
					this.load_profiles_page(entity_id, id);
				} else {
					this.load_profiles_page();
				}
			}
		});
	}

	private update_applylist(profile:config_profile, data:any, removal:string[]):Observable<any>{
		var temp:any;
		const mapToObj = m => {
		  let a = Array.from(m).sort(customsort);
			//return Array.from(m).reduce((obj, [key, value]) => {
		  let objnew = a.reduce((obj, [key, value]) => {
			  if (! removal.includes(key)) {
				  obj[key] = JSON.stringify(value);
			  }
			  return obj;
		  }, {});
		  return objnew;
		};

		var customsort = function (a, b) {
			let m1 = a[1]["asset_name"].match(/(\d+)/g)[0];
			let m2 = b[1]["asset_name"].match(/(\d+)/g)[0];
			return (Number(m1) - Number(m2));
		}

		const buildMap = obj => {
			//let bmap = new Map(Object.entries(obj));
			
			let bmap = new Map();
			Object.keys(obj).forEach(key => {
				bmap.set(key, JSON.parse(obj[key]));
			});
			return bmap;
		};

		let amap = new Map(data.map(x => [x.asset_id, x] as [string, any]));
		//		let amap = new Map(data.map(x => [x.asset_id, JSON.stringify(x)] as [string, string]));
		if (profile.apply_to) {
			let emap = buildMap(profile.apply_to)
			let mergedmap = new Map([...emap, ...amap]);
			temp = mapToObj(mergedmap);
		} else {
			temp = mapToObj(amap);
		}
		//temp = mapToObj(amap);

		//let mergedmap = new Map([...Array.from(amap.entries()), ...Array.from(profile.apply_to.entries())]);
		profile.apply_to = temp;
		return this.profilesvc.update_profile(profile);
	}

	private create_profile(data:any, schema:config_schema, origprofile:any):Observable<any>{

		var profile:config_profile = new (config_profile);

		if (origprofile) {
			// Copy/Modify workflow
			if (this.action == "Modify") {
				profile.id = origprofile.id;
				data['name'] = origprofile.name;
				profile.name = origprofile.name;
				profile.apply_to = origprofile.apply_to;
			} else {
				profile.name = data['name'];
				profile.apply_to = null;
			}
			profile.cschema_id = origprofile.cschema_id;
			profile.cschema_name = origprofile.cschema_name;
			profile.aschema_id = origprofile.aschema_id;
			profile.aschema_name = origprofile.aschema_name;
			profile.profile_type_id = origprofile.profile_type_id;
			profile.profile_type_name = origprofile.profile_type_name;
			// Update the order and type
			profile.profile_type = schema.cschema_type;
			profile.config_order = schema.config_order;
			profile.kv = schema.kv;
		} else {
			profile.cschema_id = schema.id;
			profile.cschema_name = schema.name;
			profile.aschema_id = schema.apply_schema_id;
			profile.aschema_name = schema.apply_schema_name;
			profile.profile_type = schema.cschema_type;
			profile.profile_type_id = schema.cschema_type_id;
			profile.config_order = schema.config_order;
			profile.profile_type_name = schema.cschema_type_name;
			profile.name = data['name'];
			profile.kv = schema.kv;
			profile.apply_to = null;
		}
		profile.config = JSON.stringify(data);
		return this.profilesvc.create_profile(profile, this.action == "Modify");
	}

	private mapToJson(map) {
		return JSON.stringify([...map]);
	}


	private load_profiles_page_by_asset(entity_id:string, asset_id:string) {
		if (! entity_id)  {
			entity_id = this.global.user_entity;
		}

		const dataObservable = this.profilesvc.get_profiles_by_asset(asset_id)
		  .map(response => {
			  if (! response || ! response.length) {
				  return ([]);
			  }
			  return response.map((item) => {
				  let config = JSON.parse(item.config);
				  return ({...item, umconfig:config});
			  });
		});

		this.cachesvc.register('profiles' + asset_id, dataObservable)
		.subscribe((cache) => {
			this.pacache = cache;
			this.data$ = this.pacache.get$;
			// Refresh the cache immediately.
			this.pacache.refresh().subscribe();
		});
	}

	private load_profiles_page(entity_id?:string, atype?:string) {

		if (! entity_id)  {
			entity_id = this.global.user_entity;
		}

		const dataObservable = this.profilesvc.get_profiles(entity_id)
		  .map(response => {
			  if (! response || ! response.length) {
				  return ([]);
			  }
			  this.cprofiles = response;
			  if (atype) {
				  return response.filter(item => item.profile_type_id == atype).map((item) => {
					  let config = JSON.parse(item.config);
					  return ({...item, umconfig:config});
				  });
			  } else {
				  return response.map((item) => {
					  let config = JSON.parse(item.config);
					  return ({...item, umconfig:config});
				  });
			  }
		});
		this.cachesvc.register('profiles' + entity_id + atype, dataObservable)
		.subscribe((cache) => {
			this.cache = cache;
			this.data$ = this.cache.get$;
			// Refresh the cache immediately.
			this.cache.refresh().subscribe();
		});
	}

	// Load list of sites and present it
	private load_entities(entity_type?:number) {
		var load_type:string;
		let entity_id = this.global.user_entity;
		if (!entity_type) {
			if (this.global.user_entity_type < 3) {
				load_type = "controlled"
			} else {
				load_type = "owned"
			}
		}
		//load sites by default
		let match_entity_type = 4;
		const dataObservable = this.entitysvc.get_entities(
			entity_id, load_type)
			.map(response => {
				if (! response || ! response.length) {
					return ([]);
				}
				return response.filter(item => item.entity_type == match_entity_type);
			});

		this.cachesvc.register(load_type + 'sites' + entity_id, dataObservable)
			.subscribe((cache) => {
			  this.sitecache = cache;
			  this.sites$ = this.sitecache.get$;
			  // Refresh the cache immediately.
			  this.sitecache.refresh().subscribe();
		});
	}

	onRefresh(refresher): void {
    // Check if the cache has registered.
		this.profilesvc.refresh();
		this.profilesvc.ready().subscribe(resp => {
			if (this.cache) {
				this.cache.refresh().subscribe(() => {
				// Refresh completed, complete the refresher animation.
					if (this.sitecache) {
						this.sitecache.refresh().subscribe(() => {
							refresher.target.complete();
						});
					} 
					if (this.assetcache) {
						this.assetcache.refresh().subscribe(() => {
							refresher.target.complete();
						});
					} else {
						refresher.target.complete();
					}
				}, (err) => {
					// Something went wrong!
					// Log the error and cancel refresher animation.
					console.error('Refresh failed!', err);
					refresher.target.cancel();
				});
			} else {
			  // Cache is not registered yet, so cancel refresher animation.
			  refresher.target.cancel();
			}
		});
		setTimeout(() => { refresher.target.complete(); }, 4000);
	}

	labelClick(e, arg1, arg2) {
	  console.log(e.target);
	  //this.router.navigate(['/config', { id: arg1, type: "controlled" }]);
	  this.navCtrl.navigateForward(['/config', { id: arg1, type: "details" }]);
	}

	async present_loading(message:string, duration:number) {

	  if (duration > 0) {
		  const loading = await this.loadCtrl.create({
			message: message,
			duration: duration * 1000,
			spinner: 'crescent'
		  });
		  await loading.present();
		  return loading;
	  } else {
		  const loading = await this.loadCtrl.create({
			message: message,
			duration: 200000,
			spinner: 'dots'
		  });
		  await loading.present();
		  return loading;
	  }
	}

	async handle_operation(action:string, data?:any, schema?:any){
	  if (action == "Sync") {
		  this.loadinganimation = this.present_loading("Performing " +  action, -1);
		  let l = new(push_config_input)
		  if (this.global.loginAs_token) {
			  l.access_token = this.global.loginAs_token;
		  } else {
			  l.access_token = this.global.token;
		  }
		  l.debug = this.global.debug;
		  this.provsvc.push_user_config(l, true).subscribe(resp1 => {
			var header:string;
			this.loadinganimation.then(loading => {
				loading.dismiss();
			});
			if (resp1.status != 200) {
				header = action + ' failed';
			} else {
				header = action + ' succeeded';
			}
			if (this.global.debug) {
			  let message = "";
			  for (let cresp of resp1.body.response) {
				  message += cresp.name + ":" + cresp.desc + "\n"
				  if (cresp.cmds != null) {
					  for (let msg of cresp.cmds) {
						  message = message + msg + "\n"; 
					  }
				  }
				  if (cresp.replies != null) {
					  for (let msg of cresp.replies) {
						  message = message + msg + "\n"; 
					  }
				  }
			  }
			  //this.presentToast(message);
			  this.alertCtrl.create({
				  header: header,
				  buttons: [
					  {
						text: 'Dismiss',
						role: 'cancel',
						cssClass: 'secondary',
						handler: () => {
						  console.log('Confirm Cancel');
						}
					  }, {
						text: 'Show',
						handler: () => {
							this.presentToast("Commands/Response", message);
						}
					  }
				  ]
			  }).then(alert => alert.present());
			}
		  },
		  e => {
			  this.presenterror(e, action);
		  });
	  } else if (action == "Delete") {
		  const alert = await this.alertCtrl.create({
		  header: 'Warning!',
		  message: 'Message <strong>Are you sure you want to delete ?</strong>!!!',
		  buttons: [
			{
			  text: 'Cancel',
			  role: 'cancel',
			  cssClass: 'secondary',
			  handler: (blah) => {
				console.log('Confirm Cancel: blah');
				return true;
			  }
			}, {
			  text: 'Okay',
			  role: 'okay',
			  handler: () => {
				console.log('Confirm Okay');
				// Call delete profile here
				return true;
			  }
			}
		  ]
		});
		await alert.present();
		let resp = await alert.onDidDismiss();
		console.log('Delete :', resp.role, data.id);  
		if (resp.role == 'okay') {
			this.profilesvc.delete_profile(data)
			  .subscribe(resp1 => {
				if (this.pacache)
					this.pacache.refresh().subscribe();
				else if (this.cache) 
					this.cache.refresh().subscribe();
			},
			e => {
				this.presenterror(e, action);
			});
		}
		return;
	  } else if (action == "Modify" || action == "View") {
		  this.action = action;
		  this.presentModal(data, schema);
	  } else if (action == "Copy") {
		  this.action = action;
		  delete(data.id);
		  this.presentModal(data, schema);
	  } else if (action == "Add") {
		  this.action = action;
		  this.presentModal(data, schema);
	  } 
	}

	async presentToast(header:string, msg:any) {
		const toast = await this.toastCtrl.create({
		  message: header + '\n' + msg,
		  color:"dark",
		  cssClass:"toast",
		  showCloseButton: true,
		  translucent: true,
		  position : 'bottom'
		});
		toast.present();
	}


	async showsites(profile:any){
		var inputs:any[] = [];
		var esites:any[] = [];

	    if (this.view_type.includes("Contextual")) {
		  let controller_id = this.aroute.snapshot.paramMap.get('controller_id');
		  let site_id = this.aroute.snapshot.paramMap.get('entity_id');
		  this.sites$.subscribe(sites => {
			  for (let site of sites){
				if (site.id == site_id) {
				  this.selectedsite = site;
				  if (controller_id == "") {
					this.load_assets(site_id, "owned");
				  } else {
					this.load_assets(controller_id, "controlled");
				  }
				  return;
				}
			  }
		  });
		  return(Observable.of('okay'));
		}

		if (profile.apply_to) {
		  let alist = profile.apply_to;
		  let emap = this.buildMap(alist)
			Array.from(emap.values()).forEach(value =>  {
			  let adata =  JSON.parse(value.toString());
			  esites.push(adata["site_id"]);
		  });
		}

		this.sites$.subscribe(sites => {
			for (let site of sites){
				let checked = false;
				if (esites.includes(site.id))
				  checked = true;
				inputs.push({
				  type: 'radio',
				  label: site.name,
				  value: site,
				  checked: checked
				});
			}
		});

		let alert = await this.alertCtrl.create({
		  header: 'Select Site',
		  inputs:  inputs,
		  buttons: [
			{
			  text: 'Cancel',
			  role: 'cancel',
			  cssClass: 'secondary',
			  handler: (blah) => {
				console.log('Confirm Cancel: blah');
				return true;
			  }
			}, {
			  text: 'Okay',
			  role: 'okay',
			  handler: () => {
				console.log('Confirm Okay');
				// Call delete profile here
				return true;
			  }
			}
		  ]
		});
		await alert.present();
		let resp =  await alert.onDidDismiss();
		console.log('Apply :', resp);  
		if (resp.role == 'okay') {
			this.selectedsite = resp.data.values;
		    this.load_assets(this.selectedsite.id, "owned");
		  /*
			const dataObservable = this.assetsvc.get_assets(this.selectedsite.id, "owned").map(response => {
				if (! response || ! response.length) {
						return ([]);
					}
					return(response);
			});
			this.cachesvc.register('ownedassets' +this.selectedsite.id, dataObservable).subscribe((cache) => {
				this.assetcache = cache;
				this.assets$ = this.assetcache.get$;
			});
			this.assetcache.refresh().subscribe();
		   */
		}
		return(Observable.of(resp.role));
	}

    load_assets(id:any, atype:string) {
	  const dataObservable = this.assetsvc.get_assets(id, atype).map(response => {
		  if (! response || ! response.length) {
				  return ([]);
			  }
			  return(response);
	  });
	  this.cachesvc.register('ownedassets' + id, dataObservable).subscribe((cache) => {
		  this.assetcache = cache;
		  this.assets$ = this.assetcache.get$;
	  });
	  this.assetcache.refresh().subscribe();
	}


	buildMap(obj) {
		let map = new Map(Object.entries(obj));
		return map;
	}


	async applyview(profile:any, schema:any) {
		var modal_presented:boolean = false;
		if (schema.cschema_type == "app_config") {

		}

		let res = await this.showsites(profile);

		if (this.selectedsite) {
			if (profile.aschema_id) {
				console.log(this.assets);
				this.assets$.subscribe(assets => {
				  if (assets && assets.length > 0) {
					(async () => {
						if (! modal_presented) {
							let action = 'Apply';
							if (this.global.user_entity_type > schema.allowed_entity_type) {
								action='viewApply';
							}
							modal_presented = true;
							const modal = await this.modalCtrl.create({
								component: SchemaPage,
								//	component: DetailPage,
								componentProps: {profile:profile, schema_name:profile.aschema_name, action:action, applyset:assets, profiles:this.cprofiles}
							});
							let resp = await modal.present();
							let data = await modal.onDidDismiss();

							let src = data.data;
							if (src == null) {
								console.log(this.action, ' DATA:', data.data);  
								return;
							}
							this.loadinganimation = this.present_loading("Performing Apply", 5);
							console.log(this.action, ' DATA:', data.data);  
							var update:apply_data[];
							var removal:string[];
							update = [];
							removal = [];
							var adata:apply_data; 
							for (let asset_id of Object.keys(src)) {
								/*
								if (asset_id == 'submit' || src[asset_id] != true) 
									continue;
								 */
								if (asset_id == 'submit' ) 
									continue;
								
								if (src[asset_id] != true) {
									removal.push(asset_id);
									continue;
								}
								let asset = this.get_asset(assets, asset_id);
								if (asset == null) {
									removal.push(asset_id);
									continue;
								}
								adata = new (apply_data);
								adata.site_id = this.selectedsite.id;
								adata.site_name = this.selectedsite.name;
								adata.parent_asset_id = asset.parent_asset_id;
								adata.asset_id = asset.id;
								adata.asset_name = asset.name;
								adata.type_name = profile.profile_type_name;
								adata.type_id = profile.profile_type_id;
								update.push(adata);
							}
							let json_data = JSON.stringify(update); 
							console.log('Updated data:', json_data);
							this.update_applylist(profile, update, removal)
							.subscribe(resp1 => {
								this.loadinganimation.then(loading => {
									loading.dismiss();
								});
								  // Refresh the caches
								if (this.pacache)
									this.pacache.refresh().subscribe();
								else if (this.cache)
									this.cache.refresh().subscribe();
							},
							e => {
								this.presenterror(e, action);
							});
						}
					})();
				  }
				});
			} 
		}
		// Refresh the cache immediately.
		//if (this.assetcache) 
		//	this.assetcache.refresh().subscribe();
	}

	private get_asset(assets:any, id:string) {
		for (let item of assets) {
			if (id == item.id)
				return item;
		}
	    return null
	}

	get_parent_asset(assets:any, asset_id:string) {
		for (let asset of assets) {
			if (asset.id == asset_id) {
				return asset.parent_asset_id;
			}
		}
		return "";
	}


	async present_app_config(profile?:any, schema?:any) {
	  //var cdata:any;
	  var origprofile:any;
	  var modal_presented:boolean = false;
	  

	  if (this.action == "Add") {
		//	  cdata = schema.name;
		  origprofile = '';
	  } else {
		// cdata = profile.cschema_name;
		  origprofile = profile;
	  }

	  let assets = []
			  

	  modal_presented = true;
	  const modal = await this.modalCtrl.create({
		  component: SchemaPage,
		  //	component: DetailPage,
		//componentProps: {profile:profile, schema_name:cdata, action:this.action}
		  componentProps: {profile:profile, schema:schema, action:this.action, applyset:assets, profiles:this.cprofiles}
	  });

	  let resp = await modal.present();
	  let data = await modal.onDidDismiss();
	  console.log(this.action, ' DATA:', data.data);  

	  if (this.action == "View") {
		  return(resp);
	  }

	  if (data.data) {
		  this.loadinganimation = this.present_loading("Performing " +  this.action, 5);
		  this.create_profile(data.data, schema, origprofile)
			  .subscribe(resp1 => {
				this.loadinganimation.then(loading => {
					loading.dismiss();
				});
				if (this.cache)
					this.cache.refresh().subscribe();
				  /*
				if (this.pacache)
					this.pacache.refresh().subscribe();
				else if (this.cache)
					this.cache.refresh().subscribe();
				   */
		  },
		  e => {
			  this.presenterror(e, this.action);
		  });
	  }
	  return(resp);
	}

	async presentModal(profile?:any, schema?:any) {
	  //var cdata:any;
	  var origprofile:any;
	  var modal_presented:boolean = false;

	  if (schema.cschema_type == "app_config") {
		  this.present_app_config(profile, schema);
		  return;
	  }

	  if (this.action == "Add") {
		//	  cdata = schema.name;
		  origprofile = '';
	  } else {
		// cdata = profile.cschema_name;
		  origprofile = profile;
	  }
			  

	  let controller_id = this.aroute.snapshot.paramMap.get('controller_id');
	  if (controller_id == "") {
		let site_id = this.aroute.snapshot.paramMap.get('entity_id');
		this.load_assets(site_id, "owned");
	  } else {
		this.load_assets(controller_id, "controlled");
	  }

	  this.assets$.subscribe(assets => {
		if (assets && assets.length > 0) {
		  (async () => {
			if (modal_presented) {
			  return;
			}
			modal_presented = true;
			const modal = await this.modalCtrl.create({
				component: SchemaPage,
				//	component: DetailPage,
			  //componentProps: {profile:profile, schema_name:cdata, action:this.action}
				componentProps: {profile:profile, schema:schema, action:this.action, applyset:assets, profiles:this.cprofiles}
			});
			let resp = await modal.present();
			let data = await modal.onDidDismiss();
			console.log(this.action, ' DATA:', data.data);  

			if (this.action == "View") {
				return(resp);
			}

			if (data.data) {
				this.loadinganimation = this.present_loading("Performing " +  this.action, 5);
				this.create_profile(data.data, schema, origprofile)
					.subscribe(resp1 => {
					  this.loadinganimation.then(loading => {
						  loading.dismiss();
					  });
					  if (this.cache)
						  this.cache.refresh().subscribe();
						/*
					  if (this.pacache)
						  this.pacache.refresh().subscribe();
					  else if (this.cache)
						  this.cache.refresh().subscribe();
						 */
				},
				e => {
					this.presenterror(e, this.action);
				});
			}
			return(resp);
		  })();
		}
	  });
	}

	async presenterror(e:any, action?:string, msg?:string) {
		let message = "";
		if (e != "") {
			console.error(e)

			if ( e.response) {
			  for (let cresp of e.response) {
				  message = cresp.desc + "\n"
				  if (cresp.cmds != null) {
					  for (let msg of cresp.cmds) {
						  message = message + msg + "\n"; 
					  }
				  }
				  if (cresp.replies != null) {
					  for (let msg of cresp.replies) {
						  message = message + msg + "\n"; 
					  }
				  }
			  }
			}
		}
		  //this.loadCtrl.dismiss();

		let header = action + ' failed:' + e.error;

		if (msg) {
			header = action + ' failed due to ' + msg ;
		}

		this.loadinganimation.then(loading => {
			loading.dismiss();
		});

		if (header != "") {
			this.alertCtrl.create({
				header: header,
				buttons: [
					{
					  text: 'Dismiss',
					  role: 'cancel',
					  cssClass: 'secondary',
					  handler: () => {
						console.log('Confirm Cancel');
					  }
					}, {
					  text: 'Show',
					  handler: () => {
							if (e) {
								if (message != "") {
									  this.presentToast("Commands/Response", message);
								}
						  }
					  }
					}
				]
			}).then(alert => alert.present());
			return;
		} 
	}
}
