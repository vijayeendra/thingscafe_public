//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	//  "time"
)

type data_record_new struct {
	Data  []data_rec_new `json:"data"`
	App   string         `json:"tcafe_app"`
	Cid   string         `json:"tcafe_cid"`
	Ename string         `json:"tcafe_ename"`
}

type data_rec_new struct {
	Common data_rec_common   `json:"hdr"`
	Rec    resource_record   `json:"rec, omitempty"`
	Recs   []resource_record `json:"recs, omitempty"`
}

type data_rec_common struct {
	Dtype    string `json:"dat"`
	Ts       int64  `json:"t"`
	Jrv      string `json:"jrv"`
	Severity string `json:"sev, omitempty"`
	Nid      uint16 `json:"nid"`
	Rc       uint16 `json:"rc"`
	Rid      uint16 `json:"rid, omitempty"`
}

type resource_record struct {
	Res_type string      `json:"res_type"`
	Rdata    interface{} `json:"rdata, omitempty"`
	Stats    stats       `json:"rstats, omitempty"`
}

type stats struct {
	Pname    string            `json:"pname, omitempty"`
	Name     string            `json:"name"`
	Gauges   map[string]int64  `json:"gauges"`
	Counters map[string]uint64 `json:"counters"`
}

type alarm struct {
	Pname  string      `json:"pname, omitempty"`
	Name   string      `json:"name"`
	Source string      `json:"source"`
	Value  interface{} `json:"value"`
	Status interface{} `json:"status"`
}

type pli struct {
	Pname  string `json:"pname"`
	Name   string `json:"name"`
	State  string `json:"state"`
	Cycles uint64 `json:"cyl, omitempty"`
	//Summary pli_summary `json:"summary,omitempty"`
}

type pli_summary struct {
	Cycles         uint64     `json:"cycles"`
	Pump_summary   []string   `json:"mot"`
	Switch_summary []vsummary `json:"val"`
	Fuse_summary   []fsummary `json:"flt"`
}

type vsummary struct {
	Name string      `json:"name"`
	Run  uint64      `json:"run"`
	Nuts nut_summary `json:"nut, omitempty"`
}
type nut_summary [4]int

type fsummary struct {
	Name   string `json:"name"`
	Run    uint64 `json:"run"`
	Cycles uint64 `json:"cycles"`
}

type val struct {
	Pname     string   `json:"pname"`
	Name      string   `json:"name"`
	State     string   `json:"state"`
	Grouped   []string `json:"gr"`
	Itype     string   `json:"itype"`
	Scheduled uint64   `json:"sch"`
	Run       uint64   `json:"run"`
	Total     uint64   `json:"trun"`
	Cycles    uint64   `json:"cyl"`
}

//pipelineID, valveID, venturiID, on/off, runtime/vol, completed cycles]
type nut struct {
	Pname     string `json:"pname"`
	Name      string `json:"name"`
	State     string `json:"state"`
	Ntype     string `json:"ntype"`
	Vname     string `json:"vname"`
	Scheduled uint64 `json:"sch, omitempty"`
	Run       uint64 `json:"run"`
	Cycles    uint64 `json:"cyl"`
}

//pipelineID, filterID, on/off, elapsed time in minutes from last cycle, filtration cycles in current irrigation cycle, total filtration cycles]
type flt struct {
	Pname   string `json:"pname"`
	Name    string `json:"name"`
	State   string `json:"state"`
	Elapsed int64  `json:"elptm"`
	Current uint64 `json:"cfcyl"`
	Cycles  uint64 `json:"tfcyl"`
}

type mot struct {
	Pname string `json:"pname"`
	Name  string `json:"name"`
	State string `json:"state"`
}

var msg string = `{"data": [{"dat": "er","jrv": "1.0", "pump": ["P1", "M1", 1],"fuse":["P1","F1",1,5,0,1322], "t": 1571310714}]}`
var msg1 string

func parse_data(data []byte) (*data_record_new, error) {
	dr2 := new(data_record_new)
	err := json.Unmarshal(data, dr2)
	if err == nil {
		return dr2, nil
	} else {
		fmt.Println(err)
		return nil, err
	}
}

func Unmarshal_record_common() {
}

type record_temp struct {
	Res_type string `json:"res_type"`
}

//func (u *resource_record) UnmarshalJSON(b []byte) error {
func Unmarshal_record(dtype string, b []byte) (error, *resource_record) {

	var err error

	temp := struct {
		Res_type string `json:"res_type"`
	}{}

	if err := json.Unmarshal(b, &temp); err != nil {
		log.Println("Unable to find type and resource_type:", string(b))
		return err, nil
	}
	u := new(resource_record)

	switch dtype {
	case "dr":
		{
			rec := struct {
				Res_type string `json:"res_type"`
				Rdata    stats  `json:"rdata"`
			}{}
			err = json.Unmarshal(b, &rec)
			// no error, but we also need to make sure we unmarshaled something
			if err == nil && rec.Rdata.Name != "" {
				//fmt.Println(rec)
				u.Res_type = rec.Res_type
				u.Rdata = rec.Rdata
				//fmt.Println("U :", *u)
				return nil, u
			}
		}
	case "ar":
		{
			rec := struct {
				Res_type string `json:"res_type"`
				Rdata    alarm  `json:"rdata"`
			}{}
			err = json.Unmarshal(b, &rec)
			// no error, but we also need to make sure we unmarshaled something
			if err == nil && rec.Rdata.Name != "" {
				//fmt.Println(rec)
				u.Res_type = rec.Res_type
				u.Rdata = rec.Rdata
				//fmt.Println("U :", *u)
				return nil, u
			}
		}
	case "er":
		{
			switch temp.Res_type {
			case "ckt":
				rec := struct {
					Res_type string `json:"res_type"`
					Rdata    pli    `json:"rdata"`
				}{}
				err = json.Unmarshal(b, &rec)

				// no error, but we also need to make sure we unmarshaled something
				if err == nil && rec.Rdata.Pname != "" {
					//fmt.Println(rec)
					u.Res_type = rec.Res_type
					u.Rdata = rec.Rdata
					//fmt.Println("U :", *u)
					return nil, u
				}
			case "pump":
				rec := struct {
					Res_type string `json:"res_type"`
					Rdata    mot    `json:"rdata"`
				}{}
				err = json.Unmarshal(b, &rec)

				// no error, but we also need to make sure we unmarshaled something
				if err == nil && rec.Rdata.Pname != "" {
					//fmt.Println(rec)
					u.Res_type = rec.Res_type
					u.Rdata = rec.Rdata
					//fmt.Println("U :", *u)
					return nil, u
				}
			case "sw":
				rec := struct {
					Res_type string `json:"res_type"`
					Rdata    val    `json:"rdata"`
				}{}
				err = json.Unmarshal(b, &rec)

				// no error, but we also need to make sure we unmarshaled something
				if err == nil && rec.Rdata.Pname != "" {
					//fmt.Println(rec)
					u.Res_type = rec.Res_type
					u.Rdata = rec.Rdata
					//fmt.Println("U :", *u)
					return nil, u
				}
			case "nut":
				rec := struct {
					Res_type string `json:"res_type"`
					Rdata    nut    `json:"rdata"`
				}{}
				err = json.Unmarshal(b, &rec)

				// no error, but we also need to make sure we unmarshaled something
				if err == nil && rec.Rdata.Pname != "" {
					//fmt.Println(rec)
					u.Res_type = rec.Res_type
					u.Rdata = rec.Rdata
					//log.Println("U :", *u)
					return nil, u
				}
			case "fuse":
				rec := struct {
					Res_type string `json:"res_type"`
					Rdata    flt    `json:"rdata"`
				}{}
				err = json.Unmarshal(b, &rec)

				// no error, but we also need to make sure we unmarshaled something
				if err == nil && rec.Rdata.Pname != "" {
					//fmt.Println(rec)
					u.Res_type = rec.Res_type
					u.Rdata = rec.Rdata
					return nil, u
				}
			}
		}
	}

	// abort if we have an error other than the wrong type
	if _, ok := err.(*json.UnmarshalTypeError); err != nil && !ok {
		return err, nil
	}

	if err == nil {
		err = errors.New("Failed to unmarshal")
	}
	return err, nil
}

func (u *data_rec_new) UnmarshalJSON(b []byte) error {

	//var rec_types = []string{"pli", "mot", "val", "flt", "nut"}
	var err error
	//var rec_types = []string{"pli", "mot", "val"}

	temp1 := struct {
		Common data_rec_common   `json:"hdr"`
		Rec    json.RawMessage   `json:"rec"`
		Recs   []json.RawMessage `json:"recs"`
	}{}

	if err = json.Unmarshal(b, &temp1); err != nil {
		log.Println("Unable to find record type :", string(b))
		return err
	}

	u.Common = temp1.Common

	record := []byte(temp1.Rec)
	records := temp1.Recs

	if len(record) > 0 {
		err, res_rec := Unmarshal_record(temp1.Common.Dtype, record)

		if err == nil {
			//log.Println("Unmarshalled value:", string(b), u)
			u.Rec = *res_rec
		} else {
			log.Println("Unable to unmarshall record :", string(b))
		}
	} else if len(records) > 0 {
		u.Recs = []resource_record{}

		for _, rec := range records {
			err, res_rec := Unmarshal_record(temp1.Common.Dtype, []byte(rec))

			if err == nil {
				u.Recs = append(u.Recs, *res_rec)
				//log.Println("Unmarshalled value:", string(b), u)
			} else {
				log.Println("Unable to unmarshall record :", string(rec))
			}
		}
	}

	return err
}

func (u *resource_record) String() string {
	switch d := u.Rdata.(type) {
	case *pli:
		return fmt.Sprint("ckt: ", d)
	case *mot:
		return fmt.Sprint("pump: ", d)
	case *val:
		return fmt.Sprint("sw: ", d)
	case *flt:
		return fmt.Sprint("fuse: ", d)
	case *nut:
		return fmt.Sprint("mcb: ", d)
	}
	return ""
}
