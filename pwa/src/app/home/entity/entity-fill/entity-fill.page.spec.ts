import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntityFillPage } from './entity-fill.page';

describe('EntityFillPage', () => {
  let component: EntityFillPage;
  let fixture: ComponentFixture<EntityFillPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntityFillPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityFillPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
