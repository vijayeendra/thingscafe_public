seq=0
seq1=1000
seqi2=10000
cycles=0
total=100
run=0
sch=1000
while :
do 
time=`date "+%s"`
echo $time
seq=`expr $seq + 1`
seq1=`expr $seq1 + 1`
seq2=`expr $seq2 + 1`
cycles=`expr $cycles + 1`
total=`expr $cycles + 1`
run=`expr $run + 1`
curl --insecure -X POST \
  'https://localhost:8080/tccollect/v1/submit-data?app=saasv&ename=AJoffice&cid=a24515a4-bc8c-11e9-985c-0242ac110004' \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 5490' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8080' \
  -H 'cache-control: no-cache' \
  -d '{
    "data": [
		{
			"hdr": {
				"dat": "er",
				"t": '"$time"',
				"jrv": "1.0",
				"sev": "info",
				"nid": 102,
				"rc": '"$seq2"',
				"rid": 0
			},
			"rec": {
				"res_type": "pli",
				"rdata": {
					"pname": "P1",
					"name": "P1",
					"state": "end",
					"cyl": 100
				}
			}
		},
        {
            "hdr": {
                "dat": "er",
                "t": '"$time"',
                "jrv": "1.0",
                "sev": "info",
                "nid": 102,
                "rc": '"$seq"',
                "rid": 0
            },
            "rec": {
                "res_type": "val",
                "rdata": {
                    "pname": "p1",
                    "name": "v11",
                    "state": "on",
					"gr":["v11","v12"],
                    "itype": "tm",
                    "sch": '"$sch"',
                    "run": '"$run"',
                    "trun": '"$total"',
                    "cyl": '"$cycles"'
                }
            }
        },
		{
			"hdr": {
				"dat": "er",
				"t": 1574739147,
				"jrv": "1.0",
				"sev": "info",
				"nid": 106,
				"rc": 4041,
				"rid": 0
			},
			"rec": {
				"res_type": "val",
				"rdata": {
					"pname": "p1",
					"name": "v10",
					"state": "on",
					"itype": "tm",
					"sch": 30,
					"run": '"$run"',
					"trun": 5319,
					"cyl": 235
				}
			}
		}
    ]
}'
sleep 8
done
