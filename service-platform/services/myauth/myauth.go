//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package myauth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.com/service-platform/common"
	objectdb "gitlab.com/service-platform/db/objectmanagement"
	entitysvc "gitlab.com/service-platform/services/entitymanagement"
	usersvc "gitlab.com/service-platform/services/usermanagement"
	"golang.org/x/crypto/bcrypt"
)

const ACCESSTOKEN_EXPIRY_TIME = 86400 * 2
const REFRESHTOKEN_EXPIRY_TIME = 86400 * 3

/* Set up a global string for our secret */
var mykey = []byte("secret-things")

type login_info struct {
	Username         string `json:"username"`
	Password         string `json:"password"`
	Saas_entity_name string `json:"saas_entity_name"`
}

type login_resp struct {
	Id               gocql.UUID    `json:"id"`
	Access_token     string        `json:"access_token"`
	Refresh_token    string        `json:"refresh_token"`
	Expires_in       time.Duration `json:"expires_in"`
	Saas_entity_name string        `json:"saas_entity_name"`
}

func keyLookupFunc(token *jwt.Token) (interface{}, error) {
	// Don't forget to validate the alg is what you expect:
	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	}

	return mykey, nil

	/*
		// Look up key
		key, err := jwt.lookupPublicKey(token.Header["kid"])
		if err != nil {
			return nil, err
		}

		// Unpack key from PEM encoded PKCS8
		return jwt.ParseRSAPublicKeyFromPEM(key)
	*/
}

const (
	BASIC_SCHEMA  string = "Basic "
	BEARER_SCHEMA string = "Bearer "
)

func Authorizerequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var err error
		var token *jwt.Token
		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			http.Error(w, "Authorization header required", 500)
			return
		}

		// Confirm the request is sending Basic Authentication credentials.
		if !strings.HasPrefix(authHeader, BASIC_SCHEMA) && !strings.HasPrefix(authHeader, BEARER_SCHEMA) {
			http.Error(w, "Authorization requires Basic/Bearer schema", 500)
			return
		}

		// Get the token from the request header
		// The first six characters are skipped - e.g. "Basic ".
		if strings.HasPrefix(authHeader, BASIC_SCHEMA) {
			return
		}
		//splitToken := strings.Split(reqToken, "Bearer")
		//reqToken = splitToken[1]
		tokenstring := authHeader[len(BEARER_SCHEMA):]
		if token, err = jwt.Parse(tokenstring, keyLookupFunc); err == nil && token.Valid {

			err = token.Claims.Valid()
			if err == nil {
				claims := token.Claims.(jwt.MapClaims)

				// Get user record
				user_id := claims[string(common.ContextUserKey)].(string)
				saas := claims[string(common.SaasEntityKey)].(string)
				err, user_info, einfo := get_user("id", user_id, saas)
				if err != nil {
					http.Error(w, "Could not Authorize user", 500)
					return
				}

				var v common.ContextValues
				v.M = make(map[string]interface{})

				v.M[string(common.SaasEntityKey)] = saas
				v.M[string(common.ContextUserKey)] = user_info
				v.M[string(common.ContextEntityKey)] = einfo
				v.M[string(common.ContextRoleKey)], _ = claims[string(common.ContextRoleKey)].(common.User_role)
				v.M[string(common.ContextEntityTypeKey)] = claims[string(common.ContextEntityTypeKey)]
				ctx := context.WithValue(r.Context(), "MyContextvalues", v)
				r = r.WithContext(ctx)
				next.ServeHTTP(w, r)
				return
			}
		}

		// Request Error
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
		} else {
			http.Error(w, "Authorization failed", http.StatusUnauthorized)
		}

		return
	})
}

// BasicAuth is the basic auth handler
/*
func PerformBasicAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Get the Basic Authentication credentials
		// var user_info userm.user_info
		// var present boolean
		//user, password, hasAuth := basicAuth(ctx)
		user1, password, hasAuth := r.BasicAuth()
		var v common.ContextValues
		v.M = make(map[string]interface{})

		u := usersvc.Userm(userdb.Cass{})
		user_info, present := u.Get_user(user1)
		if hasAuth && present {
			//user == requiredUser && password == requiredPassword {
			// Delegate request to the given handle
			user_cred, present := u.Get_user_credentials(user_info.Id)
			if present && comparePasswords(user_cred.Password, []byte(password)) {
				log.Println("User:", user_info.First_name, user_info.Last_name)
				v.M[string(common.ContextEntityKey)] = gocql.UUID(user_info.Entity_id)
				v.M[string(common.ContextRoleKey)] = user_info.Role
				v.M[string(common.ContextUserKey)] = gocql.UUID(user_info.Id)
				ctx := context.WithValue(r.Context(), "MyContextvalues", v)
				r = r.WithContext(ctx)
				next.ServeHTTP(w, r)
				return
			}
		}
		// Request Basic Authentication otherwise
		w.Header().Set("WWW-Authenticate", `Basic realm="MY REALM"`)
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Unauthorized"))
	})
}
*/

func get_user(otype string, id string, saas string) (error, *usersvc.User_info, *entitysvc.Entity_info) {

	//var objmap map[string]*json.RawMessage
	var obj common.Object_handler
	var qoptions common.Query_options

	user := new(usersvc.User_info)
	entity := new(entitysvc.Entity_info)

	default_user_query_options := common.Query_options{
		Limit: 10, Allow_filtering: true}
	qoptions = default_user_query_options
	qoptions.Where = make(map[string]interface{}, 5)
	qoptions.Where["saas_entity_name"] = saas
	qoptions.Where["keyspace"] = saas
	if otype == "id" {
		obj.Init("user", &qoptions)
		qoptions.Where["id"] = id
	} else {
		obj.Init("users", &qoptions)
		qoptions.Where["email"] = id
	}
	ctx := context.Background()

	e := objectdb.DBObject(objectdb.Cass{})
	payload, err := e.Get_object(ctx, []byte{}, &obj)
	if err != nil {
		return err, nil, nil
	}
	err = json.Unmarshal(payload, &user)
	if err != nil {
		return err, nil, nil
	}

	qoptions = default_user_query_options
	qoptions.Where = make(map[string]interface{}, 5)
	qoptions.Where["id"] = user.Entity_id
	qoptions.Where["keyspace"] = saas
	obj.Init("entity", &qoptions)

	payload, err = e.Get_object(ctx, []byte{}, &obj)
	if err != nil {
		return err, user, nil
	}
	err = json.Unmarshal(payload, &entity)
	if err != nil {
		return err, user, nil
	}
	//user.Kv["entity_key"] = string(entity.Entity_id) + ":" + entity.Name

	return nil, user, entity
}

type User_cred struct {
	Id       gocql.UUID `json:"id"`
	Password string     `json:"password"`
}

func get_user_cred(id gocql.UUID, saas string) (error, *User_cred) {

	//var objmap map[string]*json.RawMessage
	var object User_cred
	var obj common.Object_handler
	var qoptions common.Query_options

	default_user_query_options := common.Query_options{
		Limit: 10, Allow_filtering: true}
	qoptions = default_user_query_options
	qoptions.Where = make(map[string]interface{}, 5)
	qoptions.Where["id"] = id
	qoptions.Where["saas_entity_name"] = saas
	qoptions.Where["keyspace"] = saas
	obj.Init("user_credentials", &qoptions)
	ctx := context.Background()

	e := objectdb.DBObject(objectdb.Cass{})
	payload, err := e.Get_object(ctx, []byte{}, &obj)
	if err != nil {
		return err, nil
	}
	err = json.Unmarshal(payload, &object)
	if err != nil {
		return err, nil
	}
	return nil, &object
}

func check_credentials(rec *login_info) (*usersvc.User_info, error) {
	err, user_info, einfo := get_user("name", rec.Username, rec.Saas_entity_name)
	if err == nil {
		err, user_cred := get_user_cred(user_info.Id, rec.Saas_entity_name)
		if err == nil {
			if comparePasswords(string(user_cred.Password), []byte(rec.Password)) {
				log.Println("Logged in User:", einfo.Name, user_info.First_name, user_info.Last_name)
				return user_info, nil
			}
			err := errors.New("Incorrect password/user")
			return user_info, err
		}
		return user_info, err
	} else {
		return nil, err
	}
}

type refresh_input struct {
	Refresh_token string `json:"refresh_token"`
	Grant_type    string `json:"grant_type"`
	User          string `json:"user"`
}

var RefreshToken = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var input refresh_input
	var token *jwt.Token
	var user_info2 *usersvc.User_info

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}
	log.Println(string(body))
	err = json.Unmarshal(body, &input)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}
	log.Println(input.Refresh_token, input.Grant_type)

	/*
		if input.Grant_type != "refresh_token" {
			err := errors.New("Invalid Grant type")
			http.Error(w, err.Error(), 500)
			return
		}
	*/

	if token, err = jwt.Parse(input.Refresh_token, keyLookupFunc); err == nil && token.Valid {

		err = token.Claims.Valid()
		if err == nil {
			claims := token.Claims.(jwt.MapClaims)
			var user_info usersvc.User_info
			var expiry_period int = ACCESSTOKEN_EXPIRY_TIME
			if strings.Contains(r.URL.Path, "gettoken") {
				if input.User != "" {
					saas := claims[string(common.SaasEntityKey)].(string)
					err, user_info2, _ = get_user("name", input.User, saas)
					if err != nil {
						log.Println(err)
						err := errors.New("User incorrect")
						http.Error(w, err.Error(), 500)
						return
					}
				} else {
					err := errors.New("User information missing")
					http.Error(w, err.Error(), 500)
					return
				}
				user_info.Saas_entity_name = user_info2.Saas_entity_name
				user_info.Entity_id = user_info2.Entity_id
				user_info.Id = user_info2.Id
				expiry_period = ACCESSTOKEN_EXPIRY_TIME
			} else {
				user_info.Saas_entity_name = claims[string(common.SaasEntityKey)].(string)
				user_info.Entity_id, _ = gocql.ParseUUID(claims[string(common.ContextEntityKey)].(string))
				user_info.Id, _ = gocql.ParseUUID((claims[string(common.ContextUserKey)]).(string))
			}
			user_info.Role = common.User_role((claims[string(common.ContextRoleKey)]).(float64))
			atoken := GetTokenHandler(&user_info, mykey, ACCESSTOKEN_EXPIRY_TIME)
			rtoken := GetTokenHandler(&user_info, mykey, REFRESHTOKEN_EXPIRY_TIME)
			resp := new(login_resp)
			resp.Access_token = string(atoken)
			resp.Refresh_token = string(rtoken)
			resp.Expires_in = time.Duration(expiry_period)
			resp.Id = user_info.Id
			resp.Saas_entity_name = user_info.Saas_entity_name
			payload, _ := json.Marshal(&resp)
			w.WriteHeader(http.StatusOK)
			w.Header().Set("Content-Type", "application/jwt")
			w.Write([]byte(payload))
			return
		}
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
	} else {
		http.Error(w, "Invalid token", http.StatusUnauthorized)
	}
	return
})

var LoginHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	var login login_info

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}
	//log.Println(string(body))
	err = json.Unmarshal(body, &login)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}

	user_info, err := check_credentials(&login)
	if err != nil {
		// Request Basic Authentication otherwise
		//	w.Header().Set("WWW-Authenticate", `Basic realm="MY REALM"`)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	atoken := GetTokenHandler(user_info, mykey, ACCESSTOKEN_EXPIRY_TIME)
	rtoken := GetTokenHandler(user_info, mykey, REFRESHTOKEN_EXPIRY_TIME)
	resp := new(login_resp)

	resp.Access_token = string(atoken)
	resp.Refresh_token = string(rtoken)
	resp.Expires_in = ACCESSTOKEN_EXPIRY_TIME
	resp.Id = user_info.Id
	payload, _ := json.Marshal(&resp)
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/jwt")
	w.Write([]byte(payload))
	return
})

// AuthToken returns a token for further usage
var AuthToken = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	// Get the Basic Authentication credentials
	// var user_info userm.user_info
	// var present boolean
	//user, password, hasAuth := basicAuth(ctx)
	//token := GetTokenHandler(r, mykey)
	w.Header().Set("Content-Type", "application/jwt")
	//w.Write([]byte(token))
	return
})

func GetTokenHandler(user_info *usersvc.User_info, mySigningKey []byte, expiry time.Duration) (tok []byte) {
	/* Create the token */
	token := jwt.New(jwt.SigningMethodHS256)

	/* Create a map to store our claims */
	claims := token.Claims.(jwt.MapClaims)

	/* Set token claims */
	claims[string(common.SaasEntityKey)] = string(user_info.Saas_entity_name)
	claims[string(common.ContextEntityKey)] = gocql.UUID(user_info.Entity_id)

	claims[string(common.ContextRoleKey)] = common.User_role(user_info.Role)
	claims[string(common.ContextUserKey)] = gocql.UUID(user_info.Id)
	if user_info.Kv != nil {
		claims[string(common.ContextEntityTypeKey)] = user_info.Kv[string(common.ContextEntityTypeKey)].(string)
	} else {
		claims[string(common.ContextEntityTypeKey)] = "-1"
	}
	claims["Email"] = user_info.Email
	claims["exp"] = time.Now().Add(expiry * time.Second).Unix()

	/* Sign the token with our secret */
	tokenString, _ := token.SignedString(mySigningKey)

	/* Finally, write the token to the browser window */
	return ([]byte(tokenString))
}

func comparePasswords(hashedPwd string, plainPwd []byte) bool {
	// Since we'll be getting the hashed password from the DB it
	// will be a string so we'll need to convert it to a byte slice
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}

func SetAuthenticationRoutes(router *mux.Router) *mux.Router {
	router.HandleFunc("/tc/login", LoginHandler).Methods("POST")
	router.HandleFunc("/tc/refreshtoken", RefreshToken).Methods("POST")
	router.HandleFunc("/tc/gettoken", RefreshToken).Methods("POST")
	//router.HandleFunc("/auth", AuthHandler).Methods("GET")
	/*
		router.HandleFunc("/token-auth", controllers.Login).Methods("POST")
		router.Handle("/refresh-token-auth", negroni.New(negroni.HandlerFunc(controllers.RefreshToken))).Methods("GET")
		router.Handle("/logout", negroni.New(negroni.HandlerFunc(authentication.RequireTokenAuthentication), negroni.HandlerFunc(controllers.Logout))).Methods("GET")
	*/
	return router
}

func init() {
}
