//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, MenuController} from '@ionic/angular';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { UserService } from '../services/user.service';
import { Observable } from  'rxjs/Observable';
import { GlobalService } from '../services/global.service';
import { SwUpdate } from '@angular/service-worker';
import { take } from  'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {

	username : string = "";
	authState$: Observable<boolean>;
	ghdata:any;

	constructor(private router: Router,
	           private navCtrl:NavController,
	           private alertCtrl:AlertController,
	           private updates:SwUpdate,
			   private global: GlobalService,
	           private usersvc:UserService) {

    }


	onRefresh(refresher): void {
		this.ionViewWillEnter();
		refresher.target.complete();
	}

	ionViewWillEnter(){
	}


    ngOnInit() {

		if (!this.updates.isEnabled) {
		  console.log("Service worker is not enabled");
		  return;
		}

		this.updates.activated
	      .pipe(take(1),)
			.subscribe(event => {
			  console.log("Received Update activated event");
			  this.alertCtrl.create({
				  header: "Updated to latest version of app",
				  buttons: [
					  {
						text: 'Dismiss',
						role: 'cancel',
						cssClass: 'secondary',
						handler: () => {
						  console.log('Confirm Cancel');
						}
					  }
				  ]
			  }).then(alert => {
				  alert.present();
				  //this.updates.activateUpdate().then(() => document.location.reload());
			  });
			});
	}

	public labelClick() {
		console.log("Open Explorer", );
		this.navCtrl.navigateForward(["/home/entity", {id: this.global.user_entity, type: this.global.user_entity_type}]);
	}
	
	public dummy() {
	}
}
