//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { EntityPageModule } from './entity/entity.module';
import { EntityPage } from './entity/entity.page';
import { EntityFillPageModule } from './entity/entity-fill/entity-fill.module';
import { EntityFillPage } from './entity/entity-fill/entity-fill.page';
import { AssetPageModule } from './asset/asset.module';
import { AssetPage } from './asset/asset.page';
import { AssetFillPageModule } from './asset/asset-fill/asset-fill.module';
import { AssetFillPage } from './asset/asset-fill/asset-fill.page';
import { ShowdataComponentModule } from '../vendor-components/showdata/showdata.component';
import { ShowdataComponent } from '../vendor-components/showdata/showdata.component';
import { ShowgridComponentModule } from './showgrid/showgrid.component';
import { ShowgridComponent } from './showgrid/showgrid.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
	IonicModule,
	ShowdataComponentModule, 
	ShowgridComponentModule, 
	EntityPageModule,
	EntityFillPageModule,
	AssetPageModule,
	AssetFillPageModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
			},
      {
        path: 'first',
        component: HomePage
	  },
			{ 
        path: 'entity',
        component: EntityPage
			},
			{ 
		path: 'entity/fill',
        component: EntityFillPage
			},
			{ 
		path: 'asset',
        component: AssetPage
			},
			{ 
		path: 'asset/fill',
        component: AssetFillPage
			}
		/*
			{ 
		path: 'entity:/id',
        component: EntityPage
			}
		 */
    ])
  ],
	//declarations: [HomePage, MenuItemComponent, EntityPage]
	
  declarations: [HomePage, MenuItemComponent, EntityPage, EntityFillPage, AssetPage, AssetFillPage ]
})

export class HomePageModule {}
