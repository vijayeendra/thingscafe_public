//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

// +build production

package aggregates

import "time"

const ticker_5m_duration = 5 * time.Minute
const ticker_1h_duration = 1 * time.Hour

const ticker_1m_duration = 1 * time.Minute
const ticker_15m_duration = 15 * time.Minute
const ticker_2h_duration = 2 * time.Hour

const ticker_1day_duration = 24 * time.Hour
const ticker_1week_duration = 7 * 24 * time.Hour
const ticker_1month_duration = 30 * 7 * 24 * time.Hour
