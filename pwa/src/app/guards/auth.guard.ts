//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { UserService } from '../services/user.service';
import { take } from  'rxjs/operators';
import { BehaviorSubject, Observable, throwError, from  } from  'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

    readystatus$: BehaviorSubject<boolean> = new BehaviorSubject(null);
    authState$: BehaviorSubject<boolean> = new BehaviorSubject(null);
	//authState$: Observable<boolean>;

	public ready():Observable<boolean> {
		return this.readystatus$.asObservable();
	}

	constructor(
		private global: GlobalService,
		private usersvc: UserService,
		) {

		this.authState$.next(false);
		this.readystatus$.next(false);
	    this.global.ready().subscribe(resp => {
			this.usersvc.ready().subscribe(resp => {
				this.readystatus$.next(true);
			});
		});
	}

    canActivate( next: ActivatedRouteSnapshot,
			   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

	   this.ready().subscribe(resp => {
		    let authstate$ = this.usersvc.checkloginstatusObserver();
			authstate$.pipe(take(2),)
			  .subscribe(x => {
					this.authState$.next(x);
			  });
	   });

	   return this.authState$.asObservable();
   }
}
