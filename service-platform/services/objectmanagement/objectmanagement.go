//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package objectmanagement

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strings"

	"github.com/gocql/gocql"
	"gitlab.com/service-platform/common"
	objectdb "gitlab.com/service-platform/db/objectmanagement"
)

type emptyI interface {
	SetId(id gocql.UUID)
	GetId() (id gocql.UUID)
	SetEntityId(id gocql.UUID)
	GetEntityId() (id gocql.UUID)
}

type Objectservice interface {
	Create_object_svc(w http.ResponseWriter, r *http.Request, obj common.Object, object emptyI) error
	List_objects_svc(w http.ResponseWriter, r *http.Request, obj common.Object) ([]common.Object, error)
	Delete_object_svc(w http.ResponseWriter, r *http.Request, obj common.Object) error
	Get_object_svc(w http.ResponseWriter, r *http.Request, obj common.Object) (common.Object, error)
	Modify_object_svc(w http.ResponseWriter, r *http.Request, obj common.Object) error
}

type Generic_svc struct {
}

func (svc *Generic_svc) Create_object_svc(w http.ResponseWriter, r *http.Request, obj common.Object, object emptyI) error {

	var modify_flag bool = false
	if err := obj.Write_permissions(w, r); err != nil {
		return err
	}

	//body, err := ioutil.ReadAll(r.Body)
	err, objmap, _ := obj.Unmarshal(r, object)
	_ = objmap

	if err != nil {
		http.Error(w, err.Error(), 500)
		return err
	}

	ctx := r.Context()
	if strings.Contains(r.URL.Path, "modify") {
		id, result := obj.Get_modify_id()
		modify_flag = true
		if result == true {
			if object.GetId() != id {
				err := errors.New("Not Authorized")
				http.Error(w, "Not Authorized", http.StatusUnauthorized)
				return err
			}
		}
	} else {
		if object.GetId().Node() == nil {
			id := gocql.TimeUUID()
			object.SetId(id)
		}
	}

	create_json, _ := json.Marshal(object)
	//create_json := body
	e := objectdb.DBObject(objectdb.Cass{})
	if modify_flag {
		err = e.Modify_object(ctx, create_json, obj)
	} else {
		err = e.Create_object(ctx, create_json, obj)
	}

	if err != nil {
		http.Error(w, err.Error(), 500)
		//log.Println("Create json:", string(create_json))
	} else {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(create_json))
	}
	return err
}

func (svc *Generic_svc) List_paged_objects_svc(w http.ResponseWriter, r *http.Request, obj common.Object, pgstate interface{}) (error, payload interface{}, pstate interface{}) {

	//	var list_json []byte
	if err := obj.Read_permissions(w, r); err != nil {
		return err, nil, nil
	}
	/*
		if r.Method == "POST" {
			_, _, orig := obj.Unmarshal(r, obj)
			list_json = orig
		}
	*/
	ctx := r.Context()
	e := objectdb.DBObject(objectdb.Cass{})
	payload, err, pstate := e.List_paged_objects(ctx, nil, obj, pgstate)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return err, nil, nil
	}
	w.Header().Set("Content-Type", "application/json")
	//w.Write(payload)
	return err, payload, pstate
}

func (svc *Generic_svc) List_objects_svc(w http.ResponseWriter, r *http.Request, obj common.Object) error {

	var list_json []byte
	if err := obj.Read_permissions(w, r); err != nil {
		return err
	}
	if r.Method == "POST" {
		err, objmap, orig := obj.Unmarshal(r, obj)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return err
		}
		//list_json, _ = json.Marshal(obj)
		list_json = orig
		_ = objmap
	}
	ctx := r.Context()
	e := objectdb.DBObject(objectdb.Cass{})
	payload, err := e.List_objects(ctx, list_json, obj)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return err
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(payload)
	return err
}

func (svc *Generic_svc) Delete_object_svc(w http.ResponseWriter, r *http.Request, obj common.Object) error {

	if err := obj.Write_permissions(w, r); err != nil {
		return err
	}

	err, objmap, orig := obj.Unmarshal(r, obj)
	_ = objmap
	if err != nil {
		http.Error(w, err.Error(), 500)
		return err
	}

	//delete_json, _ := json.Marshal(obj)
	delete_json := orig
	ctx := r.Context()
	e := objectdb.DBObject(objectdb.Cass{})
	err1 := e.Delete_object(ctx, delete_json, obj)
	if err1 != nil {
		http.Error(w, err1.Error(), 500)
	}
	return err1
}

func (svc *Generic_svc) Copy_object_svc(w http.ResponseWriter, r *http.Request, obj common.Object, object emptyI) error {

	if err := obj.Read_permissions(w, r); err != nil {
		return err
	}

	err, objmap, orig := obj.Unmarshal(r, obj)
	_ = objmap
	if err != nil {
		http.Error(w, err.Error(), 500)
		return err
	}

	//get_json, _ := json.Marshal(obj)
	copy_json := orig
	ctx := r.Context()
	e := objectdb.DBObject(objectdb.Cass{})
	payload, err := e.Get_object(ctx, copy_json, obj)

	if err != nil {
		http.Error(w, err.Error(), 500)
		return err
	}
	err = json.Unmarshal(payload, &object)

	if err == nil {
		id, present := obj.Get_entity()
		if !present {
			err := errors.New("Destination Entity Id missing")
			http.Error(w, err.Error(), 500)
			return err
		}
		object.SetEntityId(id)
		create_json, _ := json.Marshal(object)
		err = e.Create_object(ctx, create_json, obj)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return err
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(create_json))
		return nil
	} else {
		http.Error(w, err.Error(), 500)
		return err
	}
}

func (svc *Generic_svc) Get_object_svc(w http.ResponseWriter, r *http.Request, obj common.Object) error {

	if err := obj.Read_permissions(w, r); err != nil {
		return err
	}

	err, objmap, orig := obj.Unmarshal(r, obj)
	_ = objmap
	if err != nil {
		http.Error(w, err.Error(), 500)
		return err
	}

	/*
		if !common.Check_against_user_entity(r, obj.Id) {
			err := errors.New("Access denied ")
			http.Error(w, "Access denied", http.StatusUnauthorized)
			return err
		}
	*/

	//get_json, _ := json.Marshal(obj)
	get_json := orig
	ctx := r.Context()
	e := objectdb.DBObject(objectdb.Cass{})
	payload, err := e.Get_object(ctx, get_json, obj)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return err
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(payload))
	return nil
}

func (svc *Generic_svc) Update_object_svc(w http.ResponseWriter, r *http.Request, obj common.Object, object emptyI) error {

	if err := obj.Write_permissions(w, r); err != nil {
		return err
	}

	//body, err := ioutil.ReadAll(r.Body)
	err, objmap, orig := obj.Unmarshal(r, obj)
	_ = objmap

	if err != nil {
		http.Error(w, err.Error(), 500)
		return err
	}

	ctx := r.Context()
	update_json := orig
	e := objectdb.DBObject(objectdb.Cass{})
	err = e.Update_object(ctx, update_json, obj)

	if err != nil {
		http.Error(w, err.Error(), 500)
		log.Println("Update json:", string(update_json))
	} else {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
	}
	return err
}

func (svc *Generic_svc) Modify_object_svc(w http.ResponseWriter, r *http.Request, obj common.Object, object emptyI) error {

	if err := obj.Write_permissions(w, r); err != nil {
		return err
	}

	//body, err := ioutil.ReadAll(r.Body)
	err, objmap, _ := obj.Unmarshal(r, object)
	_ = objmap

	if err != nil {
		http.Error(w, err.Error(), 500)
		return err
	}

	ctx := r.Context()
	id, result := obj.Get_modify_id()
	if result == true {
		if object.GetId() != id {
			err := errors.New("Not Authorized")
			http.Error(w, "Not Authorized", http.StatusUnauthorized)
			return err
		}
	}

	modify_json, _ := json.Marshal(object)
	//create_json := body
	e := objectdb.DBObject(objectdb.Cass{})
	//err = e.Create_object(ctx, modify_json, obj)
	err = e.Modify_object(ctx, modify_json, obj)

	if err != nil {
		http.Error(w, err.Error(), 500)
		log.Println("Modify json:", string(modify_json))
	} else {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(modify_json))
	}
	return err
}
