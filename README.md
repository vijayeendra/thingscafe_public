# ThingsCafe_public

IOT service platform and miscellaneous 

Check README.md in service_platform 
directory for setting up development environment 
and how to build

Check README.md in devops directory 
on how to deploy Thingcafe in the cloud,
one time setup and install/update procedures. While 
there are scripts for everything, the entire procedure
is still manual.

