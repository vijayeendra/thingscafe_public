//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package aggregates

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/sasha-s/go-deadlock"
	cooked "gitlab.com/service-platform/common/cooked"
	mykafka "gitlab.com/service-platform/common/mykafka"
	sarama "gopkg.in/Shopify/sarama.v2"
)

/*
var ticker_5m *time.Ticker
var ticker_1h *time.Ticker
var ticker_1day *time.Ticker
var ticker_1week *time.Ticker
var ticker_1month *time.Ticker
*/
var done chan bool
var metadata_map map[string]*Metadata
var topic_config map[string]*mykafka.Topic_config

/*
Test constants
const ticker_1m_duration = 15 * time.Second
const ticker_15m_duration = 45 * time.Second
const ticker_5m_duration = 30 * time.Second
const ticker_1h_duration = 120 * time.Second
const ticker_1day_duration = 8 * time.Minute
const ticker_1week_duration = 20 * time.Minute
const ticker_1month_duration = 40 * time.Minute
*/

/*
Production constants
const ticker_5m_duration = 5 * time.Minute
const ticker_1h_duration = 1 * time.Hour

const ticker_1m_duration = 1 * time.Minute
const ticker_15m_duration = 15 * time.Minute
const ticker_2h_duration = 2 * time.Hour

const ticker_1day_duration = 24 * time.Hour
const ticker_1week_duration = 7 * 24 * time.Hour
const ticker_1month_duration = 30 * 7 * 24 * time.Hour
*/

type local_aggregate struct {
	name            string
	period          int64
	resource_aggr   map[string]map[string]*aggregate
	start_of_period int64
	next            *local_aggregate
	prev            *local_aggregate
	mux             deadlock.Mutex
	//mux             sync.Mutex
	ticker *time.Ticker
}

const AGG_TABLE_PREFIX = "cnts"

var num_aggregates = NUM_AGGREGATES
var selected_scheme = ""

const NUM_AGGREGATES = 5
const NUM_AGGREGATES_SCHEME2 = 6

var local_aggregates []local_aggregate

var local_aggregate_suffixes = [NUM_AGGREGATES]string{"1month", "1week", "1day", "1h", "5m"}
var local_aggregate_durations = [NUM_AGGREGATES]time.Duration{ticker_1month_duration, ticker_1week_duration, ticker_1day_duration, ticker_1h_duration, ticker_5m_duration}

var local_aggregate_scheme2_suffixes = [NUM_AGGREGATES_SCHEME2]string{"1month", "1week", "1day", "1h", "15m", "1m"}
var local_aggregate_scheme2_durations = [NUM_AGGREGATES_SCHEME2]time.Duration{ticker_1month_duration, ticker_1week_duration, ticker_1day_duration, ticker_1h_duration, ticker_15m_duration, ticker_1m_duration}

var headers []sarama.RecordHeader
var my_node_id string

/*
var aggregates_5m local_aggregate
var aggregates_1h local_aggregate
var aggregates_1day local_aggregate
var aggregates_1week local_aggregate
var aggregates_1month local_aggregate
*/

var stats_publish_channel interface{}

func find_local_aggregate_by_name(name string) *local_aggregate {
	for i := 0; i < num_aggregates; i++ {
		la := &local_aggregates[i]
		if la.name == name {
			return la
		}
	}
	return nil
}

func Setup(node_id string, stats_kp interface{}) error {
	var err error = nil
	stats_publish_channel = stats_kp
	my_node_id = node_id
	topic := "aggregates_checkpoint_compact"
	tc := mykafka.Get_topic_config(topic)
	err = mykafka.Create_topic(stats_kp, topic, tc)
	if err != nil {
		log.Panicf("Error creating topic :%s :%v", topic, err)
	}
	return err
}

func Setup_app_aggregates(app string) error {
	var err error = nil
	topics := Aggregate_topics(app)
	for _, topic := range topics {
		tc := mykafka.Get_topic_config(topic)
		err = mykafka.Create_topic(stats_publish_channel, topic, tc)
		if err != nil {
			log.Panicf("Error creating topic :%s :%v", topic, err)
		}
	}
	return err
}

func Shutdown() {
	done <- true
}

// pdata usually refers to thresholds to check for creating alarm records
func Publish_aggregates(msg []byte, pdata interface{}) {

	raw_rec := new(cooked.Stats_record)
	_ = json.Unmarshal(msg, raw_rec)
	calc_aggr(raw_rec, pdata)
}

func (laggr *local_aggregate) reset_time() {
	laggr.start_of_period = (time.Now().Unix() * 1000)
}

func (laggr *local_aggregate) init(name string, period time.Duration, tfunc func(*local_aggregate), next *local_aggregate) *time.Ticker {
	log.Printf("Initializing local aggregate : %s, period:%d\n", name, period.Seconds())
	laggr.name = name
	laggr.period = int64(period.Seconds())
	laggr.reset_time()
	laggr.next = next
	if next != nil {
		next.prev = laggr
	}
	laggr.ticker = time.NewTicker(period)
	// Check if resource aggregates can be loaded
	laggr.resource_aggr = nil

	start_timer(laggr.ticker, tfunc, laggr)
	return laggr.ticker
}

func init_local_aggregates(scheme string) {
	var next *local_aggregate = nil

	if scheme == "scheme2" {
		num_aggregates = NUM_AGGREGATES_SCHEME2
	}

	local_aggregates = make([]local_aggregate, num_aggregates)

	for i := 0; i < num_aggregates; i++ {
		var name string
		var duration time.Duration

		if scheme == "scheme2" {
			name = AGG_TABLE_PREFIX + local_aggregate_scheme2_suffixes[i]
			duration = local_aggregate_scheme2_durations[i]
		} else {
			name = AGG_TABLE_PREFIX + local_aggregate_suffixes[i]
			duration = local_aggregate_durations[i]
		}

		la := &local_aggregates[i]
		_ = la.init(name, duration, push_aggr, next)
		next = la
	}
}

func Published_topics() []string {
	result := make([]string, 0)
	for topic, ok := range published_topics {
		if ok {
			result = append(result, topic)
		}
	}
	return result
}

func Aggregate_topics(app string) []string {
	result := make([]string, 0)
	for i := 0; i < num_aggregates; i++ {
		la := &local_aggregates[i]
		atopic := app + "_stats_" + la.name
		result = append(result, atopic)
	}
	return result
}

func Topics_to_consume(app string) []string {
	result := make([]string, 0)
	topic := "aggregates_checkpoint_compact"
	result = append(result, topic)
	return result
}

var published_topics map[string]bool

func init() {
	create_headers()
	init_local_aggregates(selected_scheme)
	published_topics = make(map[string]bool)

	/*
		ticker_1month = aggregates_1month.init("cnts1month", ticker_1month_duration, push_aggr, nil)
		ticker_1week = aggregates_1week.init("cnts1week", ticker_1week_duration, push_aggr, &aggregates_1month)
		ticker_1day = aggregates_1day.init("cnts1day", ticker_1day_duration, push_aggr, &aggregates_1week)
		//ticker_1h = aggregates_1h.init("cnts1h", 1*time.Hour, push_aggr, &aggregates_1day)
		ticker_1h = aggregates_1h.init("cnts1h", ticker_1h_duration, push_aggr, &aggregates_1day)
		ticker_5m = aggregates_5m.init("cnts5m", ticker_5m_duration, push_aggr, &aggregates_1h)
	*/

	done = make(chan bool)
	metadata_map = make(map[string]*Metadata)
	topic_config = make(map[string]*mykafka.Topic_config)
	deadlock.Opts.DeadlockTimeout = time.Millisecond * 5000 // Enable deadlock detection
	deadlock.Opts.DisableLockOrderDetection = false
	deadlock.Opts.Disable = false
	deadlock.Opts.PrintAllCurrentGoroutines = true

}

func start_timer(ticker *time.Ticker, tfunc func(*local_aggregate), laggr *local_aggregate) {
	go func() {
		for {
			select {
			case <-done:
				fmt.Println("Aggregates Done!")
				ticker.Stop()
				return
			case t := <-ticker.C:
				log.Printf("Calling  %v at time:%v \n", laggr.name, t)
				tfunc(laggr)
			}
		}
	}()
}

func fill_aggregates(key string, ctype string, name string, value interface{}, laggr *local_aggregate) *aggregate {
	var aggr *aggregate

	laggr.mux.Lock()
	if laggr.resource_aggr == nil {
		resource_aggr := make(map[string]map[string]*aggregate)
		laggr.resource_aggr = resource_aggr
		laggr.reset_time()
	}

	_, ok := laggr.resource_aggr[key]
	if !ok {
		resource_aggr := make(map[string]*aggregate)
		laggr.resource_aggr[key] = resource_aggr
	}

	if aggr, ok = laggr.resource_aggr[key][name]; !ok {
		aggr = new(aggregate)
		aggr.Initialize(ctype, "")
		laggr.resource_aggr[key][name] = aggr
		from_aggr, ok := value.(*aggregate)
		if ok {
			*aggr = *from_aggr
			aggr.Setstate("updated")
			laggr.mux.Unlock()
			return (aggr)
		}
	}

	from_aggr, ok := value.(*aggregate)
	if ok {
		aggr.Rollup(from_aggr)
		laggr.mux.Unlock()
		return (aggr)
	}

	// if this was called during roll-up, just copy the rolled up aggregates
	// and return

	// Otherwise set value based on value coming in from the source
	aggr.Setvalues(value)
	laggr.mux.Unlock()
	return (aggr)
}

func calc_aggr(rec *cooked.Stats_record, pdata interface{}) {

	laggr := &local_aggregates[num_aggregates-1]
	//if rec.Ts < laggr.start_of_period {
	if rec.Ts < (laggr.start_of_period - laggr.period*1000) {
		log.Println("Older data ignored:", laggr.start_of_period, ":", rec)
		return
	}

	aname := rec.Resource + ":" + rec.Asset_name
	md, ok := metadata_map[aname]
	if !ok {
		// Create metadata for every resource
		md = new(Metadata)
		md.Resource_id = rec.Resource
		md.Entity_name = rec.Entity_name
		md.Asset_name = rec.Asset_type + ":" + rec.Asset_name
		md.Kv = rec.Kv
		md.App = rec.App
		md.flag = "Ready"
		metadata_map[aname] = md
	}

	if len(rec.Kv) > 0 {
		md.Kv = rec.Kv
	}

	for gauge, v := range rec.Gauges {
		aggr := fill_aggregates(aname, "gauge", gauge, v, &local_aggregates[num_aggregates-1])
		aggr.resource_id = rec.Resource
		//log.Println("Calculated aggregates:", gauge, ":", *aggr)
	}
	for counter, v := range rec.Counters {
		aggr := fill_aggregates(aname, "counter", counter, v, &local_aggregates[num_aggregates-1])
		aggr.resource_id = rec.Resource
		//log.Println("Calculated aggregates:", counter, ":", *aggr)
	}
}

func rollup_aggr(laggr *local_aggregate) {

	var wg sync.WaitGroup
	for key, raggr := range laggr.resource_aggr {
		wg.Add(1)

		go func(key string, laggr *local_aggregate, ragg interface{}) {
			// Create record and publish
			var num_ctrs = 0
			defer wg.Done()

			md, ok := metadata_map[key]
			if !ok {
				log.Printf("Metadata for aggregate not available. Skipping rollup:%s:%s", laggr.name, key)
				return
			}
			raggr := ragg.(map[string]*aggregate)

			aggr_rec := new(Stats_record)
			aggr_rec.Aggregation_type = laggr.name
			aggr_rec.Aggregation_interval = int64(laggr.period)
			aggr_rec.Aggregation_table = make(map[string]aggregate)

			for cntr, aggr := range raggr {
				if laggr.prev == nil {
					if aggr.Count == 0 {
						log.Println("Skipping - no new records or not updated", md.Asset_name+":"+laggr.name, key, aggr)
						continue
					}
				} else {
					if aggr.Count == 0 || aggr.Getstate() != "updated" {
						log.Println("Skipping - no new records or not updated", md.Asset_name+":"+laggr.name, key, aggr)
						continue
					}
				}

				aggr_rec.Aggregation_table[cntr] = *aggr
				num_ctrs++

				// roll-up to next level
				if laggr.next != nil {
					log.Printf("%s:%s :Rolling up aggregate:%s %v\n", laggr.name, key, cntr, *aggr)
					naggr := fill_aggregates(key, aggr.Stat_type, cntr, aggr, laggr.next)
					naggr.resource_id = aggr.resource_id
					log.Printf("To: %s:%v\n", laggr.next.name, *naggr)
				}
				aggr.Setstate("rolledover")
				// reset all stats
				//aggr.Initialize(aggr.Stat_type, "re-init")
			}

			//aggr_rec.Ts = (time.Now().Unix() * 1000)
			aggr_rec.Ts = time.Now().Round(time.Millisecond).UnixNano() / 1e6
			if num_ctrs > 0 {
				aggr_rec.Resource_id = md.Resource_id
				aggr_rec.Entity_name = md.Entity_name
				aggr_rec.Asset_name = md.Asset_name
				aggr_rec.Kv = md.Kv
				aggr_rec.app = md.App
				atopic := md.App + "_stats_" + laggr.name
				publish_rec(atopic, aggr_rec)
			} else {
				log.Println("Skipping as no new records", md.Asset_name+":"+laggr.name, key)
			}
			return
		}(key, laggr, raggr)
	}
	// Start new period
	wg.Wait()
}

var push_aggr = func(laggr *local_aggregate) {

	var wg sync.WaitGroup

	laggr.mux.Lock()
	raggr_list := laggr.resource_aggr
	prev_la := laggr.prev
	laggr.mux.Unlock()

	if prev_la != nil {
		//roll-up now
		prev_la.mux.Lock()
		rollup_aggr(prev_la)
		prev_la.resource_aggr = nil
		prev_la.mux.Unlock()

		// dump laggr resource records to kafka
		raggr_list := laggr.resource_aggr
		if len(raggr_list) > 0 {
			fmt.Println("Dumping Resource records: ", laggr.name, raggr_list)
			msg_json, _ := json.Marshal(raggr_list)
			key := laggr.name + ":" + my_node_id
			topic := "aggregates_checkpoint_compact"
			mykafka.Push(stats_publish_channel, topic, key, msg_json)
		}
	}

	for key, raggr := range raggr_list {
		// Create record and publish
		wg.Add(1)

		go func(key string, laggr *local_aggregate, ragg interface{}) {
			var num_ctrs = 0

			defer wg.Done()

			md, ok := metadata_map[key]
			if !ok {
				log.Printf("Metadata for aggregate not available. Skipping publish:%s:%s", laggr.name, key)
				return
			}

			raggr := ragg.(map[string]*aggregate)
			aggr_rec := new(Stats_record)
			aggr_rec.Aggregation_type = laggr.name
			aggr_rec.Aggregation_interval = int64(laggr.period)
			aggr_rec.Aggregation_table = make(map[string]aggregate)

			for cntr, aggr := range raggr {
				if aggr.Count == 0 || aggr.Getstate() != "updated" {
					continue
				}

				aggr_rec.Aggregation_table[cntr] = *aggr
				num_ctrs++
				//aggr.Setstate("published")
			}

			if num_ctrs > 0 {
				//aggr_rec.Ts = (time.Now().Unix() * 1000)
				aggr_rec.Ts = time.Now().Round(time.Millisecond).UnixNano() / 1e6
				aggr_rec.Resource_id = md.Resource_id
				aggr_rec.Entity_name = md.Entity_name
				aggr_rec.Asset_name = md.Asset_name
				aggr_rec.Kv = md.Kv
				aggr_rec.app = md.App
				atopic := md.App + "_stats_" + laggr.name
				msg_json := publish_rec(atopic, aggr_rec)
				if msg_json != nil {
					log.Println("Publishing aggregate :", md.Asset_name+":"+string(msg_json))
				}
			} else {
				log.Println("Skipping as no new records", md.Asset_name+":"+laggr.name, key)
			}
			return
		}(key, laggr, raggr)
	}
	wg.Wait()
	laggr.reset_time()
}

func publish_rec(atopic string, rec *Stats_record) []byte {
	key := rec.Resource_id
	msg_json, err := json.Marshal(rec)
	if err == nil {
		mykafka.Push(stats_publish_channel, atopic, key, msg_json)
		published_topics[atopic] = true
		return msg_json
	} else {
		log.Println("Aggregates:Error in Marshaling:", rec)
		return nil
	}
}

func Offset(topic string) int64 {
	// Return the oldest offset for the only
	if topic == "aggregates_checkpoint_compact" {
		return -2
	}
	return -1
}

func create_headers() {

	header := new(sarama.RecordHeader)
	header.Key = []byte("nodeid")
	header.Value = []byte(my_node_id)
	headers = append(headers, *header)
}

func check_headers(headers []*sarama.RecordHeader) bool {

	for _, header := range headers {
		if string(header.Key) == "node_id" && string(header.Value) == my_node_id {
			return true
		}
	}
	return false
}

func Consume(app string, mesg interface{}) {
	msg := mesg.(*sarama.ConsumerMessage)

	if len(msg.Value) == 0 {
		return
	}

	keys := strings.Split(string(msg.Key), ":")

	if len(keys) == 2 {
		if keys[1] == my_node_id {
			log.Println("Ignoring data published by this node")
			return
		}
	} else {
		return
	}
	/*
		mine := check_headers(msg.Headers)

		if mine {
			log.Println("Ignoring data published by this node")
			return
		}
	*/

	la := find_local_aggregate_by_name(string(keys[0]))
	if la != nil {
		la.mux.Lock()
		if la.resource_aggr == nil {
			resource_aggr := make(map[string]map[string]*aggregate)
			la.resource_aggr = resource_aggr
			la.reset_time()
		}
		temp := make(map[string]map[string]aggregate)
		//		err := json.Unmarshal(msg.Value, &la.resource_aggr)
		err := json.Unmarshal(msg.Value, &temp)
		if err != nil {
			log.Println("Json error during recovery :", err)
		} else {
			for key, raggr := range temp {
				key_ra := make(map[string]*aggregate)
				for cntr, aggr := range raggr {
					if aggr.Count == 0 {
						continue
					}
					copy_aggr := new(aggregate)
					copy_aggr.Copy(&aggr)
					//*copy_aggr = aggr
					key_ra[cntr] = copy_aggr
					log.Printf("Restored/Updated %s:%s:%s:", string(keys[0]), key, cntr)
					log.Printf("Data %v", aggr)
				}
				la.resource_aggr[key] = key_ra
				/*
					md, ok := metadata_map[key]
					if !ok {
						// Create metadata for every resource
						md = new(Metadata)
						metadata_map[key] = md
					}
				*/
			}
		}
		la.mux.Unlock()
	}
}
