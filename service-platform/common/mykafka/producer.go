//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package mykafka

import (
	"errors"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/service-platform/tc-mw/utils"
	sarama "gopkg.in/Shopify/sarama.v2"
)

var client_producer_map map[string]*producer_handle
var topic_exists map[string]bool
var topic_config map[string]*Topic_config

var (
	// kafka
	KafkaBrokerUrls []string
	KafkaVerbose    bool
	KafkaClientId   string
	//KafkaTopic           string
	Kafka_num_partitions int = 10
	Kafka_producer       sarama.AsyncProducer
)

var (
	wg                  sync.WaitGroup
	enqueued, successes int
)

type producer_handle struct {
	client          sarama.Client
	ap              sarama.AsyncProducer
	handler_running bool
}

func GetProducer(client_id string) *producer_handle {
	ph := client_producer_map[client_id]
	if ph == nil {
		client, kp := newProducer(KafkaBrokerUrls, client_id)

		if kp != nil && client != nil {
			ph := new(producer_handle)
			ph.client = client
			ph.ap = kp
			ph.handler_running = false
			client_producer_map[client_id] = ph
			return ph
		}
		return nil
	} else {
		return ph
	}
}

func GetTopics(p interface{}) ([]string, error) {
	client := p.(*producer_handle).client
	topics, err := client.Topics()
	return topics, err
}

func GetBroker(p interface{}) *sarama.Broker {
	client := p.(*producer_handle).client
	brokers := client.Brokers()
	if len(brokers) == 0 {
		log.Panicf("Cannot find broker")
	}
	broker := brokers[0]

	err := broker.Open(nil)
	if err != nil {
		log.Println("Cannot get Broker", err)
		return nil
	}
	return broker
}

func GetBroker_scratch() *sarama.Broker {
	broker := sarama.NewBroker(KafkaBrokerUrls[0])
	err := broker.Open(nil)
	if err != nil {
		log.Println("Cannot get Broker", err)
		return nil
	}
	return broker
}

func GetMetadata(broker *sarama.Broker, topic string) *sarama.TopicMetadata {

	request := sarama.MetadataRequest{Topics: []string{topic}}

	response, err := broker.GetMetadata(&request)
	if err != nil {
		return nil
	} else {
		return response.Topics[0]
	}
}

func newProducer(brokerList []string, client_id string) (sarama.Client, sarama.AsyncProducer) {

	// For the access log, we are looking for AP semantics, with high throughput.
	// By creating batches of compressed messages, we reduce network I/O at a cost of more latency.
	vers := "2.3.1"
	version, err := sarama.ParseKafkaVersion(vers)
	if err != nil {
		log.Panicf("Error parsing Kafka version: %v", err)
	}

	/**
	 * Construct a new Sarama configuration.
	 * The Kafka cluster version has to be defined before the consumer/producer is initialized.
	 */
	config := sarama.NewConfig()
	config.Version = version
	config.ClientID = client_id
	//	tlsConfig := createTlsConfiguration()
	//	if tlsConfig != nil {
	//		config.Net.TLS.Enable = true
	//		config.Net.TLS.Config = tlsConfig
	//	}
	config.Producer.RequiredAcks = sarama.WaitForLocal       // Only wait for the leader to ack
	config.Producer.Compression = sarama.CompressionSnappy   // Compress messages
	config.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms
	config.Producer.Return.Successes = true
	config.Producer.Return.Errors = true

	client, err := sarama.NewClient(brokerList, config)
	if err != nil {
		log.Panicf("Producer : Error creating Kafka client")
	}

	producer, err := sarama.NewAsyncProducerFromClient(client)
	if err != nil {
		log.Println("Failed to start Sarama producer:", err)
		return client, nil
	}

	// We will just log to STDOUT if we're not able to produce messages.
	// Note: messages will only be returned here after all retry attempts are exhausted.

	return client, producer
}

func Close(p interface{}) {
	prod := p.(*producer_handle).ap
	prod.Close()
}

type Topic_config struct {
	Num_parts          int32
	Replication_factor int16
	Compact            bool
}

var compact_str string = "compact"
var delete_str string = "delete"
var valhour string = "86400000"
var val1 string = "1000"
var val2 string = "100"
var val3 string = ".01"

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func Topic_exists(p interface{}, topic string) bool {

	if exists := topic_exists[topic]; !exists {
		topics, err := GetTopics(p)
		if err == nil {
			if Contains(topics, topic) {
				topic_exists[topic] = true
			} else {
				return false
			}
		} else {
			return false
		}
	}
	return true
}

func Create_topic_broker(p interface{}, topic string, tc *Topic_config) error {

	if !Topic_exists(p, topic) {
		broker := GetBroker(p)

		if broker != nil {
			err := broker_create_topic(broker, topic, tc)
			if err != nil {
				log.Println("Couldn't create topic", topic, err)
				topic_exists[topic] = false
				return err
			} else {
				log.Printf("Topic Exists/Created topic %s ", topic)
				topic_exists[topic] = true
				return nil
			}

			/*
				md := GetMetadata(broker, topic)
				if md != nil {
					if md.Err == sarama.ErrUnknownTopicOrPartition {
						// create topic
						err := broker_create_topic(broker, topic, tc)
						if err != nil {
							log.Println("Couldn't create topic", topic, err)
							topic_exists[topic] = false
							return err
						} else {
							log.Print("Created topic %s ", topic)
							topic_exists[topic] = true
							return nil
						}
					} else if md.Err == sarama.ErrNoError {
						if md.Name == topic {
							topic_exists[topic] = true
							return nil
						} else {
							log.Println("Topic describe Name mismatch", md[0].Name)
							err := errors.New("Topic Name Mismatch")
							topic_exists[topic] = false
							return err
						}
					} else {
						log.Println("Topic describe Error", md[0].Err)
						err := errors.New("Topic Describe  Error")
						topic_exists[topic] = false
						return err
					}
				} else {
					// create topic
					log.Println("Topic describe Error", err)
					err = broker_create_topic(broker, topic, tc)
					if err != nil {
						log.Println("Couldn't create topic", topic, err)
						topic_exists[topic] = false
						return err
					} else {
						log.Printf("Created topic %s ", topic)
						topic_exists[topic] = true
						return nil
					}
				}
			*/
		} else {
			err := errors.New("Cannot get broker")
			return err
		}
	}
	return nil
}

func Create_topic(p interface{}, topic string, tc *Topic_config) error {

	if !Topic_exists(p, topic) {
		// Check if topic exists. Otherwise create it
		client := p.(*producer_handle).client
		cadmin, err := sarama.NewClusterAdminFromClient(client)

		if err != nil {
			log.Printf("Cannot get ClusterAdmin :", err)
		} else {

			topics := []string{topic}
			md, err := cadmin.DescribeTopics(topics)
			if err == nil {
				if md[0].Err == sarama.ErrUnknownTopicOrPartition {
					// create topic
					err := create_topic(cadmin, topic, tc)
					if err != nil {
						log.Println("Couldn't create topic", topic, err)
						topic_exists[topic] = false
						return err
					} else {
						log.Printf("Created topic %s ", topic)
						topic_exists[topic] = true
						return nil
					}
				} else if md[0].Err == sarama.ErrNoError {
					if md[0].Name == topic {
						topic_exists[topic] = true
						return nil
					} else {
						log.Println("Topic describe Name mismatch", md[0].Name)
						err := errors.New("Topic Name Mismatch")
						topic_exists[topic] = false
						return err
					}
				} else {
					log.Println("Topic describe Error", md[0].Err)
					err := errors.New("Topic Describe  Error")
					topic_exists[topic] = false
					return err
				}
			} else {
				// create topic
				log.Println("Topic describe Error", err)
				err = create_topic(cadmin, topic, tc)
				if err != nil {
					log.Println("Couldn't create topic", topic, err)
					topic_exists[topic] = false
					return err
				} else {
					log.Printf("Created topic %s ", topic)
					topic_exists[topic] = true
					return nil
				}
			}
		}
	}
	return nil
}

func broker_create_topic(broker *sarama.Broker, topic string, config *Topic_config) error {
	var td sarama.TopicDetail
	var ctreq sarama.CreateTopicsRequest

	if config != nil {
		td.NumPartitions = config.Num_parts
		td.ReplicationFactor = config.Replication_factor
		centries := make(map[string]*string)

		if config.Compact {
			centries["cleanup.policy"] = &compact_str
			centries["delete.retention.ms"] = &val1
			centries["segment.ms"] = &val2
			centries["min.cleanable.dirty.ratio"] = &val3
		}
		td.ConfigEntries = centries
	} else {

		// Defaults
		centries := make(map[string]*string)
		centries["cleanup.policy"] = &delete_str
		centries["delete.retention.ms"] = &valhour
		td.NumPartitions = 4
		td.ReplicationFactor = 1
		td.ConfigEntries = centries
	}
	ctreq.TopicDetails = make(map[string]*sarama.TopicDetail)
	ctreq.TopicDetails[topic] = &td
	ctreq.Version = 1
	ctreq.Timeout = time.Duration(180) * time.Second

	ctresp, err := broker.CreateTopics(&ctreq)
	if err == nil {
		kerr := ctresp.TopicErrors[topic].Err
		errmsg := ctresp.TopicErrors[topic].ErrMsg
		if kerr == sarama.ErrTopicAlreadyExists {
			if errmsg != nil {
				log.Println("Topic exists :", *errmsg)
			} else {
				log.Println("Topic exists :", err)
			}
			err = nil
		} else if kerr == sarama.ErrNoError {
			err = nil
		} else {
			if errmsg != nil {
				err = errors.New(*errmsg)
			} else {
				err = errors.New("kafka error: " + string(kerr))
			}
		}
	}
	return err
}

func create_topic(ca sarama.ClusterAdmin, topic string, config *Topic_config) error {
	var td sarama.TopicDetail

	if config != nil {
		td.NumPartitions = config.Num_parts
		td.ReplicationFactor = config.Replication_factor
		centries := make(map[string]*string)

		if config.Compact {
			centries["cleanup.policy"] = &compact_str
			centries["delete.retention.ms"] = &val1
			centries["segment.ms"] = &val2
			centries["min.cleanable.dirty.ratio"] = &val3
		}
		td.ConfigEntries = centries
	} else {

		// Defaults
		centries := make(map[string]*string)
		centries["cleanup.policy"] = &delete_str
		centries["delete.retention.ms"] = &valhour
		td.NumPartitions = 4
		td.ReplicationFactor = 1
		td.ConfigEntries = centries
	}

	err := ca.CreateTopic(topic, &td, false)
	return err
}

func Get_topic_config(topic string) *Topic_config {

	if _, ok := topic_config[topic]; !ok {
		tc := new(Topic_config)
		if strings.Contains(topic, "compact") {
			tc.Num_parts = 1
			tc.Replication_factor = 1
			tc.Compact = true
		} else {
			tc.Compact = false
			tc.Num_parts = 4
			tc.Replication_factor = 1
		}
		topic_config[topic] = tc
	}
	return topic_config[topic]
}

func Push(p interface{}, topic string, key string, value []byte) {

	// Get Async producer

	ph := p.(*producer_handle)
	prod := ph.ap

	if !ph.handler_running {
		go func() {
			defer wg.Done()
			for p := range prod.Successes() {
				log.Println("Write Success:", p.Topic, p.Offset, p.Metadata)
				successes++
			}
		}()

		go func() {
			defer wg.Done()
			for err := range prod.Errors() {
				log.Println(err.Error())
			}
		}()
	}

	p.(*producer_handle).handler_running = true
	t := time.Now().UTC()
	prod.Input() <- &sarama.ProducerMessage{
		Topic:     topic,
		Key:       sarama.StringEncoder(key),
		Value:     sarama.ByteEncoder(value),
		Metadata:  key,
		Timestamp: t,
	}
}

func InitProducer() {
	client_producer_map = make(map[string]*producer_handle)
	topic_exists = make(map[string]bool)
	topic_config = make(map[string]*Topic_config)
}

func init() {
	k := utils.Getenv("KAFKA_BROKERS", "172.17.0.6:9092")
	KafkaBrokerUrls = strings.Split(k, ",")

	b := utils.Getenv("KAFKA_VERBOSE", "true")
	KafkaVerbose, _ := strconv.ParseBool(b)

	if len(KafkaBrokerUrls) == 0 {
		panic("no Kafka bootstrap brokers defined, please set the -brokers flag")
	}

	if KafkaVerbose {
		sarama.Logger = log.New(os.Stdout, "[tcafe] ", log.LstdFlags)
	}
}
