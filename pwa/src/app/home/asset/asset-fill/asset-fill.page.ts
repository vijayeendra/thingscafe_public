//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { Input, Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormArray, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NavParams, NavController , ModalController} from '@ionic/angular';
import {asset_type_info} from '../../../services/asset_type.service';
import {asset_info} from '../../../services/asset.service';
import {CustomValidator} from '../../../common/custom-validator';


@Component({
  selector: 'app-asset-fill',
  templateUrl: './asset-fill.page.html',
  styleUrls: ['./asset-fill.page.scss'],
})
export class AssetFillPage implements OnInit {

	@Input() asset:asset_info;
	@Input() atypinfo:asset_type_info;
	@Input() entity_id:string;
	@Input() controller_asset_id:string;
	@Input() action:string;
	@Input() asset_types:asset_type_info[];
	@Input() assets:asset_info[];

	//config:any[] = JSON.parse(input_json);
	config:any[]; 
	changeForm: FormGroup;

	asset_options:any[];
	subtypes:any[];
	submitAttempt: boolean = false;
	title:string;

	ngOnInit() {
		console.log("Asset type details", this.atypinfo);
		//		console.log("Config :",this.config);
		if (this.action == 'Add') {
			this.asset = new (asset_info);
			this.asset.name = '';
			this.asset.description = '';
			this.asset.entity_id = this.entity_id;
			this.asset.parent_asset_id = this.controller_asset_id;
			this.asset.asset_type = this.atypinfo.id;
			this.asset.user_defined = null;
			this.asset.mobile = '';
			this.asset.serial_no = '';
			if (this.atypinfo.root_asset_flag) {
				this.asset.root_asset_flag = true;
			} else {
				this.asset.root_asset_flag = false;
			}
			this.asset.subtype = '';
			this.asset.location = '';
			this.asset.configured = true;
		}

		if (this.asset_options.length == 0) {

			let maxvalue = Number(this.atypinfo.kv['max']);
			let minvalue = Number(this.atypinfo.kv['min']);
			let name = this.atypinfo.kv['name_option'];
			//let maxvalue = 12;
			for(let i = minvalue;i <= maxvalue ;i++) {
				let temp = this.get_asset(name+i);
				if (temp) {
					if (temp.configured == false) {
						this.asset_options.push(name + i);
					}
				} else {
					this.asset_options.push(name + i);
				}
			}
	        this.subtypes = this.asset_type_subtypes();
		}
	    this.changeForm = this.creategroup();
		this.title = this.atypinfo.name + " " + this.asset.name;
	}

	private get_asset(name:string) {
		if (this.assets) {
			for (let asset of this.assets) {
				if (asset.name != name) 
					continue;
				return asset;
			}
		}
		return null;
	}

	private get_chosen_assets(group_type_id:string, atype_name:string) {
		if (this.assets) {
			var clist:string[]; 
			clist = [];
			for (let asset of this.assets) {
				if (asset.asset_type != group_type_id) 
					continue;
				if (this.action == "Modify" &&  asset.name == this.asset.name)
					continue;

				let collm = JSON.parse(asset.collmap);
				let tempmap = JSON.parse(collm[atype_name]);
				clist.push(...tempmap["chosen_list"]);
			}
			return clist;
		}
		return null;
	}

  private get_asset_options(asset_type_id:string, value:string) {

	    let atype_info = this.asset_type_details(asset_type_id);

		let json_options = value;
		let options = JSON.parse(json_options);
		let maxvalue = Number(options['max']);
		let minvalue = Number(options['min']);


		let name = atype_info.kv['name_option'];
		let asset_type_name = atype_info.name;
	    let asset_options = [];
	    
		for(let i = minvalue;i <= maxvalue ;i++) {
			asset_options.push(name + i);
		}

	  return(asset_options);
  }

	private get_asset_choices(asset_type_id:string, subtype?:string, asset_group_type_id?:string) {

	    let atype_info = this.asset_type_details(asset_type_id);

		let maxvalue = Number(atype_info.kv['max']);
		let minvalue = Number(atype_info.kv['min']);

		let name = atype_info.kv['name_option'];
	    let asset_choices = [];
	    
	    var found:any;

		var clist:string[];
		clist = this.get_chosen_assets(asset_group_type_id, atype_info.name);

	    this.assets.map(asset => {
			if (!asset.configured || asset.asset_type != atype_info.id) 
				return;
			if (asset.subtype != "" && subtype && asset.subtype != subtype) 
				return;

			if (this.action != "Modify") {
				if (clist) {
					let i = clist.indexOf(asset.name);
					if (clist.indexOf(asset.name) > -1) {
						return;
					}
				}
			} else {
					if (clist) {
						if (clist.indexOf(asset.name) > -1) {
							return;
						}
					}
			}

			asset_choices.push(asset.name);
		});

		return(asset_choices);
	}

  private asset_type_details(id:string):asset_type_info {
		if (this.asset_types) {
			for(let item of this.asset_types ) {
				if (item.id == id) {
					return item;
				}
			}
		} else {
			return null;
		}
  }

  private asset_type_subtypes(id?:string):string[] {
	  var kv_info:any;
	  if (id) {
		  kv_info = this.asset_type_details(id).kv;
	  } else {
		  kv_info = this.atypinfo.kv;
	  }
	  
	  if (kv_info['types']) {
		  return JSON.parse(kv_info['types']);
	  } else {
		  return [];
	  }
  }

  private asset_type_operations(id?:string):string[] {
	  var kv_info:any;
	  if (id) {
		  kv_info = this.asset_type_details(id).kv;
	  } else {
		  kv_info = this.atypinfo.kv;
	  }
	  
	  if (kv_info['operations']) {
		  return JSON.parse(kv_info['operations']);
	  } else {
		  return [];
	  }
  }

  private creategroup():FormGroup {

	const group = this.formBuilder.group({
	});
	  /*
	const group = this.formBuilder.group({
	},{ updateOn: 'blur' });
	   */
	
	var value:any;
	var disabled:boolean;
	for (let field of Object.keys(this.asset)) {
		disabled = false;
		value = this.asset[field];
		if (this.action == "Modify" || this.action == "Copy") {
			if (this.action == "Copy" && field == "name") {
				value = '';
			} else if (this.action == "Modify" && field == "name") {
				disabled = true;
				this.title = value;
			} 
		}

		let validation_list = this.customvalidator.create_validations(100, field);
		group.addControl(field, this.formBuilder.control({value:value, disabled:disabled}, Validators.compose(validation_list)));
		//group.addControl(field, this.formBuilder.control(''));
	}
	
	if (this.atypinfo.addressable != true)  {
		group.removeControl('mobile');
		group.removeControl('serial_no');
	}

	
	if (this.action == "Add" || this.action == "Modify" || this.action == "Copy") {
		let asset_map = this.atypinfo.collection_type_list;
		if (asset_map) {
			var chosen_list:string[] = [];
			group.setControl('collmap', new FormArray([]));
			let collections = group.get('collmap') as FormArray;
			Object.keys(asset_map).map(key => {
				let asset_type_d = JSON.parse(asset_map[key]);
				let filter_subtype = asset_type_d.subtype;
				let atype_info = this.asset_type_details(key);
				//			let asset_choices = this.get_asset_options(key, asset_map[key]);
				let subtypes = this.asset_type_subtypes(key);
				let asset_choices = this.get_asset_choices(key, filter_subtype, this.atypinfo.id);
				if (this.action == "Modify") {
					let collm = JSON.parse(this.asset.collmap);
					let tempmap = JSON.parse(collm[atype_info.name]);
					chosen_list = tempmap["chosen_list"];
				} else if (this.action == "Copy") {
					let collm = JSON.parse(this.asset.collmap);
					let tempmap = JSON.parse(collm[atype_info.name]);
					chosen_list = asset_choices.filter(function(v,i,a) {
						 return ! tempmap["chosen_list"].includes(v);
					});
				}
				//let req_list_validation = this.customvalidator.create_validations(100, "requiredlist");
				let req_list_validation = null;
				collections.push(this.formBuilder.group({
					asset_type: [key],
					asset_type_name: [atype_info.name],
					chosen_list : [chosen_list, Validators.compose(req_list_validation)],
					alist : [asset_choices],
					subtype_list : [subtypes],
					asset_id :[''],
					properties : ['']
				}));
			});
		}
	}

	return group;
  }

  private getControls() {
	  return(<FormArray> (this.changeForm.get('collmap')));
  }

  private getcurrentvalue(i:any, option:any) {
	  let control = <FormArray>(this.getControls());
	  let g = control.controls[i]['controls'];
	  let alist = g['alist'].value;
	  return (alist.includes(option));
  }

  private getvalue(i:any) {
	  let control = <FormArray>(this.getControls());
	  let g = control.controls[i]['controls'];
	  return (g['alist'].value);
  }

  private getname(i:any) {
	  let control = <FormArray>(this.getControls());
	  let g = control.controls[i]['controls'];
      return (g['asset_type_name'].value);
  }


	constructor(public navCtrl: NavController, 
		public formBuilder: FormBuilder,
		public customvalidator: CustomValidator,
		private modalCtrl: ModalController
	) { 
	  this.asset_options = [];
  }

  next() {
	  //	  this.fillupSlider.slideNext();
  }

  prev() {	
	  //this.fillupSlider.slidePrev();
  }

  save() {
	console.log("Saved");
	console.log(this.changeForm.value);
	this.modalCtrl.dismiss(this.changeForm.getRawValue());
  }

  cancel() {
	console.log("Cancelled");
	this.modalCtrl.dismiss(null);
  }

}
