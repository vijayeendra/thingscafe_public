//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { environment, tcapp } from '../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { LoginService } from '../services/login.service';
import { EntityService } from '../services/entity.service';
import { GlobalService } from '../services/global.service';
import { user_cred, user_info, UserService } from '../services/user.service';
import { Observable } from  'rxjs/Observable';
import { take } from  'rxjs/operators';
import { ActivatedRoute, Router, RouterEvent, NavigationEnd } from '@angular/router';
import { LoadingController, AlertController, NavController, ModalController } from '@ionic/angular';

const TOKEN_KEY = "X-Auth-Token";
const LOGINAS_TOKEN_KEY = "LoginAs-Token";
const USER_NAME = "USER_NAME";
const USER = "USER";
const HOST_URL = "HOST_URL";
const ENTITY_ID = "ENTITY_ID";
const USER_ENTITY_TYPE = "USER_ENTITY_TYPE";
const USER_ENTITY_TYPE_STR = "USER_ENTITY_TYPE_STR";
const DEBUG = "DEBUG_FLAG";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})

export class SettingsPage implements OnInit {

	login_form : FormGroup;
	server_form : FormGroup;
	password_form : FormGroup;
	token : string;
	action : string;
	user_details : user_info;
	entity_type_str : string;
	authState$: Observable<boolean>;


    carray:string[];
	//  constructor(public navCtrl : NavController, private loginsvc :LoginService);
	constructor(public navCtrl : NavController, 
	    private fb: FormBuilder, 
	    private usersvc : UserService, 
	    private entitysvc : EntityService, 
	    public global : GlobalService, 
		private aroute: ActivatedRoute,
		private alertCtrl: AlertController, 
		private storage: Storage, 
	    private loginsvc : LoginService) { 

		this.carray = [];
		this.loginsvc.ready().subscribe(resp => {
			let hosturl = this.loginsvc.hostUrl;
			let username = this.loginsvc.username;
			this.server_form = this.fb.group ({
			  server: new FormControl(hosturl, Validators.required)
			});
			this.login_form = this.fb.group ({
			  username: new FormControl(username, Validators.compose([
				  Validators.required,
				  Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			  ])),
				password: new FormControl('', Validators.required),
				saas_entity_name : new FormControl(tcapp.saas, Validators.required)
			});
			this.password_form = this.fb.group ({
			  current : new FormControl('', Validators.required),
			  newpass : new FormControl('', Validators.required),
			  newpass1 : new FormControl('', Validators.required)
			});
		});
	};

	ngOnInit() {
		this.ViewLoad();
	}

	ionViewWillEnter(){
		console.log("ionViewWillEnter");
	}

	ionViewDidEnter(){
		console.log("DidEnter");
	}


	ionViewDidLoad() {
		console.log("ionViewDidLoad");
		this.ViewLoad();
	}

	ViewLoad() {
		this.action = this.aroute.snapshot.paramMap.get('action');
		if (this.action == "logout") {
			this.onlogout();
		}
		this.global.ready().subscribe(resp => {
			this.usersvc.ready().subscribe(resp => {
				this.authState$ = this.usersvc.checkloginstatusObserver();
				this.authState$.pipe(take(2),)
				  .subscribe(x => {
					if (x == true) {
						this.get_user_details();
						this.get_entity_details();
						console.log("Logged in");
					} else {
						console.log("Logged out");
					}
				});
			});
		});
	}

	set_tracing (enable_flag:boolean) {
		this.global.set_stored(DEBUG, enable_flag);
	}

	labelClick(e) {
		console.log(e.target);

		//		this.navCtrl.navigateRoot(["/home/entity", {id: this.global.user_entity, type: this.global.user_entity_type}]);
		this.navCtrl.navigateForward(['/config', {entity_id:this.global.user_entity, atype_id: this.global.user_entity, controller_id:"", context:this.carray}]);
	}
	
		/* TBD ..
	update_profile () {

		console.log(this.password_form.value);
		if (this.password_form.status == "VALID" ) {
			B
			this.loginsvc
			.login(this.login_form.value)
			.subscribe( (response) => {
				this.usersvc.store_user_auth(response).then (resp => {
					if (resp) {
						this.usersvc.setloginstatus(true);
						//this.navCtrl.navigateForward('/home');
						this.navCtrl.navigateRoot('/home');
					}
				});
			},
			e => {
				this.presenterror(e, "Update Profile ");
			});
		}
	}
	*/

	change_passwd () {
		if (this.password_form.status == "VALID" ) {
		  let ucred = new(user_cred);
		  ucred.id = this.user_details.id;
		  ucred.password = this.password_form.controls.newpass.value;
		  this.usersvc.create_user_cred(ucred, true)
		  .subscribe(resp3 => {
			if (resp3.status != 200) {
			  let header = 'Change password failed' + this.user_details.email;
			  this.alertCtrl.create({
					  header: header,
					  buttons: ['Dismiss']
			  }).then(alert => alert.present());
			} else {
			  let header = 'Change password successful:' + this.user_details.email;
			  this.alertCtrl.create({
					header: header,
					buttons: ['Dismiss']
				}).then(alert => alert.present());
			  console.log("Change password successful:", this.user_details.email);
			}
		  },
		  e => {
			  this.presenterror(e, "Password change ");
		  });
		}
	}

	onlogin () {
		console.log(this.server_form.value);
		if (this.server_form.status == "VALID" && this.login_form.status == "VALID") {
			this.global.seturl(this.server_form.value);
			//this.loginsvc.seturl(this.server_form.value);
			//this.usersvc.seturl(this.server_form.value);
			//this.loginsvc.seturl(this.server_form.value);
			this.loginsvc
			.login(this.login_form.value)
			.subscribe( (response) => {
				//this.usersvc.store_user_auth(response);
				//this.usersvc.setloginstatus(true);
				//this.navCtrl.navigateRoot('/home');
				this.usersvc.store_user_auth(response).then(() => {
					let initstate = this.usersvc.checkinitstatusObserver();
					initstate.pipe(take(2),)
						.subscribe(resp => {
						if (resp) {
							this.entitysvc.init_on_login();
							this.entitysvc.ready().subscribe(resp => {
								if (resp) {
									this.usersvc.setloginstatus(true);
									//this.navCtrl.navigateForward('/home');
									this.navCtrl.navigateRoot(["/home/entity", {id: this.global.user_entity, type: this.global.user_entity_type}]);
									//this.navCtrl.navigateRoot('/home/entity');
								}
							});
						}
						});
				});
			},
			e => {
				this.presenterror(e, "Login");
			});
		}
	};

	get_user_details(){
		this.user_details = this.global.user;
		/*
		this.storage.get(USER).then(resp => { 
				this.user_details = resp;
		});
		 */
	}

	get_entity_details(){
		this.entity_type_str = this.global.user_entity_type_string;
		/*
		this.storage.get(USER_ENTITY_TYPE_STR).then(resp => { 
				this.entity_type_str = resp;
		});
		 */
	}

	onlogout () {
		this.entitysvc.onlogout();
		this.usersvc.delete_user_auth();
		this.usersvc.setloginstatus(false);
			//this.navCtrl.navigateRoot('/settings');
	    this.action = 'Login';
	}

	async presenterror(e:any, action?:string) {
		console.error(e)
		let header = action + ' failed';
		this.alertCtrl.create({
		  header: header,
		  message: e.error,
		  buttons: ['Dismiss']
		}).then(alert => alert.present());
	}
}
