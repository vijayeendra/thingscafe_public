# Backup schema for a SAAS vendor after 
# making any changes
# Usage ./schema_backup.sh <backup_dir> <saas vendor>

mkdir -p $2
bfile="$1.schema-$(date +'%m%d%y')"
echo -e "describe keyspace $1;\n" | cqlsh `hostname -i` > $2/$bfile
echo -e "describe keyspace thingscafe_data;\n" | cqlsh `hostname -i` >> $2/$bfile
