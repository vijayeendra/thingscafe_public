//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { environment, tcapp } from '../../../environments/environment';
import { NgModule, Input, Component, OnInit , ViewChild, ElementRef} from '@angular/core';
import { Chart } from 'chart.js';
import { GlobalService } from '../../services/global.service';
import { take } from  'rxjs/operators';
import { CommonModule } from "@angular/common";
import { IonicModule } from '@ionic/angular';
import { ToastController, ModalController } from '@ionic/angular';

interface dataset {
  label:any;
  labels:any;
  active:any;
  data:any;
  backgroundColor:any; 
  borderColor:any;
  hoverBackgroundColor:any;
  borderWidth:any;
  stack:any;
  fill:boolean;
  spanGaps:boolean;
  type:string;
  pointHoverRadius: number;
  pointHoverBackgroundColor: string;
  pointHoverBorderColor: string;
  pointHoverBorderWidth: number;
}
interface coord {
  x:any;
  y:any;
}


@Component({
  selector: 'app-showdata',
  templateUrl: './showdata.component.html',
  styleUrls: ['./showdata.component.scss']
})

export class ShowdataComponent implements OnInit {
  @Input() json_data:any;
  @Input() message:string;
  @Input() entity_id:string;
  @Input() asset:any;
  @Input() action:any;
  @Input() title:string;
  @Input() debug:boolean;

  @ViewChild("barCanvas1") barCanvas1: ElementRef;
  @ViewChild("barCanvas") barCanvas: ElementRef;
  @ViewChild("doughnutCanvas") doughnutCanvas: ElementRef;
  @ViewChild("lineCanvas") lineCanvas: ElementRef;

  private barChart1: Chart;
  private barChart: Chart;
  private doughnutChart: Chart;
  private lineChart: Chart;
  public sample_data:any; 

  data:any = {};

		  
  constructor(private modalCtrl: ModalController, 
		private toastCtrl: ToastController,
        private global: GlobalService ) { 
		this.sample_data = '"rc":0,"d":"ih","et":705753,"p1":(694822,1,885),"m1":(684822,1,800),"m2":(684822,0,85),"v9":(694848,0,5,4,55),"v11":(684851,0,2,1,54),"v13":(684854,0,2,0,45),"v14":(684858,1,3,1,133,60),"v15":(684861,0,4,2,60),"v16":(684864,0,6,2,60), "v17":(634867,0,7,2,60),"v28":(684822,0,1,0,0),"v29":(684823,0,0,0,0),"ct":(0)';
  }

  ionViewOnEnter() {
	this.data = {};
	this.ngOnInit();
  }

  ngOnInit() {
		//let data = <JSON><unknown>{"rc":2,"d":"ih","et":850371,"p1":[842988,1,10],"m1":[842988,1,10],"v2":[842988,1,10,20,10,50],"v3":[843894,1,15,20,25,50],"ct":[0]} ; 
	//let data = <JSON><unknown>{"rc":2,"d":"ih","et":850371,"p1":[842988,1,10],"m1":[842988,1,10],"v2":[842988,1,10,20,10,50],"v3":[843894,1,15,20,25,50],"v4":[843894,1,15,20,25,50],"v5":[843894,1,15,20,25,50],"v6":[843894,1,15,20,25,50],"v7":[840000,1,15,20,25,50],"v8":[835894,1,15,20,25,50],"v9":[843894,1,15,20,25,50],"v10":[843894,1,15,20,25,50],"v11":[843894,1,15,20,25,50],"v12":[843894,1,15,20,25,50],"ct":[0]} ; 
	//let data = <JSON><unknown>{"rc":0,"d":"ih","et":685753,"p1":[684822,1,885],"m1":[684822,1,885],"m2":[684822,0,1],"v9":[684848,0,5,4,55],"v11":[684851,0,2,1,54],"v13":[684854,0,2,0,45],"v14":[684858,1,3,1,133,60],"v15":[684861,0,4,2,60],"v16":[684864,0,6,2,60], "v17":[684867,0,7,2,60],"v28":[684822,0,1,0,0],"v29":[684823,0,0,0,0],"ct":[0]};
		this.global.ready()
	      .pipe(take(1),)
		  .subscribe(resp => {


		 if (this.json_data) {
			 if (this.action != "" && ! this.action.includes("getoperation:Get Switching History"))  {
				 if (this.message) {
					 this.presentToast("Commands/Response", this.message);
				 }
				 return;
			 }
			 var norm_data:any;
			 if (environment.production == false) {
				 norm_data = this.normalize(this.sample_data)
			 } else {
				 norm_data = this.normalize(this.json_data)
			 }
			 this.data = JSON.parse(norm_data);
			 this.setup_ghcharts(this.data);
		 } else {
			 if (environment.production == false) {
				let norm_data = this.normalize(this.sample_data);
				this.data = JSON.parse(norm_data);
				this.setup_ghcharts(this.data);
			 } else {
				 this.global.app_data.subscribe(resp => {
				  if (resp) {
					let norm_data = this.normalize(resp);
					this.data = JSON.parse(norm_data);
					this.setup_ghcharts(this.data);
				  }
			  });
			 }
		 }
		});
    }

	public normalize(mesg:string, title?:string):string {
	/* GH
	 * 0: ""rc":2,"d":"ih","et":850371,"p1":(842988,0,3),"m1":(842988,0,3),"v2":(842988,0,1,0,3),"v3":(843894,0,1,0,3),"ct":(0)" status: 404
	 */
		var re1 = /\(/gi
		var re2 = /\)/gi
		let str = mesg.replace(re1, "[");
		let str1 = '{' + str.replace(re2, "]") + '}';
	    let obj = JSON.parse(str1);
		if (title != "") {
			obj.title = this.title;
		}
		return JSON.stringify(obj);
	}

  setup_ghcharts(d:any) {
	  var bgndColor =  [
			"rgba(255, 99, 132, 0.2)",
			"rgba(54, 162, 235, 0.2)",
			"rgba(255, 206, 86, 0.2)",
			"rgba(75, 192, 192, 0.2)",
			"rgba(153, 102, 255, 0.2)",
			"rgba(255, 159, 64, 0.2)"
		  ];

	  var borderColor = [
			"rgba(255,99,132,1)",
			"rgba(54, 162, 235, 1)",
			"rgba(255, 206, 86, 1)",
			"rgba(75, 192, 192, 1)",
			"rgba(153, 102, 255, 1)",
			"rgba(255, 159, 64, 1)"
		  ];
	  let labels = [];
	  let vlabels = [];
	  var data:any = {};
	  var data1:any = {};
	  var data2:any = {};
	  var data3:any = {};
	
	  var dataset1:dataset  = <dataset> {}; 
	  var dataset2:dataset  = <dataset> {};
	  var dataset3:dataset  = <dataset> {};
	  var dataset4:dataset  = <dataset> {};
	  var dataset5:dataset = <dataset> {};
	  var dataset6:dataset = <dataset> {};
	  var dataset8:dataset = <dataset> {};
	  var dataset9:dataset = <dataset> {};
	
	  var hoverBackgroundColor=  ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56"];

	  dataset1.label = "Running(in minutes)";
	  dataset2.label = "Scheduled(in minutes)";
	  dataset3.label = "Switching Cycles";
	  dataset4.label = "Nutrient Cycles";
	  dataset1.data = [];
	  dataset2.data = [];
	  dataset3.data = [];
	  dataset4.data = [];
	  dataset1.backgroundColor = bgndColor;
	  dataset2.backgroundColor = bgndColor[1];
	  dataset3.backgroundColor = bgndColor[2];
	  dataset4.backgroundColor = bgndColor[3];
	  dataset1.borderColor= borderColor[0];
	  dataset2.borderColor= borderColor[1];
	  dataset3.borderColor= borderColor[2];
	  dataset4.borderColor= borderColor[3];
	  dataset1.hoverBackgroundColor = hoverBackgroundColor;
	  dataset2.hoverBackgroundColor = hoverBackgroundColor[1];
	  dataset3.hoverBackgroundColor = hoverBackgroundColor[0];
	  dataset4.hoverBackgroundColor = hoverBackgroundColor[0];
	  dataset1.borderWidth= 1;
	  dataset2.borderWidth= 1;
	  dataset3.borderWidth= 1;
	  dataset4.borderWidth= 1;
	  dataset1.stack = 'Stack 0';
	  dataset2.stack = 'Stack 0';
	  dataset3.stack = 'Stack 2';
	  dataset4.stack = 'Stack 3';


	  dataset5.data = [];
	  dataset6.data = [];
	  dataset9.data = [];
	  dataset8.data = [];
	  dataset8.labels = [];
	  dataset9.labels = [];
	  
	  let epoch_margin_time = 1514764800;
	  let current_time = (d.et*60) + epoch_margin_time;
	  var dt = new Date(0);
	  dt.setUTCSeconds(current_time);

	  var start_time = d.et;
	  var co_ord1:coord = <coord>{};
	  var co_ord2:coord = <coord>{};
	  data2.datasets = [];
	  let index = 0;

	  for (let [key, value] of Object.entries(d)) {
		  //let m1 = key.match(/(^[pvm]{1})(\d+)/g);
		  var temp_dt = new Date(0);
		  let re = /(^[pvm]{1})(\d+)/g; 
		  let m1 = re.exec(key);
		  co_ord1 = <coord> {};
		  co_ord2 = <coord> {};
		  if (m1 && Number(m1[2]) > 0) {
			  switch (m1[1]) {
				  case "p" : 
				     if (value[0]) {
					  labels.unshift(m1[0]);
					  dataset8.data.unshift(value[2]);
					  dataset8.labels.unshift(m1[0]);
					 } else {
					  labels.push(m1[0]);
					  dataset8.data.push(value[2]);
					  dataset8.labels.push(m1[0]);
					 }
				  break;
				  case "m" : 
				     if (value[0]) {
					  labels.unshift(m1[0]);
					  dataset9.data.unshift(value[2]);
					  dataset9.labels.unshift(m1[0]);
					 } else {
					  labels.push(m1[0]);
					  dataset9.data.push(value[2]);
					  dataset9.labels.push(m1[0]);
					 }
				  break;
				  case "v" : 
					  var dataset7:dataset = <dataset> {};
					  if (value[0] < start_time) {
					   start_time = value[0]; 
					  }
					  temp_dt.setUTCSeconds((value[0]*60) + epoch_margin_time);
					  co_ord1.x = temp_dt;
					  co_ord1.y = 0;
					  let colors = this.colors(index);
					  dataset7.data = [];
					  dataset7.fill = false;
					  dataset7.type = "line";
					  dataset7.backgroundColor = colors[0];
					  dataset7.borderColor = colors[1];
					  dataset7.pointHoverRadius =  5;
					  dataset7.pointHoverBackgroundColor =  "rgba(75,192,192,1)";
					  dataset7.pointHoverBorderColor =  "rgba(220,220,220,1)";
					  dataset7.pointHoverBorderWidth =  2;
					  dataset7.spanGaps = false;
					  index++;
					  dataset7.borderWidth = 1;
					  dataset7.data.push(co_ord1);
					  dataset7.label = m1[0];
					  co_ord2.x = dt;
					  if (value[1] == 1) {
						vlabels.unshift(m1[0]);
						let calc_value = (value[2] * value[5]) + value[4]
						co_ord2.y = calc_value;
						dataset7.data.unshift(co_ord2);
						dataset1.data.unshift(calc_value);
						dataset2.data.unshift(value[5]);
						dataset5.data.unshift(value[4]);
						dataset6.data.unshift(value[5]);
						dataset3.data.unshift(value[2]);
						dataset4.data.unshift(value[3]);
					  } else {
						vlabels.push(m1[0]);
						let calc_value = (value[2] * value[4]); 
						dataset1.data.push(calc_value);
						dataset2.data.push(value[4]);
						dataset6.data.push(value[4]);
						dataset3.data.push(value[2]);
						dataset4.data.push(value[3]);
						dataset5.data.push(0);
						co_ord2.y = calc_value;
						dataset7.data.push(co_ord2);
					  }
					  data2.datasets.push(dataset7);
				  break;
			  }
		  }
	  }
	  var start_dt = new Date(0);
	  start_dt.setUTCSeconds(start_time * 60 + epoch_margin_time);

	  data.labels = [];
	//data.labels.push(...labels);
	  data.labels.push(...vlabels);
	  data.datasets = [dataset2, dataset1];
	   

	  this.barChart = new Chart(this.barCanvas.nativeElement, {
		type: "bar",
		data: data,
			options : {
			maintainAspectRatio: true,
			padding : {
			  top : 50,
			  bottom : 20
			},
			tooltips : {
			  intersect: false,
			  mode: 'index',
			  enabled : true
			},
			legend: {
				display: false,
				fullwidth: true
			},
				title : {
					display :true,
					text : "Switching Status"
				},
				scales: {
						xAxes: [{
								display: true,
								scaleLabel: {
									display: true,
									labelString: 'Currents'
								},
						        stacked:true,
								barPercentage: 0.5,
								barThickness: 6,
								maxBarThickness: 8,
								minBarLength: 2,
								gridLines: {
										offsetGridLines: true
								}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Minutes/Amps'
							},
							ticks: {
						        stacked:true
							  //suggestedMin: 0,
							  //	suggestedMax: 100
							}
						}]
				}
			}
	  });

	  data3.labels = [];
	  data3.labels.push(...vlabels);
	  data3.datasets = [dataset3, dataset4];

	  this.barChart1 = new Chart(this.barCanvas1.nativeElement, {
		type: "bar",
		data: data3,
			options : {
			maintainAspectRatio: true,
			tooltips : {
			  intersect: false,
			  mode: 'index',
			  enabled : true
			},
			legend: {
				display: false,
				fullwidth: true
			},
				title : {
					display :true,
					text : "Switching History"
				},
				scales: {
						xAxes: [{
								display: true,
								scaleLabel: {
									display: true,
									labelString: 'Currents'
								},
						        stacked:false,
								barPercentage: 0.5,
								barThickness: 6,
								maxBarThickness: 8,
								minBarLength: 2,
								gridLines: {
										offsetGridLines: true
								}
						}],
						yAxes: [{
							scaleLabel: {
								display: true,
								labelString: 'Cycles'
							},
							ticks: {
						        stacked:false
							  //suggestedMin: 0,
							  //	suggestedMax: 100
							}
						}]
				}
			}
	  });

	//data1.labels = [];
	// data1.labels.push(...labels);
	// data1.labels.push(...vlabels);
	  dataset8.backgroundColor = bgndColor[1];
	  dataset9.backgroundColor = bgndColor;
	  dataset8.hoverBackgroundColor = hoverBackgroundColor[1];
	  dataset9.hoverBackgroundColor = hoverBackgroundColor;
	  dataset1.labels = vlabels;
	  data1.datasets = [dataset1, dataset9, dataset8];
	  
	  this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
		type: "doughnut",
			  tooltips : {
				intersect: false,
				mode: 'index',
				enabled : true
			  },
			  data: data1,
			  options : {
				  legend: {
					  display: false,
					  fullwidth: false
				  },
				  title : {
					  display :true,
					  position :"top",
					text : "Run time"
				  },
				  tooltips: {
					callbacks: {
					  label: function(tooltipItem, data) {
						var dataset = data.datasets[tooltipItem.datasetIndex];
						var index = tooltipItem.index;
						return dataset.labels[index] + ': ' + dataset.data[index];
					  }
				  }
				}
			  }
	  });

	  this.lineChart = new Chart(this.lineCanvas.nativeElement, {
		type: "line",
		data: data2, 
		/*
		{
		  //labels: ["January", "February", "March", "April", "May", "June", "July"],
		  datasets: [
			{
			  label: "My First dataset",
			  fill: false,
			  lineTension: 0.1,
			  backgroundColor: "rgba(75,192,192,0.4)",
			  borderColor: "rgba(75,192,192,1)",
			  borderCapStyle: "butt",
			  borderDash: [],
			  borderDashOffset: 0.0,
			  borderJoinStyle: "miter",
			  pointBorderColor: "rgba(75,192,192,1)",
			  pointBackgroundColor: "#fff",
			  pointBorderWidth: 1,
			  pointHoverRadius: 5,
			  pointHoverBackgroundColor: "rgba(75,192,192,1)",
			  pointHoverBorderColor: "rgba(220,220,220,1)",
			  pointHoverBorderWidth: 2,
			  pointRadius: 1,
			  pointHitRadius: 10,
			  data: dataset7.data,
			  spanGaps: false
			}
		  ]
		},
		 */
		options: {
			responsive: true,
			padding : {
			  bottom : 50
			},
			legend: {
				display: false,
				fullwidth: true
			},
			title: {
				display: true,
				text: 'Current Switching'
			},
			scales: {
				xAxes: [{
					type: 'time',
					time: {
					  minUnit : 'hour',
					  unit : 'week',
					  unitStepSize : 1,
					  displayFormats : {
						hour : "D h",
						day : "MMM D",
						week : "ll"
					  }
					},
					display: true,
					distribution: 'series',
					bounds: 'ticks',
					scaleLabel: {
						display: true,
						labelString: 'Date'
					},
					ticks: {
						source : 'data',
						autoskip : false,
						major: {
							fontStyle: 'bold',
							fontColor: '#FF0000'
						}
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Minutes/Amps'
					}
				}]
			},
			tooltips: {
				intersect: false,
				mode: 'index',
				callbacks: {
					label: function(tooltipItem, myData) {
						var label = myData.datasets[tooltipItem.datasetIndex].label || '';
						if (label) {
							label += ': ';
						}
						label += parseFloat(tooltipItem.value).toFixed(2);
						return label;
					}
				}
			}
		}
	  });
	}

    colors(index:number) {
	  var bgndColor =  [
			"rgba(255, 99, 132, 0.2)",
			"rgba(54, 162, 235, 0.2)",
			"rgba(255, 206, 86, 0.2)",
			"rgba(75, 192, 192, 0.2)",
			"rgba(153, 102, 255, 0.2)",
			"rgba(255, 159, 64, 0.2)"
		  ];

	  var borderColor = [
			"rgba(255,99,132,1)",
			"rgba(54, 162, 235, 1)",
			"rgba(255, 206, 86, 1)",
			"rgba(75, 192, 192, 1)",
			"rgba(153, 102, 255, 1)",
			"rgba(255, 159, 64, 1)"
		  ];
	  index = index % 6;
	  return [bgndColor[index], borderColor[index]];
	}

	close() {
	  this.modalCtrl.dismiss(null);
	}

	async presentToast(header:string, msg:any) {
		const toast = await this.toastCtrl.create({
		  message: header + '\n' + msg,
		  color:"dark",
		  cssClass:"toast",
		  showCloseButton: true,
		  translucent: true,
		  duration : 60000,
		  position : 'bottom'
		});
		toast.present();
	}

	// Test code - Not used
	setup_charts() {
		this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: "bar",
      data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [
          {
            label: "# of Votes",
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)"
            ],
            borderColor: [
              "rgba(255,99,132,1)",
              "rgba(54, 162, 235, 1)",
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
              "rgba(255, 159, 64, 1)"
            ],
            borderWidth: 1
          }
        ]
      },
			options : {
				scales: {
						xAxes: [{
								barPercentage: 0.5,
								barThickness: 6,
								maxBarThickness: 8,
								minBarLength: 2,
								gridLines: {
										offsetGridLines: true
								}
						}],
          yAxes: [
            {
              ticks: {
                beginAtZero: true
              }
            }
          ]
        }
      }
    });

    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: "doughnut",
      data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [
          {
            label: "# of Votes",
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)"
            ],
            hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56"]
          }
        ]
      }
    });

    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: "line",
      data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
          {
            label: "My First dataset",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [65, 59, 80, 81, 56, 55, 40],
            spanGaps: false
          }
        ]
      }
    });
  }
}

@NgModule({
   imports: [IonicModule,CommonModule ],
   exports: [ShowdataComponent],
   declarations: [ShowdataComponent],
   providers: [],
})

export class ShowdataComponentModule {
}
