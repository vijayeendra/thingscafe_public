//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package utils

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"plugin"
	"sort"
	"strconv"
	"strings"

	"github.com/gorilla/schema"
	"gitlab.com/service-platform/tc-mw/tc-client"
)

// "name":"MWS","mtype":"Water Supply","wsource":"River","capacity":"5","wd":"1000","dryrun":true,"cmeasure":true,"vmonitor":true,"tank":"Tank1","monsystem":"Plus System","vmin":"200","vmax":"450","submit":""}

var seq_num int16 = 0
var saas_asset_types_map map[Saas_entity_name]*[]tcclient.Asset_type_info
var saas_entity_name string
var controller_options_map map[string]interface{}

type Controller_response struct {
	Cid     string   `json:"id"`
	Name    string   `json:"name"`
	Desc    string   `json:"desc"`
	Mobile  string   `json:"mobile"`
	Cmds    []string `json:"cmds"`
	Replies []string `json:"replies"`
	Status  int      `json:"status"`
}

type Config_response struct {
	Statustext string                `json:"status"`
	Response   []Controller_response `json:"response"`
}

type Collection_list struct {
	Asset_type      string   `json:"asset_type"`
	Asset_type_name string   `json:"asset_type_name"`
	Chosen_list     []string `json:"chosen_list"`
}
type Xport_params struct {
	Xport_type        string
	Xport_max_len     int
	Xport_max_timeout int
	Encode            bool
}

type Controller_state struct {
	Id                   string   `json:"id"`
	Name                 string   `json:"name"`
	Site                 string   `json:"site"`
	Initialized          bool     `json:"initialized"`
	Configured           bool     `json:"configured"`
	Sms_config           []string `json:"sms_config"`
	Sms_user_config      []string `json:"sms_user_config"`
	Options              string   `json:"options"`
	Mobile               string   `json:"mobile"`
	Last_sync_time       int64    `json:"last_sync_time"`
	Last_usersync_time   int64    `json:"last_usersync_time"`
	Progress_flag        string   `json:"progress_flag"`
	Last_config_pos      int      `json:"last_config_position"`
	Last_user_config_pos int      `json:"last_user_config_position"`
}

type apply_data struct {
	Site_id         string `json:"site_id"`
	Site_name       string `json:"site_name"`
	Asset_id        string `json:"asset_id"`
	Asset_name      string `json:"asset_name"`
	Parent_asset_id string `json:"parent_asset_id"`
	Type_name       string `json:"type_name"`
	Type_id         string `json:"type_id"`
}

type Plugin_env struct {
	User           string
	Saas           string
	Password       string
	Root_entity_id string
}

func Contains(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}

	_, ok := set[item]
	return ok
}

func Merge(elements1 []string, elements2 []string, num int) []string {
	// Use map to record duplicates as we find them.
	encountered := map[string]string{}
	result := []string{}

	/*
		if len(elements1) == 0 {
			return string([]byte(elements2))
		}
	*/

	for v := range elements2 {
		cmd := elements2[v]
		if num > 0 {
			k := strings.Split(cmd, " ")
			cmd = strings.Join(k[:num], " ")
		}
		encountered[cmd] = elements2[v]
	}

	for v := range elements1 {
		cmd := elements1[v]
		if num > 0 {
			k := strings.Split(cmd, " ")
			cmd = strings.Join(k[:num], " ")
		}
		if org, ok := encountered[cmd]; ok {
			// Push element from the new array
			delete(encountered, cmd)
			result = append(result, org)
		} else {
			// Record this element as an encountered element.
			// Append to result slice.
			result = append(result, elements1[v])
		}
	}

	for v := range elements2 {
		cmd := elements2[v]
		//cmd := elements2[v]
		if num > 0 {
			k := strings.Split(cmd, " ")
			cmd = strings.Join(k[:num], " ")
		}
		if org, ok := encountered[cmd]; ok {
			result = append(result, org)
		}
	}

	// Return the new slice.
	return result
}

func Append_unique(elements []string, elem string) []string {
	// Use map to record duplicates as we find them.

	encountered := map[string]bool{}

	for v := range elements {
		cmd := elements[v]
		encountered[cmd] = true
	}

	// Return the new slice.
	if encountered[elem] == false {
		elements = append(elements, elem)
	}
	return elements
}

func Remove_duplicates(elements []string, num int) []string {
	// Use map to record duplicates as we find them.
	encountered := map[string]bool{}
	result := []string{}

	for v := range elements {
		cmd := elements[v]
		if num > 0 {
			k := strings.Split(cmd, " ")
			cmd = strings.Join(k[:num], " ")
		}
		if encountered[cmd] == true {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[cmd] = true
			// Append to result slice.
			result = append(result, elements[v])
		}
	}
	// Return the new slice.
	return result
}

func Sort_by_asset_name(alist map[string]interface{}) PairList {

	pl := make(PairList, len(alist))
	i := 0
	for k, v := range alist {
		data := v.(string)
		adata := new(apply_data)
		err := json.Unmarshal([]byte(data), adata)
		if err != nil {
			return nil
		}
		pl[i] = Pair{k, adata}
		i++
	}
	sort.Sort(pl)
	return pl
}

type Pair struct {
	Key   string
	Value *apply_data
}

type PairList []Pair

func (p PairList) Len() int { return len(p) }

// Get env var or default
func Getenv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func (p PairList) Less(i, j int) bool {
	num1 := GetNumber(p[i].Value.Asset_name)
	num2 := GetNumber(p[j].Value.Asset_name)

	return num1 < num2
}

func GetNumber(str string) int {
	value := strings.IndexAny(str, "0123456789")
	if value >= 0 && value <= len(str) {
		value, _ = strconv.Atoi(str[value:])
		return value
	}
	return 1000
}

/*
func (p PairList) Less(i, j int) bool {
	pattern, err := regexp.Compile("/(d+)/g")
	if err != nil {
		panic(err)
	}
	num1 := pattern.Find(p[i].Value.Asset_name)
	num2 := pattern.Find(p[j].Value.Asset_name)

	return num1 < num2
}
*/

func (p PairList) Swap(i, j int) { p[i], p[j] = p[j], p[i] }

type Saas_entity_name string

func (s Saas_entity_name) Get_list_asset_types(handle interface{}, root_entity_id string) {
	atypes := tcclient.Get_list_asset_types(handle, root_entity_id)
	if len(saas_asset_types_map) == 0 {
		saas_asset_types_map = make(map[Saas_entity_name]*[]tcclient.Asset_type_info)
	}
	saas_asset_types_map[s] = atypes
}

func (s Saas_entity_name) Find_asset_type(asset_type_name string) *tcclient.Asset_type_info {

	var atypes *[]tcclient.Asset_type_info
	var ok bool
	if atypes, ok = saas_asset_types_map[s]; !ok {
		log.Print("No assets types for Saas entity", saas_entity_name)
		return nil
	}

	for _, lasset := range *atypes {
		if lasset.Name == asset_type_name {
			return &lasset
		}
	}
	return nil
}

func (s Saas_entity_name) Find_asset_type_by_id(asset_type_id string) *tcclient.Asset_type_info {
	var atypes *[]tcclient.Asset_type_info
	var ok bool

	if atypes, ok = saas_asset_types_map[s]; !ok {
		log.Print("No assets types for Saas entity", saas_entity_name)
		return nil
	}

	for _, lasset := range *atypes {
		if lasset.Id == asset_type_id {
			return &lasset
		}
	}
	log.Print("Couldn't find asset_type", saas_entity_name, ":"+asset_type_id)
	return nil
}

func Find_profile(plist *[]tcclient.Config_profile, profile_name string) *tcclient.Config_profile {
	for _, profile := range *plist {
		if profile.Name == profile_name {
			return &profile
		}
	}
	return nil
}

func Get_asset_collection(ainfo *tcclient.Asset_info) (bool, []string) {
	var asset_list Collection_list
	var pconfig map[string]string

	if ainfo.Collmap != "" {
		err := json.Unmarshal([]byte(ainfo.Collmap), &pconfig)
		if err != nil {
			log.Print("Collection Map format Error:", ainfo.Collmap)
		} else {
			for _, cinfo := range pconfig {
				asset_list.Chosen_list = make([]string, 32)
				err = json.Unmarshal([]byte(cinfo), &asset_list)
				if err != nil {
					log.Print("Config format Error:", cinfo)
				} else {
					return true, asset_list.Chosen_list
				}
			}
		}
	}
	return false, []string{}
}

func Find_asset_collection(aname string, asset_type_id string, alist *[]tcclient.Asset_info, collection_type_id string) (bool, []tcclient.Asset_info, []string) {
	var found bool = false
	var pconfig map[string]string
	var asset_list Collection_list
	//var ainfo *tcclient.Asset_info
	var pinfo []tcclient.Asset_info = nil

	for _, asset := range *alist {
		if asset.Name == aname || asset.Asset_type != collection_type_id {
			continue
		}

		if asset.Collmap != "" {
			err := json.Unmarshal([]byte(asset.Collmap), &pconfig)
			if err != nil {
				log.Print("Collection Map format Error:", asset.Collmap)
			} else {
				for _, cinfo := range pconfig {
					asset_list.Chosen_list = make([]string, 32)
					err := json.Unmarshal([]byte(cinfo), &asset_list)
					if err != nil {
						log.Print("Config format Error:", cinfo)
					} else {
						if asset_list.Asset_type != asset_type_id {
							continue
						}
						for _, casset := range asset_list.Chosen_list {
							if casset == aname {
								pinfo = append(pinfo, asset)
								found = true
								//fmt.Print(" Collection :", ainfo.Name, ":", asset.Name)
								return found, pinfo, asset_list.Chosen_list
							}
						}
					}
				}
			}
		}
	}

	return found, pinfo, asset_list.Chosen_list
}

func Find_asset(asset_id string, alist *[]tcclient.Asset_info) (bool, *tcclient.Asset_info) {
	var found bool = false
	var ainfo *tcclient.Asset_info

	for _, lasset := range *alist {
		if lasset.Id == asset_id {
			found = true
			ainfo = &lasset
			break
		}
	}
	return found, ainfo
}

func Find_asset_by_name(aname string, alist *[]tcclient.Asset_info) (bool, *tcclient.Asset_info) {
	var found bool = false
	var ainfo *tcclient.Asset_info

	for _, lasset := range *alist {
		if lasset.Name == aname {
			found = true
			ainfo = &lasset
			break
		}
	}
	return found, ainfo
}

func Load_plugin(mod string, function string) (interface{}, error) {

	//	mod := plugin_dir + "/" + plug + ".so"
	plugmod, err := plugin.Open(mod)
	if err != nil {
		fmt.Println("Cannot load plugin:", err)
		return nil, err
	}

	// 2. look up a symbol (an exported function or variable)
	// in this case, variable Greeter
	procsym, err := plugmod.Lookup(function)
	if err != nil {
		fmt.Printf("Cannot find %s symbol in plugin:\n", function, err)
		return nil, err
	}

	/*
		// 3. Assert that loaded symbol is of a desired type
		proc, ok := procsym.(t)
		if !ok {
			fmt.Println("unexpected type from module symbol")
			return nil, err
		}
	*/
	return procsym, nil
}

func Get_query_params(r *http.Request, qparams interface{}) error {
	err := r.ParseForm()
	if err != nil {
		log.Println("Received Message Format error", err.Error(), r.URL, r.Form, r.PostForm)
		return err
	} else {
		log.Println("Received Message :", r.URL, r.Form)
	}

	var decoder = schema.NewDecoder()
	err = decoder.Decode(qparams, r.Form)

	if err != nil {
		log.Println("Received Message Decoding Error", err.Error(), r.URL, r.Form, r.PostForm)
		return err
	}
	return nil
}

func Collect_url(app string, ename string) (string, string) {
	curl := app + "_THINGSCAFE_COLLECT_URL"
	url := Getenv(curl, "live2.thingscafe.net/tccollect/v1/submit-data?")
	url1 := "app=" + app + "&ename=" + ename + "&cid="
	return url, url1
}

func Set_controller_options(id string, coptions interface{}) {

	controller_options_map[id] = coptions
}

func Controller_options(id string) interface{} {
	return (controller_options_map[id])
}

func Controller_options_map() map[string]interface{} {
	return (controller_options_map)
}

type Query_string struct {
	App string `schema:"app"`
	Cid string `schema:"cid"`
}

func init() {
	controller_options_map = make(map[string]interface{})
}
