"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var throw_1 = require("rxjs/observable/throw");
var operators_1 = require("rxjs/operators");
var InterceptorService = /** @class */ (function () {
    function InterceptorService(storage, alertCtrl, tokensvc) {
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.tokensvc = tokensvc;
    }
    // Intercepts all HTTP requests!
    InterceptorService.prototype.intercept = function (request, next) {
        var _this = this;
        var tokenObservable = this.tokensvc.getTokenAsObservable()
            .pipe(map(function (token) { return ({
            let: let, clonedReq: clonedReq
        }); }));
        ;
        return tokenObservable.pipe(flatMap(function (req) { return next.handle(req); }), pipe(operators_1.catchError(function (error) {
            // Perhaps display an error for specific status codes here already?
            var msg = error.message;
            var alert = _this.alertCtrl.create({
                title: error.name,
                message: msg,
                buttons: ['OK']
            });
            alert.present();
            // Pass the error to the caller of the function
            return throw_1._throw(error);
        })));
        ;
    };
    // Adds the token to your headers if it exists
    InterceptorService.prototype.addToken = function (request, token) {
        if (token) {
            var clone = void 0;
            clone = request.clone({
                setHeaders: {
                    Accept: "application/json",
                    'Content-Type': "application/json",
                    Authorization: "Bearer " + token
                }
            });
            return clone;
        }
        return request;
    };
    InterceptorService = __decorate([
        core_1.Injectable()
    ], InterceptorService);
    return InterceptorService;
}());
exports.InterceptorService = InterceptorService;
