#!/bin/bash

# This script can be used to start docker containers on a new
# VM which has been cloned from existing node or started from an
# existing snapshot which already has the docker images required

# Start DB node
echo "starting the dbnode"
./dbnode_run.sh dbnode "9042/tcp -p 9160/tcp" vkumarbs/thingscafe:tc-cassandra
docker ps

# Make sure dbnode has IP address 172.17.0.3
docker exec -it dbnode hostname -i

# Start the first tcnode
echo "Starting the first tcnode"
./tcnode_run.sh tcnode11 "8000:8000/tcp -p 8001:8001/tcp -p 8002:8002/tcp -p 8003:8003/tcp -p 8004:8004/tcp" vkumarbs/thingscafe:tc-ubuntu &
# Start the second tcnode
echo "Starting the second tcnode"
 ./tcnode_run.sh tcnode12 "8080:8080/tcp -p 8081:8081/tcp -p 8082:8082/tcp -p 8083:8083/tcp -p 8084:8084/tcp" vkumarbs/thingscafe:tc-ubuntu &
sleep 10
docker ps -a
echo "If both containers are up, kill the docker run commands explicitly"

