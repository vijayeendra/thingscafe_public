//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { Component, OnInit } from '@angular/core';
import { MenuService } from '../services/menu.service';
import { AlertController, NavController, MenuController} from '@ionic/angular';
import { GlobalService } from '../services/global.service';

export interface Menu_item {
  title: string;
  url: string;
  check: any;
  icon: string;
  action: string;
}

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

	constructor(public menusvc:MenuService,
	           public global:GlobalService,
			   private navCtrl:NavController) {
	}

	ngOnInit() {
		this.global.ready()
		.subscribe(ready => {
			if (!ready) {
				return;
			}
			console.log("Menu Component ready");
		});
	}

	public navigate(p:Menu_item) {
		if (p.action != "") {
			if (p.url == "") {
				console.log("Performing action ", p.action);
				if (this.global[p.action]) {
					this.global[p.action]();
				}
			} else {
				this.navCtrl.navigateForward([p.url, { action: p.action}]);
			}
		} else if (p.url != "") {
			console.log("Navigating to ", p.url);
			this.navCtrl.navigateForward([p.url, { action: p.action}]);
		}
	}

}
