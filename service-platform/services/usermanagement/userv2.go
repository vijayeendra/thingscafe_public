//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package usermanagement

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.com/service-platform/common"
	objsvc "gitlab.com/service-platform/services/objectmanagement"
)

var default_user_query_options common.Query_options
var default_usercred_query_options common.Query_options

//
type User_info struct {
	Email            string                 `json:"email"`
	Saas_entity_name string                 `json:"saas_entity_name"`
	Entity_id        gocql.UUID             `json:"entity_id"`
	Id               gocql.UUID             `json:"id"`
	Last_name        string                 `json:"last_name"`
	First_name       string                 `json:"first_name"`
	Kv               map[string]interface{} `json:"kv"`
	Role             common.User_role       `json:"role"`
	Tags             string                 `json:"tags"`
}

type User_cred struct {
	Id       gocql.UUID             `json:"id"`
	Kv       map[string]interface{} `json:"kv"`
	Password common.Password        `json:"password"`
}

func (e *User_cred) SetEntityId(id gocql.UUID) {
}

func (e *User_cred) GetEntityId() gocql.UUID {
	return gocql.UUID{}
}

func (e *User_cred) SetId(id gocql.UUID) {
	e.Id = id
}

func (e *User_cred) GetId() gocql.UUID {
	return (e.Id)
}

func (e *User_info) SetEntityId(id gocql.UUID) {
	e.Entity_id = id
}

func (e *User_info) GetEntityId() gocql.UUID {
	return (e.Entity_id)
}

func (e *User_info) SetId(id gocql.UUID) {
	e.Id = id
}

func (e *User_info) GetId() gocql.UUID {
	return (e.Id)
}

var Create_user_credentials = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	var object User_cred
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_usercred_query_options
	common.Setup_qoptions(r, &qoptions)

	/* TBD - Add a check that only SAASPROVIDER and SERVICEPROVIDER can create user credentials */
	obj.Init("user_credentials", &qoptions)

	if err := svc.Create_object_svc(w, r, &obj, &object); err != nil {
		fmt.Println(err)
	}
	return
})

var Create_user = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	var object User_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_user_query_options
	common.Setup_qoptions(r, &qoptions)
	/* TBD - Add a check that only SAASPROVIDER and SERVICEPROVIDER can create users */
	obj.Init("users", &qoptions)

	if err := svc.Create_object_svc(w, r, &obj, &object); err != nil {
		fmt.Println(err)
	}
	return
})

var List_users = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj User_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_user_query_options
	common.Setup_qoptions(r, &qoptions)

	if strings.Contains(r.URL.Path, "get-user") {
		qoptions.Where["id"] = qoptions.Where["user_id"]
	}
	delete(qoptions.Where, "user_id")
	obj.Init("users", &qoptions)

	if err := svc.List_objects_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
})

var Find_user = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj User_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_user_query_options
	common.Setup_qoptions(r, &qoptions)
	obj.Init("user", &qoptions)
	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")

	if err := svc.Get_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
	return
})

var Get_user = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj User_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_user_query_options
	common.Setup_qoptions(r, &qoptions)
	obj.Init("user", &qoptions)

	if err := svc.Get_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
	return
})

var Delete_user = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj User_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	obj.Init("users", &qoptions)
	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")

	if err := svc.Delete_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
		http.NotFound(w, r)
		return
	}
	return
})

var Delete_user_credentials = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj User_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	obj.Init("user_credentials", &qoptions)
	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")

	if err := svc.Delete_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
		http.NotFound(w, r)
		return
	}
	return
})

func SetRoutes(router *mux.Router) *mux.Router {
	s := router.PathPrefix("/users").Subrouter()
	s.HandleFunc("/", List_users).Methods("GET")
	s.HandleFunc("/list-users", List_users).Methods("GET")
	s.HandleFunc("/get-user", Get_user).Methods("POST")
	s.HandleFunc("/find-user", Find_user).Methods("POST")
	s.HandleFunc("/get-user", List_users).Methods("GET")
	s.HandleFunc("/delete-user", Delete_user).Methods("POST")
	s.HandleFunc("/create-user", Create_user).Methods("POST")
	s.HandleFunc("/modify-user", Create_user).Methods("POST")
	s.HandleFunc("/create-user-credentials", Create_user_credentials).Methods("POST")
	s.HandleFunc("/modify-user-credentials", Create_user_credentials).Methods("POST")
	s.HandleFunc("/delete-user-credentials", Delete_user_credentials).Methods("POST")
	/*
		router.HandleFunc("/token-auth", controllers.Login).Methods("POST")
		router.Handle("/refresh-token-auth", negroni.New(negroni.HandlerFunc(controllers.RefreshToken))).Methods("GET")
		router.Handle("/logout", negroni.New(negroni.HandlerFunc(authentication.RequireTokenAuthentication), negroni.HandlerFunc(controllers.Logout))).Methods("GET")
	*/
	return router
}

func init() {
	/*
		Where           map[string]interface{} `json:"where"`
		Groupby         string                 `json:"groupby"`
		Orderby         map[string]string      `json:"orderby"`
		Limit           int                    `json:"limit"`
		Allow_filtering bool                   `json:"allow_filtering"`
		Aggregate       map[string]interface{} `json:"aggregate"`
	*/
	default_user_query_options = common.Query_options{
		Limit: 10, Allow_filtering: true}

	default_usercred_query_options = common.Query_options{
		Limit: 10, Allow_filtering: true}
}
