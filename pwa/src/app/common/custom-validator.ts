//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { AbstractControl, ReactiveFormsModule, FormArray, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import {PhoneValidator} from '../common/phone-validator';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CustomValidator {

	validation_messages = {
		'name': [
			{ type: 'required', message: 'Input is required' },
			{ type: 'minlength', message: 'Text length too small' },
			{ type: 'maxlength', message: 'Text length exceeds maximum ' },
		],
		'select': [
			{ type: 'required', message: 'Please select an option' },
		],
		'datetime': [
			{ type: 'required', message: 'Input is required' },
		],
		'email': [
			{ type: 'required', message: 'Email is required' },
			{ type: 'email', message: 'Enter a valid email' }
		],
		'mobile': [
			{ type: 'required', message: 'Mobile number is required' },
			{ type: 'validCountryPhone', message: 'Enter a valid mobile number' }
		],
		'phone': [
			{ type: 'required', message: 'Phone is required' },
			{ type: 'validCountryPhone', message: 'Enter a valid phone number' }
		],
		'phones': [
			{ type: 'required', message: 'Mobile number is required' },
			{ type: 'validCountryPhone', message: 'Enter a valid mobile number' }
		],
		'domain': [
			{ type: 'required', message: 'Input is required' },
			{ type: 'domain', message: 'Domain should be of form xxx.yyy' }
		],
		'password': [
			{ type: 'required', message: 'Password is required' },
			{ type: 'minlength', message: 'Password must be at least 5 characters long' },
			{ type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number' }
		]
	}

	constructor( public phonevalidator: PhoneValidator,
	) { }

	public create_validations(etype:any, field:string):any[]{
		var validation_list:any[] = [];

		if (etype <=3) {
			switch (field) {
			  case "email" :
					validation_list.push(Validators.email);
					validation_list.push(Validators.required);
					break;
			  case "name" :
					validation_list.push(Validators.minLength(3));
					validation_list.push(Validators.maxLength(10));
					validation_list.push(Validators.required);
					break;
			  case "domain" :
					validation_list.push(Validators.required);
					validation_list.push(this.domainValidator);
					break;
			}
		} else {
			if (field == "name") {
					validation_list.push(Validators.minLength(2));
					validation_list.push(Validators.maxLength(10));
					validation_list.push(Validators.required);
			}
		}

		switch (field) {
			case "requiredList" :
				validation_list.push(this.requiredList);
				break;

			case "phone" :
			case "phones" :
					validation_list.push(this.phonevalidator.validCountryPhone("IN"));
				//		validation_list.push(Validators.required);
					break;
			case "mobile" :
					validation_list.push(Validators.required);
					validation_list.push(this.phonevalidator.validCountryPhone("IN"));
					break;
			case "address" :
			case "address2" :
					validation_list.push(Validators.minLength(3));
					validation_list.push(Validators.maxLength(20));
					validation_list.push(Validators.required);
					break;
			case "city" :
					validation_list.push(Validators.minLength(3));
					validation_list.push(Validators.maxLength(20));
					validation_list.push(Validators.required);
					break;
			case "country" :
					validation_list.push(Validators.minLength(5));
					validation_list.push(Validators.maxLength(20));
					validation_list.push(Validators.required);
					break;
		}
		return (validation_list);
	}

	domainValidator(control: AbstractControl):{[key: string]: boolean} {
		//var EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const DOMAIN_REGEXP = /^[a-zA-Z0-9][-a-zA-Z0-9]+[a-zA-Z0-9].[a-z]{2,3}(.[a-z]{2,3})?(.[a-z]{2,3})?$/;
		if (control.value != null && control.value != "" && (control.value.length <= 4 || !DOMAIN_REGEXP.test(control.value))) {
			return {domain:true};
		}
		return null;
	}

	requiredList(control:AbstractControl) {
	  if (control.value != null && control.value.length > 0 && (control.value[0] != ""))  {
			return null;
	  }
	  return {
		requiredList: {
		  valid: false
		}
	  }
	}
}
