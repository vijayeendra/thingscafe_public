ThingsCafe Service Platform web service
The subdirectories here implement the ThingsCafe service platform functionality.

Dev environment setup for service_platform
1. Install Go environment. Check the following links on how to setup Go
	
	https://golang.org/doc/install


2.	https://www.digitalocean.com/community/tutorials/how-to-install-go-on-ubuntu-18-04 

3.  Clone the thingscafe_public repository in your home directory
    git clone git@gitlab.com:thingscafe_public/thingscafe_public.git

4.  Create a symbolic link or copy the contents of the service-platform folder to $GOPATH/src/gitlab.com

	mkdir -p $GOPATH/src/gitlab.com
	ln -s ~/thingscafe_public/service-platform $GOPATH/src/gitlab.com/service-platform

5. To build the code, change directory to $GOPATH/src/gitlab.com/service_platform/build
   5a. Run ./build.sh prod (for production code) > tc_sp_build.log
   5b. Check for errors manually within the tc_sp_build.log

   If there are errors saying that sarama.v2 NewClusterAdminfromClient not found, just run the following patch
   command to patch and rerun build.sh

	cd $GOPATH/src/gopkg.in/Shopify/sarama.v2 
	patch -p0 -i $GOPATH/src/gitlab.com/service-platform/patches/saramav2.patch

   TBD - Automating the build will need to change the build.sh and other Makefiles to check for errors

6. Binaries and Release file are created in build/production. File name will be of the form 

	build/production/tcafe_sp_build.tar.gz_cf70368-061820

	where the last two entries indicate last git commit-id and date of build

7. Run build/build.sh to build the binaries and plugins for local testing
