# !/bin/bash
# Use this script to create the SAAS entity and admin user with
# default password
# This will allow the admin to use REST APIs to create child 
# entities, asset-types, config-schemas

if  [ $# -lt 5 ]
then
	echo "Usage : $0 <saas provider name> <domain name> <email of admin> <firstname of admin> <lastname of admin>"
	echo "Not enough arguments"
	exit -1
fi

SAASP=$1
domain=$2
email=$3
firstname=$4
lastname=$5


timeout=30
schema_file=../schema/$SAASP.schema
template_schema_file=../schema/saasp_template.schema

logger -i "Creating Keyspace for SAAS provider: $SAASP"

sed "s/saasv/$SAASP/g" $template_schema_file > $schema_file
# Remove existing keyspace if it exists
echo "DROP KEYSPACE $SAASP;" | cqlsh --request-timeout=$timeout `hostname -i`
# Create schema with new keyspace for new SAAS provider
#echo "Check $schema_file and hit enter"
#read a
cqlsh --request-timeout=$timeout -f  $schema_file `hostname -i`
error=$?
if [ $error -ne 0 ] && [ $error -ne 2 ]
then
	logger -i "Creating Keyspace for $SAASP failed"
	exit -1
fi
logger -i "Succeeded in creating Keyspace for SAAS provider: $SAASP"
#echo "Press Enter to continue"
#read a

root_entity_id=a63be430-b459-11ea-b3de-0242ac130004

# Create SAAS entity under the Keyspace. Rest can be
# achieved through REST APIs
insert_cmd=`./create_entity.sh $SAASP $root_entity_id $domain $email $firstname $lastname;`
echo $insert_cmd 
echo "Created SAAS entity $SAASP"
exit 0
