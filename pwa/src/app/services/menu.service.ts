//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { Injectable } from '@angular/core';
import { MenuController } from '@ionic/angular'; 
import { ReplaySubject } from 'rxjs';
import { Menu_item } from '../menu/menu.component';

@Injectable(
)

export class MenuService {

  public title: ReplaySubject<Menu_item[]> = new ReplaySubject<Menu_item[]>(1);
  public details: ReplaySubject<Menu_item[]> = new ReplaySubject<Menu_item[]>(1);

	constructor(public menu: MenuController) {
		this.menu.enable(true, 'mainmenu');
		this.menu.open('mainmenu');
	}

  public toggle() {
    this.menu.toggle('main-menu');
  }
}
