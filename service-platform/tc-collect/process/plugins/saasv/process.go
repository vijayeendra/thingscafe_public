//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	cooked "gitlab.com/service-platform/common/cooked"
	helper "gitlab.com/service-platform/common/helper"
	sarama "gopkg.in/Shopify/sarama.v2"
)

type vendor string

var Processor vendor

var threshold_map map[string]cooked.Thresholds

func (p vendor) Init() interface{} {
	threshold_map = make(map[string]cooked.Thresholds)
	// Set thresholds here
	thres := new(cooked.Thresholds)
	thres.Min = 15
	thres.Max = 40
	thres.Number_of_times = 3
	thres.Period = time.Hour * 24
	thres.Aggregate_type = "avg"
	threshold_map["voltage"] = *thres
	return threshold_map
}

func (p vendor) Parse(msg *sarama.ConsumerMessage) (interface{}, error) {

	var kv map[string]interface{}
	datar, err := parse_data(msg.Value)

	if err != nil {
		log.Println("Ignoring message. Parse error:", err)
		return nil, err
	}

	if len(datar.Data) <= 0 {
		err = errors.New("No useful info")
		return nil, err
	}
	//log.Println("Datar:", datar)

	cdata := new(cooked.Cooked_data)
	for _, drec := range datar.Data {
		switch drec.Common.Dtype {
		case "er":
			//log.Println(drec)
			//log.Printf("%s Event: %v", drec.Rec.Res_type, drec.Rec.Rdata)

			er := new(cooked.Event_record)
			er.Resource = datar.Cid
			er.Entity_name = datar.App + ":" + datar.Ename
			er.App = datar.App

			//time := time.Unix(drec.Common.Ts, 0)
			//er.Day = time.Format("2006-01-02")
			er.Seq = int(drec.Common.Rc)
			er.Asset_type = get_asset_type_name(drec.Rec.Res_type)
			er.Ts = drec.Common.Ts * 1000
			er.Severity = drec.Common.Severity

			dr := new(cooked.Stats_record)
			dr.Resource = datar.Cid
			dr.Entity_name = datar.App + ":" + datar.Ename
			dr.App = datar.App

			dr.Seq = int(drec.Common.Rc)
			dr.Asset_type = er.Asset_type
			dr.Ts = drec.Common.Ts * 1000

			kv = make(map[string]interface{})
			dr.Kv = kv
			kv = make(map[string]interface{})
			er.Kv = kv

			switch drec.Rec.Res_type {
			case "ckt":
				d := drec.Rec.Rdata.(pli)
				er.Asset_name = strings.ToUpper(d.Pname) + "/" + strings.ToUpper(d.Name)
				er.Description = fmt.Sprintf("Switched %s", d.State)
				er.Kv["pname"] = strings.ToUpper(d.Pname)
				if d.State == "end" {
					dr.Asset_type = "Circuit"
					dr.Asset_name = strings.ToUpper(d.Name)
					counters := make(map[string]uint64)
					counters["total_cycles"] = d.Cycles
					dr.Counters = counters
					dr.Kv["pname"] = strings.ToUpper(d.Pname)
				} else {
					dr = nil
				}

			case "pump":
				d := drec.Rec.Rdata.(mot)
				er.Asset_name = strings.ToUpper(d.Pname) + "/" + strings.ToUpper(d.Name)
				er.Description = fmt.Sprintf("Switched %s", d.State)
				er.Kv["pname"] = strings.ToUpper(d.Pname)
				dr = nil

			case "sw":
				d := drec.Rec.Rdata.(val)
				dr.Kv["pname"] = strings.ToUpper(d.Pname)
				// Convert case for members in grouped - TBD
				//dr.Kv["group"] = d.Grouped
				if len(d.Grouped) > 0 {
					group_string := strings.Join(d.Grouped, ",")
					dr.Kv["group"] = group_string
				}
				dr.Asset_type = "Switch"
				dr.Asset_name = strings.ToUpper(d.Name)

				// Update stats

				counters := make(map[string]uint64)
				gauges := make(map[string]int64)

				switch d.Itype {
				case "tm": // Time based
					counters["schtime"] = d.Scheduled
					counters["runtime"] = d.Run
					counters["total_runtime"] = d.Total
					counters["total_cycles"] = d.Cycles
					dr.Kv["irrigation_type"] = "Time"

				case "vol", "sat", "sen": // Volume based
					counters["schvol"] = d.Scheduled
					counters["runvol"] = d.Run
					counters["total_volume"] = d.Total
					counters["total_cycles"] = d.Cycles
					if d.Itype == "vol" {
						dr.Kv["type"] = "Meter"
					} else if d.Itype == "sat" {
						dr.Kv["type"] = "Satl"
					} else if d.Itype == "sen" {
						dr.Kv["type"] = "Smart"
					}
				}
				er.Kv = dr.Kv
				dr.Counters = counters
				dr.Gauges = gauges
				er.Asset_name = strings.ToUpper(d.Pname) + "/" + strings.ToUpper(d.Name)
				er.Description = fmt.Sprintf("Switched %s(%s)", d.State, er.Kv["irrigation_type"])

			case "nut":
				d := drec.Rec.Rdata.(nut)
				dr.Kv["pname"] = strings.ToUpper(d.Pname)
				dr.Kv["vname"] = strings.ToUpper(d.Name)
				dr.Asset_type = "MCB"
				dr.Asset_name = strings.ToUpper(d.Vname)
				er.Asset_name = strings.ToUpper(d.Pname) + "/" + strings.ToUpper(d.Name)

				counters := make(map[string]uint64)
				gauges := make(map[string]int64)
				ntype := d.Ntype
				if ntype == "tm" {
					counters["runtime"] = d.Run
					counters["schtime"] = d.Scheduled
					dr.Kv["irrigation_type"] = "Time"
				} else if ntype == "vol" {
					counters["runvol"] = d.Run
					counters["schvol"] = d.Scheduled
					dr.Kv["irrigation_type"] = "Vol"
				}
				counters["total_cycles"] = d.Cycles
				dr.Counters = counters
				dr.Gauges = gauges

				er.Description = fmt.Sprintf("Switched %s", d.State)
				er.Kv = dr.Kv

			case "fuse":
				d := drec.Rec.Rdata.(flt)
				er.Asset_name = strings.ToUpper(d.Pname) + "/" + strings.ToUpper(d.Name)
				er.Description = fmt.Sprintf("Switched %s", d.State)
				dr.Kv["pname"] = strings.ToUpper(d.Pname)
				dr.Kv["name"] = strings.ToUpper(d.Name)
				dr.Asset_type = "Controller"
				dr.Asset_name = "C1"
				counters := make(map[string]uint64)
				gauges := make(map[string]int64)
				gauges["elapsed_time"] = d.Elapsed
				counters["current_cycles"] = d.Current
				counters["total_cycles"] = d.Cycles
				dr.Counters = counters
				dr.Gauges = gauges
				er.Kv = dr.Kv
			}
			//details = helper.Convert_struct_to_map(drec.Rec.Rdata)
			details := helper.Convert_struct_to_map(drec.Rec.Rdata)
			dj, _ := json.Marshal(&details)
			er.Details = string(dj)
			//log.Printf("Event Record: %v", *er)
			cdata.Events = append(cdata.Events, *er)
			if dr != nil {
				cdata.Stats = append(cdata.Stats, *dr)
			}

		case "dr":
			//log.Println(drec)
			//log.Printf("%s Stats: %v", drec.Rec.Res_type, drec.Rec.Stats)

			if len(drec.Recs) > 0 {
				for _, rec := range drec.Recs {
					dr := new(cooked.Stats_record)
					dr.Seq = int(drec.Common.Rc)
					dr.Ts = drec.Common.Ts * 1000
					dr.Resource = datar.Cid
					dr.Entity_name = datar.App + ":" + datar.Ename
					dr.App = datar.App
					dr.Asset_type = get_asset_type_name(rec.Res_type)

					d := rec.Rdata.(stats)
					dr.Asset_name = strings.ToUpper(d.Name)
					dr.Gauges = d.Gauges
					dr.Counters = d.Counters
					kv = make(map[string]interface{})
					dr.Kv = kv
					if d.Pname != "" {
						dr.Kv["pname"] = strings.ToUpper(d.Pname)
					}
					cdata.Stats = append(cdata.Stats, *dr)
				}
			} else {
				dr := new(cooked.Stats_record)
				dr.Seq = int(drec.Common.Rc)
				dr.Ts = drec.Common.Ts * 1000
				dr.Resource = datar.Cid
				dr.Entity_name = datar.App + ":" + datar.Ename
				dr.App = datar.App
				dr.Asset_type = get_asset_type_name(drec.Rec.Res_type)

				d := drec.Rec.Rdata.(stats)
				dr.Asset_name = strings.ToUpper(d.Name)
				dr.Gauges = d.Gauges
				dr.Counters = d.Counters
				kv = make(map[string]interface{})
				dr.Kv = kv
				if d.Pname != "" {
					dr.Kv["pname"] = strings.ToUpper(d.Pname)
				}
				cdata.Stats = append(cdata.Stats, *dr)
			}

		case "ar":
			//log.Println(drec)
			//log.Printf("%s Alarm: %v", drec.Rec.Res_type, drec.Rec.Rdata)

			// Alarm record
			ar := new(cooked.Event_record)
			ar.Resource = datar.Cid
			ar.Entity_name = datar.App + ":" + datar.Ename
			ar.App = datar.App

			ar.Seq = int(drec.Common.Rc)
			ar.Asset_type = get_asset_type_name(drec.Rec.Res_type)
			ar.Ts = drec.Common.Ts * 1000
			ar.Severity = drec.Common.Severity
			ar.Kv = make(map[string]interface{})

			d := drec.Rec.Rdata.(alarm)
			ar.Asset_name = strings.ToUpper(d.Name) + "/" + strings.ToUpper(d.Source)

			kv = make(map[string]interface{})
			ar.Kv = kv
			if d.Pname != "" {
				ar.Kv["pname"] = strings.ToUpper(d.Pname)
			}
			ar.Kv["source"] = strings.ToUpper(d.Source)
			ar.Kv["value"] = d.Value
			ar.Description = fmt.Sprintf("st:%s,v:%s", d.Status, d.Value)
			cdata.Events = append(cdata.Events, *ar)
		}
	}
	return cdata, nil
}

func get_asset_type_name(typ string) string {

	var name string
	switch typ {
	case "pump":
		name = "Pumpset"
	case "ckt":
		name = "Circuit"
	case "sw":
		name = "Switch"
	case "nut":
		name = "MCB"
	case "fuse":
		name = "Controller"
		//name = "Filter"
	case "cntl":
		name = "Controller"
	}
	return name
}
