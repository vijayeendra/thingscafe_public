openssl req -x509 -nodes -newkey rsa:2048 \
  -keyout server.key -out server.crt \
  -subj "/C=IN/L=Bengaluru/O=ThingsCafe Inc./CN=localhost1" \
  -reqexts v3_req -reqexts SAN -extensions SAN \
  -config <(cat ssl_config) \
  -days 3650

openssl x509 -in server.crt -outform pem -out server.pem
openssl x509 -in server.crt -outform der -out server.der
