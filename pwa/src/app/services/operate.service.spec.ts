import { TestBed } from '@angular/core/testing';

import { OperateService } from './operate.service';

describe('OperateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperateService = TestBed.get(OperateService);
    expect(service).toBeTruthy();
  });
});
