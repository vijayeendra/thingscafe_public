//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { AbstractControl, ValidatorFn } from '@angular/forms';
import { Injectable } from '@angular/core';
import libphonenumber from 'google-libphonenumber';
import CountryCodeSource from 'google-libphonenumber';

@Injectable({
  providedIn: 'root'
})

export class PhoneValidator {

  // Inspired on: https://github.com/yuyang041060120/ng2-validation/blob/master/src/equal-to/validator.ts
  public validCountryPhone = (country: string): ValidatorFn => {
    let subscribe:boolean = false;
    return (phoneControl: AbstractControl): {[key: string]: boolean} => {

      if (phoneControl.value !== '') {
        try {

          const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();
          const phoneNumber = "" + phoneControl.value + "";
          const pNumber = phoneUtil.parseAndKeepRawInput(phoneNumber, country);
          const isValidNumber = phoneUtil.isValidNumberForRegion(pNumber, country);

          if (isValidNumber) {
				return null;
          }
        } catch (e) {
          console.log(e);
          return {
            validCountryPhone: true
          };
        }

        return {
          validCountryPhone: true
        };
      } else {
        return null;
      }
    };
  }
}
