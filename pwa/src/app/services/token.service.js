"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var TOKEN_KEY = "X-Auth-Token";
var TokenService = /** @class */ (function () {
    function TokenService(storage) {
        this.storage = storage;
    }
    TokenService.prototype.getToken = function () {
        var _this = this;
        return this.storage.ready().then(function () {
            return _this.storage.get(TOKEN_KEY)
                .then(function (data) {
                return data;
            }, function (error) { return console.error(error); });
        });
    };
    TokenService.prototype.getTokenAsObservable = function () {
        return rxjs_1.from(this.getToken());
    };
    TokenService = __decorate([
        core_1.Injectable()
    ], TokenService);
    return TokenService;
}());
exports.TokenService = TokenService;
